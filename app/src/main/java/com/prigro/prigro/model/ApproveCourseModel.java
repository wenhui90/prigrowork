package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 27/05/2017
 */

public class ApproveCourseModel extends BaseWSModel {

    private String tutorID,listingID;

    public ApproveCourseModel(BaseActivity mActivity,String tutorID,String listingID) {
        super(mActivity);
        this.tutorID = tutorID;
        this.listingID = listingID;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {

        BaseWSBean baseWSBean = mGson.fromJson(jsonString, BaseWSBean.class);
        mCallBack.showResult(Constants.APPROVE_COURSE_MODEL,baseWSBean.getStatusMessage());
    }

    @Override
    public String getWsMethod() {
        return "bidding/accept";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token",Shared.read(Constants.SP_TOKEN));
        dataMap.put("listing_id",listingID);
        dataMap.put("tutor_id",tutorID);
        return dataMap;
    }
}
