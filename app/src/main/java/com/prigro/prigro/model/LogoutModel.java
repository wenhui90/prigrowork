package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 06/05/2017
 */

public class LogoutModel extends BaseWSModel {

    public LogoutModel(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
            mCallBack.showResult(Constants.LOGOUT_MODEL,mGson.fromJson(jsonString, BaseWSBean.class).getStatusMessage());
    }

    @Override
    public String getWsMethod() {
        return "user/logout_mobile";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {

        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        return dataMap;
    }
}
