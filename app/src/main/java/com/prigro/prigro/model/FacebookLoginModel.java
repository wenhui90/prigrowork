package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.LoginRegisterBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.RSA;

import java.util.HashMap;

/**
 * Created by vienhui on 19/05/2017
 */

public class FacebookLoginModel extends BaseWSModel {

    private LoginRegisterBean loginRegisterBean;
    private String fbToken;


    public FacebookLoginModel(BaseActivity mActivity, LoginRegisterBean loginRegisterBean, String fbToken) {
        super(mActivity);
        this.loginRegisterBean = loginRegisterBean;
        this.fbToken = fbToken;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {

        UserBean statusBean = mGson.fromJson(jsonString, UserBean.class);
        if (statusBean.getStatusCode().equals("270")){
            mCallBack.showResult(Constants.FB_ADD_TYPE,statusBean);

        }else {
            mCallBack.showResult(Constants.FB_LOGIN_MODEL, statusBean);
        }

    }

    @Override
    public String getWsMethod() {
        return "login/auth_facebook";
    }


    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {

        String loginBean = mGson.toJson(loginRegisterBean);
        loginBean = RSA.encryptWithStoredKey(loginBean);
        dataMap.put("details", loginBean);
        dataMap.put("fbtoken", fbToken);
        return dataMap;

    }
}
