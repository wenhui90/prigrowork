package com.prigro.prigro.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.bean.TimeSlotBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vienhui on 23/05/2017
 */

public class EditStdProfileModel extends BaseWSModel {

    private UserBean userBean;
    private ArrayList<TimeSlotBean> timeSlotBeans;

    public EditStdProfileModel(BaseActivity mActivity, UserBean userBean, ArrayList timeSlotBeans) {
        super(mActivity);
        this.userBean = userBean;
        this.timeSlotBeans = timeSlotBeans;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {

        mCallBack.showResult(Constants.EDIT_TUTOR_MODEL,mGson.fromJson(jsonString, BaseWSBean.class));
    }

    @Override
    public String getWsMethod() {
        return "account/edit_student";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", userBean.getId());
        dataMap.put("token", userBean.getToken());
        dataMap.put("password", userBean.getPassword());
        dataMap.put("email", userBean.getEmail());
        dataMap.put("name", userBean.getName());
        dataMap.put("contact", userBean.getContact());
        dataMap.put("new_password", userBean.getConfirm_password());
        dataMap.put("gender", userBean.getGender());
        dataMap.put("age", userBean.getAge());
        dataMap.put("race_id", userBean.getRace_id());
        dataMap.put("address", userBean.getAddress());
        dataMap.put("profile_picture", userBean.getProfilePicture());
        dataMap.put("nationality_id",userBean.getNationality_id());
        dataMap.put("timeslot", generateTimeSlot());
        dataMap.put("budget_per_month",userBean.getBudget());
        return dataMap;

    }

    private String generateTimeSlot() {

        String timeSlotData = mGson.toJson(timeSlotBeans);
        JsonParser jsonParser = new  JsonParser();
        JsonArray jsonArray  = (JsonArray)    jsonParser.parse(timeSlotData);
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("timeslot", jsonArray);
        return mGson.toJson(jsonObject);
    }
}
