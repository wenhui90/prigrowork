package com.prigro.prigro.model;

import android.os.Message;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.common.CommonHandler;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.utils.LanguageUtils;
import com.prigro.prigro.utils.Tools;

import java.util.HashMap;

/**
 * Created by SzHui on 2016/8/30
 */
abstract class BaseWSModel extends CommonHandler {

    public abstract void handlerSuccessMessage(String jsonString);

    public abstract String getWsMethod();

    public abstract HashMap<String,String> setPostData(HashMap<String,String> dataMap);

    private HashMap<String, String> dataMap;

    public BaseWSModel(BaseActivity mActivity) {
        super(mActivity);
        dataMap = new HashMap<>();
    }

    @Override
    public void handlerMainMessage(Message msg) {
        String tempData = msg.obj.toString();
        MyLog.d("tempData :: " + tempData);
        BaseWSBean wsBean = mGson.fromJson(tempData, BaseWSBean.class);
        if (!tempData.equals("")) {
            if (Tools.isSuccessCode(Integer.parseInt(wsBean.getStatusCode()))) {
                handlerSuccessMessage(tempData);
            } else {
                mCallBack.showException(Constants.STATUS_ERROR, wsBean);
            }
        } else {
            mCallBack.showUnknownError(Constants.STATUS_UNKNOWN);
        }
    }

    @Override
    protected void handlerExceptionMessage(Message msg) {
        mCallBack.showException(Constants.STATUS_EXCEPTION, msg.obj.toString());
    }

    public void executeData(String userType,String userId, String token) {
            dataMap.put("id", userId);
            dataMap.put("token", token);
            dataMap.put(userType.equals(Integer.toString(Constants.TUTOR))?"tutor_id":"student_id", userId);
            getNetworkHelper().postAsString(dataMap,getWsMethod());
    }

    public void executeData() {
        getNetworkHelper().postAsString(setPostData(dataMap),getWsMethod());
    }

    protected JsonObject getJsonObject(String jsonString){

        JsonParser jsonParser =new JsonParser();
        return (JsonObject) jsonParser.parse(jsonString);

    }

    protected JsonArray getJsonArray(JsonElement jsonArray){

        if (jsonArray.isJsonArray()){

            return (JsonArray) jsonArray;
        }

        return new JsonArray();

    }


}
