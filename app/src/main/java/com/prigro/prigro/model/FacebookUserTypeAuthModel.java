package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;

import java.util.HashMap;

/**
 * Created by vienhui on 27/05/2017
 */

public class FacebookUserTypeAuthModel extends BaseWSModel {

    private UserBean userBean;

    public FacebookUserTypeAuthModel(BaseActivity mActivity, UserBean userBean) {
        super(mActivity);
        this.userBean = userBean;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
            mCallBack.showResult(Constants.FB_SUCCESS_ADD_TYPE,userBean);
    }

    @Override
    public String getWsMethod() {
        return "login/new_facebook_user_type_assign";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id",userBean.getId());
        dataMap.put("token",userBean.getToken());
        dataMap.put("type",Integer.toString(userBean.getType()));
        return dataMap;
    }
}
