package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.LoginRegisterBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.utils.RSA;

import java.util.HashMap;

/**
 * Created by SzHui on 2017/4/22
 */

public class RegisterModel extends BaseWSModel {

    private LoginRegisterBean loginRegisterBean;

    public RegisterModel(BaseActivity mActivity, LoginRegisterBean loginRegisterBean) {
        super(mActivity);
        this.loginRegisterBean = loginRegisterBean;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        LoginRegisterBean statusBean = mGson.fromJson(jsonString, LoginRegisterBean.class);
        mCallBack.showResult(Constants.REGISTER_MODEL, statusBean.getStatusMessage());
    }

    @Override
    public String getWsMethod() {
        return "user/register";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        String loginBean = RSA.encryptWithStoredKey(mGson.toJson(loginRegisterBean));
        dataMap.put("details", loginBean);
        return dataMap;
    }
}

