package com.prigro.prigro.model;

import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vienhui on 05/05/2017
 */

public class StdCourseModel extends BaseWSModel {


    public StdCourseModel(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        new GenerateData().execute(jsonString);
    }

    @Override
    public String getWsMethod() {
        return "listing/student";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token",Shared.read(Constants.SP_TOKEN));
        return dataMap;
    }

    private class GenerateData extends AsyncTask<String,String,ArrayList<CourseBean>> {


        @Override
        protected ArrayList<CourseBean> doInBackground(String... params) {

            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = (JsonObject)jsonParser.parse(params[0]);
            return getTutorCourse(jsonObject);
        }


        @Override
        protected void onPostExecute(ArrayList<CourseBean> courseBeans) {
            mCallBack.showResult(Constants.STD_COURSE_MODEL,courseBeans);
        }

        ArrayList<CourseBean> getTutorCourse(final JsonObject jsonObject){


            final ArrayList<CourseBean> temp = new ArrayList<>();

            JsonArray courseArray =  jsonObject.getAsJsonArray("query").isJsonArray()?jsonObject.getAsJsonArray("query"):new JsonArray();

            for (int i=0;i<courseArray.size();i++){

                CourseBean tutorTemp = mGson.fromJson(courseArray.get(i),CourseBean.class);
//                tutorTemp.setAddress(Tools.getAddress(mActivity.getApplicationContext(),tutorTemp.getLatitude(),tutorTemp.getLongitude()));
                temp.add(tutorTemp);
            }
            return temp;
        }
    }
}
