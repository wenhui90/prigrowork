package com.prigro.prigro.model;

import com.google.gson.JsonArray;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.GuestBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.RSA;

import java.util.HashMap;

/**
 * Created by vienhui on 13/06/2017
 */

public class GuestLoginModel extends BaseWSModel {

    private String longitude, latitude;

    public GuestLoginModel(BaseActivity mActivity, String longitude, String latitude) {
        super(mActivity);
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        JsonArray jsonObject = getJsonObject(jsonString).getAsJsonArray("query");
        CourseBean[] courseBeans = mGson.fromJson(jsonObject, CourseBean[].class);
        mCallBack.showResult(Constants.GUEST_LOGIN, courseBeans);
    }

    @Override
    public String getWsMethod() {
        return "listing/guest";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {

        GuestBean guestBean = new GuestBean();
        guestBean.setIndex("0");
        guestBean.setLatitude(latitude);
        guestBean.setLongitude(longitude);
        String jsonTemp = mGson.toJson(guestBean);
        jsonTemp = RSA.encryptWithStoredKey(jsonTemp);
        dataMap.put("details", jsonTemp);
        return dataMap;
    }
}
