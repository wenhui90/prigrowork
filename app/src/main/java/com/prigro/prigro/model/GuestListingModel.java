package com.prigro.prigro.model;

import com.google.gson.JsonObject;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.RSA;

import java.util.HashMap;

/**
 * Created by vienhui on 13/06/2017
 */

public class GuestListingModel extends BaseWSModel {

    String listingId;

    public GuestListingModel(BaseActivity mActivity,String listingId) {
        super(mActivity);
        this.listingId = listingId;

    }

    @Override
    public void handlerSuccessMessage(String jsonString) {

        CourseBean courseBean = mGson.fromJson(getJsonObject(jsonString).get("listing"), CourseBean.class);
        mCallBack.showResult(Constants.STD_COURSE_DETAIL_MODEL,courseBean);
    }

    @Override
    public String getWsMethod() {
        return "listing/guest_detail";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("listing_id",listingId);
        dataMap.put("details", RSA.encryptWithStoredKey(jsonObject.toString()));
        return dataMap;
    }
}
