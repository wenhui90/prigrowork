package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.LoginRegisterBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 30/05/2017
 */

public class DeleteCourseModel extends BaseWSModel {

    private String listingID;

    public DeleteCourseModel(BaseActivity mActivity,String listingID) {
        super(mActivity);
        this.listingID = listingID;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        LoginRegisterBean statusBean = mGson.fromJson(jsonString, LoginRegisterBean.class);
        mCallBack.showResult(Constants.DELETE_COURSE_MODEL,statusBean);
    }

    @Override
    public String getWsMethod() {
        return "listing/delete";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {

        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        dataMap.put("listing_id",listingID);
        return dataMap;
    }
}
