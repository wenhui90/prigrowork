package com.prigro.prigro.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;

import java.util.HashMap;

/**
 * Created by vienhui on 22/11/2017
 */

public class AdsModel extends BaseWSModel {

    public AdsModel(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        MyLog.d("jsonString :: "+jsonString);
        JsonParser jsonParser = new JsonParser();
       JsonObject jsonObject = (JsonObject) jsonParser.parse(jsonString);
       mCallBack.showResult(jsonObject.get("poster_url").toString().trim().equals("\"\"")?-1:Constants.ADS_MODEL,jsonObject.get("poster_url").toString());
    }

    @Override
    public String getWsMethod() {
        return "ads/get";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        return dataMap;
    }
}
