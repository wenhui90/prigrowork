package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;

import java.util.HashMap;

/**
 * Created by vienhui on 04/05/2017
 */

public class EditTutorProfileModel extends BaseWSModel {

    UserBean tutorBean;

    public EditTutorProfileModel(BaseActivity mActivity, UserBean tutorBean) {
        super(mActivity);
        this.tutorBean = tutorBean;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        mCallBack.showResult(Constants.EDIT_TUTOR_MODEL,mGson.fromJson(jsonString, BaseWSBean.class));


    }

    @Override
    public String getWsMethod() {
        return "account/edit_tutor";
    }


    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {

        dataMap.put("id",tutorBean.getId());
        dataMap.put("token",tutorBean.getToken());
        dataMap.put("password",tutorBean.getPassword());
        dataMap.put("email",tutorBean.getEmail());
        dataMap.put("name",tutorBean.getName());
        dataMap.put("contact",tutorBean.getContact());
        dataMap.put("new_password",tutorBean.getConfirm_password());
        dataMap.put("gender",tutorBean.getGender());
        dataMap.put("age",tutorBean.getAge());
        dataMap.put("nationality_id",tutorBean.getNationality_id());
        dataMap.put("race_id",tutorBean.getRace_id());
        dataMap.put("profile_picture",tutorBean.getProfilePicture());
        dataMap.put("experience",tutorBean.getExperience());
        dataMap.put("address",tutorBean.getAddress());
        dataMap.put("category_id",tutorBean.getCategory_id());
        dataMap.put("qualification_id",tutorBean.getQualification_id());
        dataMap.put("description",tutorBean.getDescription());
        dataMap.put("nric",tutorBean.getNric());
        dataMap.put("document",tutorBean.getFilePath());
        dataMap.put("receipt",tutorBean.getReceipt());
        return dataMap;
    }
}
