package com.prigro.prigro.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.TuitionCenterBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 21/11/2017
 */

public class TuitionCenterDetailModel extends BaseWSModel {

    private String tuitionId;

    public TuitionCenterDetailModel(BaseActivity mActivity,String tuitionId) {
        super(mActivity);
        this.tuitionId = tuitionId;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        JsonParser jsonParser = new JsonParser();
        JsonObject tuitionTemp = (JsonObject) jsonParser.parse(jsonString);
        mCallBack.showResult(Constants.TUITION_CENTER_DETAIL_MODEL,mGson.fromJson(tuitionTemp.get("listing"), TuitionCenterBean.class));
    }

    @Override
    public String getWsMethod() {
        return "tuition/detail";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        dataMap.put("tuition_id",tuitionId);
        return dataMap;
    }
}
