package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.NotificationBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 16/06/2017
 */

public class NotificationSetSeenModel extends BaseWSModel {

    private int listingId;

    public NotificationSetSeenModel(BaseActivity mActivity,int listingId) {
        super(mActivity);
        this.listingId = listingId;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
         mCallBack.showResult(Constants.SET_NOTIFICATION_SEEN,mGson.fromJson(jsonString, NotificationBean.class));


    }

    @Override
    public String getWsMethod() {
        return "notification/set_seen ";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        dataMap.put("listing_id",Integer.toString(listingId));
        return dataMap;
    }
}
