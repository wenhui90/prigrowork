package com.prigro.prigro.model;

import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.utils.Shared;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vienhui on 27/05/2017
 */

public class SearchModel extends BaseWSModel {

    String longitude, latitude;
    String search;

    public SearchModel(BaseActivity mActivity,String search, String longitude, String latitude) {
        super(mActivity);
        this.search = search;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {

        new GenerateData().execute(jsonString);
    }

    @Override
    public String getWsMethod() {
        return "listing/search";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token",Shared.read(Constants.SP_TOKEN));
        dataMap.put("search_string",search);
        dataMap.put("latitude", latitude);
        dataMap.put("longitude", longitude);
        return dataMap;
    }

    class GenerateData extends AsyncTask<String,String,ArrayList<CourseBean>> {


        @Override
        protected ArrayList<CourseBean> doInBackground(String... params) {

            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = (JsonObject)jsonParser.parse(params[0]);
            return getTutorCourse(jsonObject);
        }


        @Override
        protected void onPostExecute(ArrayList<CourseBean> courseBeans) {
            mCallBack.showResult(Constants.SEARCH_MODEL,courseBeans);
        }

        public ArrayList<CourseBean> getTutorCourse(final JsonObject jsonObject){


            final ArrayList<CourseBean> temp = new ArrayList<>();
            MyLog.d("jsonObject :: "+jsonObject);
            JsonArray courseArray =  jsonObject.getAsJsonArray("result").isJsonArray()?jsonObject.getAsJsonArray("result"):new JsonArray();

            for (int i=0;i<courseArray.size();i++){

                CourseBean tutorTemp = mGson.fromJson(courseArray.get(i),CourseBean.class);
                temp.add(tutorTemp);
            }
            return temp;
        }
    }
}
