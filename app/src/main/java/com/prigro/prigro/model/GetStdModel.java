package com.prigro.prigro.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.TimeSlotBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 04/05/2017
 */

public class GetStdModel extends BaseWSModel {


    private String stdID="";

    public GetStdModel(BaseActivity mActivity) {

        super(mActivity);
    }

    public GetStdModel(BaseActivity mActivity,String stdID) {

        super(mActivity);
        this.stdID = stdID;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        JsonParser jsonParser =new JsonParser();
        JsonObject temp = (JsonObject) jsonParser.parse(jsonString);
        UserBean tutorBean = mGson.fromJson(temp.getAsJsonArray("user").get(0), UserBean.class);
        tutorBean.setTimeslot(mGson.fromJson(temp.get("timeslot"), TimeSlotBean[].class));
        tutorBean.setType(Constants.STUDENT);
        MyLog.d("tutorbean :: "+tutorBean.getTimeslot().toString());
        mCallBack.showResult(Constants.GET_STD_MODEL, tutorBean);
    }

    @Override
    public String getWsMethod() {
        return "account/get_student";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        dataMap.put("student_id",stdID.equals("")?Shared.read(Constants.SP_UNIQUE_ID):stdID);

        return dataMap;
    }
}
