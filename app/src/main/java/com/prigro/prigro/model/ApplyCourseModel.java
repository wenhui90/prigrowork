package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 05/05/2017
 */

public class ApplyCourseModel extends BaseWSModel {

    private boolean isEdit;
    private String listingID,fee;

    public ApplyCourseModel(BaseActivity mActivity,String listingID,String fee,boolean isEdit) {
        super(mActivity);
        this.listingID = listingID;
        this.fee = fee;
        this.isEdit = isEdit;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
       BaseWSBean baseWSBean = mGson.fromJson(jsonString, BaseWSBean.class);
        mCallBack.showResult(Constants.APPLY_COURSE_MODEL,baseWSBean.getStatusMessage());
    }

    @Override
    public String getWsMethod() {
        return isEdit?"bidding/edit":"bidding/apply";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id",Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token",Shared.read(Constants.SP_TOKEN));
        dataMap.put("listing_id",listingID);
        dataMap.put("rates",fee);
        return dataMap;
    }
}
