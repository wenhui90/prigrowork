package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.FAQBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.LanguageUtils;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 19/05/2017
 */

public class FaqModel extends BaseWSModel {


    public FaqModel(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {

        mCallBack.showResult(-1, mGson.fromJson(jsonString,FAQBean.class));
    }

    @Override
    public String getWsMethod() {
        return "faq/get";
    }


    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {

        dataMap.put("language", LanguageUtils.getLocaleName(Shared.readInt(Constants.SP_LANGUAGE, Constants.LANGUAGE_DEFAULT)));
        return dataMap;
    }
}
