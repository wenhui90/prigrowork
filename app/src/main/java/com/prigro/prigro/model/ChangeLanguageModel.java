package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.LanguageUtils;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 16/09/2017
 */

public class ChangeLanguageModel extends BaseWSModel {

    private int languageType;

    public ChangeLanguageModel(BaseActivity mActivity,int languageType) {
        super(mActivity);
        this.languageType = languageType;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {

        mCallBack.showResult(Constants.CHANGE_LANGUAGE,mGson.fromJson(jsonString, BaseWSBean.class).getStatusMessage());
    }

    @Override
    public String getWsMethod() {
        return "user/set_language";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token",Shared.read(Constants.SP_TOKEN));
        dataMap.put("language", LanguageUtils.getLocaleName(languageType));
        return dataMap;
    }
}
