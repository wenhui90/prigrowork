package com.prigro.prigro.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.TimeSlotBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 05/05/2017
 */

public class StdCourseDetailModel extends BaseWSModel {

    private String listingDetail;

    public StdCourseDetailModel(BaseActivity mActivity,String listingDetail) {
        super(mActivity);
        this.listingDetail =listingDetail;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject)jsonParser.parse(jsonString);
        CourseBean courseBean= mGson.fromJson(jsonObject.getAsJsonObject("listing"), CourseBean.class);
        courseBean.setTutors(mGson.fromJson(jsonObject.get("tutors"), UserBean[].class));
        courseBean.setTimeslot(mGson.fromJson(jsonObject.get("timeslot"), TimeSlotBean[].class));
        mCallBack.showResult(Constants.STD_COURSE_DETAIL_MODEL,courseBean);
    }

    @Override
    public String getWsMethod() {
        return "listing/detail";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {

        dataMap.put("id",Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token",Shared.read(Constants.SP_TOKEN));
        dataMap.put("listing_id",listingDetail);
        return dataMap;
    }
}
