package com.prigro.prigro.model;

import com.google.gson.JsonObject;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 03/05/2017
 */

public class GetTutorModel extends BaseWSModel {

    private String tutorID = "";
    private boolean isListingData = false;

    public GetTutorModel(BaseActivity mActivity) {
        super(mActivity);
    }

    public GetTutorModel(BaseActivity mActivity,String tutorID) {
        super(mActivity);
        this.tutorID = tutorID;
    }

    public GetTutorModel(BaseActivity mActivity,boolean isListingData) {
        super(mActivity);
        this.isListingData = isListingData;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        JsonObject temp = getJsonObject(jsonString);
        UserBean tutorBean = mGson.fromJson(temp.getAsJsonArray("user").get(0), UserBean.class);
        tutorBean.setListing_apply(mGson.fromJson(getJsonArray(temp.get("listing_apply")), CourseBean[].class));
        tutorBean.setListing_approve(mGson.fromJson(getJsonArray(temp.get("listing_approve")), CourseBean[].class));
        tutorBean.setType(Constants.TUTOR);
        mCallBack.showResult(!isListingData?Constants.GET_TUTOR_MODEL:Constants.GET_LISTING_DETAIL, tutorBean);
    }

    @Override
    public String getWsMethod() {
        return "account/get_tutor";
    }



    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        dataMap.put("tutor_id",tutorID.equals("")?Shared.read(Constants.SP_UNIQUE_ID):tutorID);
        return dataMap;
    }
}
