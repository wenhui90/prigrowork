package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 05/05/2017
 */

public class AddCourseModel extends BaseWSModel {

    private CourseBean courseBean;

    public AddCourseModel(BaseActivity mActivity, CourseBean courseBean) {
        super(mActivity);
        this.courseBean = courseBean;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        mCallBack.showResult(Constants.STD_ADD_COURSE_MODEL,mGson.fromJson(jsonString, BaseWSBean.class).getStatusMessage());
    }

    @Override
    public String getWsMethod() {
        return "listing/add";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {

        dataMap.put("id",Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token",Shared.read(Constants.SP_TOKEN));
        dataMap.put("title",courseBean.getTitle());
        dataMap.put("subject_id",Integer.toString(courseBean.getSubject_id()));
        dataMap.put("level_id",Integer.toString(courseBean.getLevel_id()));
        dataMap.put("lesson_type_id",Integer.toString(courseBean.getLesson_type()));
        dataMap.put("hour_per_lesson",courseBean.getHour_per_lesson());
        dataMap.put("lesson_per_week",Integer.toString(courseBean.getLesson_per_week()));
        dataMap.put("preferred_gender",courseBean.getGender());
        dataMap.put("latitude",courseBean.getLatitude());
        dataMap.put("longitude",courseBean.getLongitude());
        return dataMap;
    }
}
