package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 26/07/2017
 */

public class RegisterNotificationTokenModel extends BaseWSModel {

    private String fireBaseToken;

    public RegisterNotificationTokenModel(BaseActivity mActivity,String fireBaseToken) {
        super(mActivity);
        this.fireBaseToken = fireBaseToken;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        BaseWSBean baseWSBean = mGson.fromJson(jsonString, BaseWSBean.class);
        mCallBack.showResult(-1,baseWSBean.getStatusMessage());
    }

    @Override
    public String getWsMethod() {
        return "notification/update_firebase_token";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id",Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        dataMap.put("firebase_token",fireBaseToken);
        return dataMap;
    }
}
