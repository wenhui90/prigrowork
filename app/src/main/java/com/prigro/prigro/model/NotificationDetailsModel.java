package com.prigro.prigro.model;

import com.google.gson.JsonObject;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.bean.NotificationBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 16/06/2017
 */

public class NotificationDetailsModel extends BaseWSModel {


    public NotificationDetailsModel(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {

        JsonObject jsonObject = getJsonObject(jsonString);
        BaseWSBean wsBean = mGson.fromJson(jsonString, BaseWSBean.class);
        if (!wsBean.getStatusCode().equals("289")){
            NotificationBean[] notificationBeans = mGson.fromJson(jsonObject.getAsJsonArray("notification"), NotificationBean[].class);
            mCallBack.showResult(Constants.GET_NOTIFICATION_LIST, notificationBeans);
        }

    }

    @Override
    public String getWsMethod() {
        return "notification/details";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        return dataMap;
    }
}
