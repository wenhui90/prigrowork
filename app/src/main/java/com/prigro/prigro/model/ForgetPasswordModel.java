package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.common.Constants;

import java.util.HashMap;

/**
 * Created by vienhui on 06/05/2017
 */

public class ForgetPasswordModel extends BaseWSModel {

    String email;

    public ForgetPasswordModel(BaseActivity mActivity,String email) {
        super(mActivity);
        this.email = email;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {

        mCallBack.showResult(Constants.FORGOT_PASSWORD_MODEL,mGson.fromJson(jsonString, BaseWSBean.class).getStatusMessage());

    }

    @Override
    public String getWsMethod() {
        return "password/forgot";
    }


    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {

        dataMap.put("email",email);
        return dataMap;
    }
}
