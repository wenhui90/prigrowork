package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.LoginRegisterBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.RSA;

import java.util.HashMap;

/**
 * Created by SzHui on 2017/4/24
 */

public class EmailLoginModel extends BaseWSModel {

    private LoginRegisterBean loginRegisterBean;

    public EmailLoginModel(BaseActivity mActivity,LoginRegisterBean loginRegisterBean) {
        super(mActivity);
        this.loginRegisterBean = loginRegisterBean;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        LoginRegisterBean userBean =   mGson.fromJson(jsonString, LoginRegisterBean.class);
        mCallBack.showResult(Constants.EMAIL_LOGIN__MODEL,userBean);
    }

    @Override
    public String getWsMethod() {
        return "login/auth_mobile";
    }


    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {

        String loginBean =  mGson.toJson(loginRegisterBean);
        loginBean =    RSA.encryptWithStoredKey(loginBean);
        dataMap.put("details", loginBean);

        return dataMap;
    }

}
