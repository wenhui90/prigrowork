package com.prigro.prigro.model;

import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.CourseListBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by SzHui on 2017/4/28
 */

public class TutorCourseModel extends BaseWSModel {

    private String longitude, latitude;
    private boolean isRefresh;
    private int index;

    public TutorCourseModel(BaseActivity mActivity, String longitude, String latitude,boolean isRefresh,int index) {
        super(mActivity);
        this.longitude = longitude;
        this.latitude = latitude;
        this.isRefresh =isRefresh;
        this.index =index;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        new GenerateData().execute(jsonString);

    }

    @Override
    public String getWsMethod() {
        return "listing/tutor";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        dataMap.put("index",isRefresh?"0":Integer.toString(index));
        dataMap.put("latitude", latitude);
        dataMap.put("longitude", longitude);
        return dataMap;
    }


    private class GenerateData extends AsyncTask<String, String, CourseListBean> {


        @Override
        protected CourseListBean doInBackground(String... params) {

            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = (JsonObject) jsonParser.parse(params[0]);
            return getTutorCourse(jsonObject);
        }


        @Override
        protected void onPostExecute(CourseListBean courseBeans) {

            if (isRefresh){

                mCallBack.showResult(Constants.TUTOR_COURSE_MODEL, courseBeans);


            }else{

                mCallBack.showResult(Constants.ADD_COURSE_LIST_MODEL, courseBeans);
            }

        }

        CourseListBean getTutorCourse(final JsonObject jsonObject) {

            CourseListBean courseListBean = new CourseListBean();
            courseListBean.getCourseBeans();
            courseListBean.setEndPoint(jsonObject.getAsJsonPrimitive("endpoint").getAsInt());
            JsonArray courseArray = jsonObject.getAsJsonArray("query").isJsonArray() ? jsonObject.getAsJsonArray("query") : new JsonArray();

            for (int i = 0; i < courseArray.size(); i++) {

                CourseBean tutorTemp = mGson.fromJson(courseArray.get(i), CourseBean.class);
                courseListBean.getCourseBeans().add(tutorTemp);
            }
            return courseListBean;
        }
    }
}
