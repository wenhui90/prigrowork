package com.prigro.prigro.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.TuitionCenterBean;
import com.prigro.prigro.bean.TuitionCenterListBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 19/11/2017
 */

public class TuitionCenterModel extends BaseWSModel {

    private String index;
    private String latitude;
    private String longitude;
    private String searchKeyword;
    private boolean isRefresh;




    public TuitionCenterModel(BaseActivity mActivity) {
        super(mActivity);
    }

    public TuitionCenterModel(BaseActivity mActivity,String index,String latitude,String longitude,String searchKeyword,boolean isRefresh){
        super(mActivity);
        this.index = index;
        this.latitude = latitude;
        this.longitude = longitude;
        this.searchKeyword = searchKeyword;
        this.isRefresh = isRefresh;

    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        MyLog.d("Json String :: "+jsonString);
        mCallBack.showResult(searchKeyword.isEmpty()?Constants.TUITION_CENTER_MODEL:Constants.SEARCH_MODEL,getTuitionCenterList(jsonString));

    }

    @Override
    public String getWsMethod() {
        return "tuition/all ";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id", Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        dataMap.put("index",isRefresh?"0":index);
        dataMap.put("longitude",longitude);
        dataMap.put("latitude",latitude);
        dataMap.put("search_string",searchKeyword);
        return dataMap;
    }

    private TuitionCenterListBean getTuitionCenterList(String jsonString){
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject) jsonParser.parse(jsonString);
        TuitionCenterListBean tuitionCenterListBean = new TuitionCenterListBean();
        tuitionCenterListBean.setEndPoint(jsonObject.getAsJsonPrimitive("endpoint").getAsInt());
        JsonArray tuitionCenterArray = jsonObject.getAsJsonArray("result").isJsonArray() ? jsonObject.getAsJsonArray("result") : new JsonArray();
        for (int i = 0; i < tuitionCenterArray.size(); i++) {

            TuitionCenterBean tutorTemp = mGson.fromJson(tuitionCenterArray.get(i), TuitionCenterBean.class);
            tuitionCenterListBean.getCourseBeans().add(tutorTemp);
        }
        return tuitionCenterListBean;
    }

}
