package com.prigro.prigro.model;

import com.google.gson.JsonObject;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.DropDownBean;
import com.prigro.prigro.bean.DropDownContentBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.LanguageUtils;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 04/06/2017
 */

public class GetDropDownModel extends BaseWSModel {

    private final static int RACE = 1;

    private int dropDownType = -1;

    public GetDropDownModel(BaseActivity mActivity) {
        super(mActivity);
    }

    public GetDropDownModel(BaseActivity mActivity, int dropDownType) {
        super(mActivity);
        this.dropDownType = dropDownType;

    }

    @Override
    public void handlerSuccessMessage(String jsonString) {
        JsonObject jsonObject = getJsonObject(jsonString);


        switch (dropDownType) {

            case RACE:

                DropDownBean[] dropDownBean = mGson.fromJson(jsonObject.getAsJsonArray("race"), DropDownBean[].class);
                mCallBack.showResult(Constants.GET_DROPDOWN_TYPE,dropDownBean);
                break;
            default:
                DropDownContentBean dropDownContentBean = mGson.fromJson(jsonString, DropDownContentBean.class);
                mCallBack.showResult(Constants.GET_DROPDOWN_TYPE,dropDownContentBean);
        }

    }

    @Override
    public String getWsMethod() {

        switch (dropDownType) {

            case RACE:
                return "dropdown/race";

            default:
                return "dropdown/index";
        }
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("language", LanguageUtils.getLocaleName(Shared.readInt(Constants.SP_LANGUAGE,Constants.LANGUAGE_DEFAULT)));
        return dataMap;
    }
}
