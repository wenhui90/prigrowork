package com.prigro.prigro.model;

import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.HashMap;

/**
 * Created by vienhui on 26/07/2017
 */

public class SetSeenModel extends BaseWSModel {

    private String  listingID;

    public SetSeenModel(BaseActivity mActivity,String listingID) {
        super(mActivity);
        this.listingID = listingID;
    }

    @Override
    public void handlerSuccessMessage(String jsonString) {

    }

    @Override
    public String getWsMethod() {
        return "notification/set_seen_tutor_approved_listing";
    }

    @Override
    public HashMap<String, String> setPostData(HashMap<String, String> dataMap) {
        dataMap.put("id",Shared.read(Constants.SP_UNIQUE_ID));
        dataMap.put("token", Shared.read(Constants.SP_TOKEN));
        dataMap.put("listing_id",listingID);
        return dataMap;
    }
}
