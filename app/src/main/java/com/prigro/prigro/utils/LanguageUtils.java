package com.prigro.prigro.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;

import java.util.Locale;

/**
 * Created by vienhui on 16/09/2017
 */

public class LanguageUtils {

    public static void setLanguage(Context context, Locale localeLanguage) {

        Resources resources = context.getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        // 应用用户选择语言
            config.locale = localeLanguage;

        resources.updateConfiguration(config, dm);


    }

    public static boolean isSetLanguageValue() {


        switch (Shared.readInt(Constants.SP_LANGUAGE, Constants.LANGUAGE_DEFAULT)) {

            case Constants.LANGUAGE_EN:
            case Constants.LANGUAGE_CN:
            case Constants.LANGUAGE_MY:
                return true;

            default:

                return false;


        }
    }

    public static Locale getSettingLocaleValue(int localeValue) {

        switch (localeValue) {

            case Constants.LANGUAGE_EN:
                MyLog.d("Locale.ENGLISH");
                return Locale.ENGLISH;
            case Constants.LANGUAGE_CN:

                MyLog.d("Locale.CN");
                return Locale.CHINA;
            case Constants.LANGUAGE_MY:

                MyLog.d("Locale.MY");
                return new Locale("ms");

            default:

                MyLog.d("Locale.DEFAULT");
                return Locale.getDefault();
        }
    }


    public static String getLocaleName(int languageType){

        switch (languageType) {

            case Constants.LANGUAGE_EN:
                MyLog.d("Locale.ENGLISH");
                return "en";
            case Constants.LANGUAGE_CN:

                MyLog.d("Locale.CN");
                return "cn";
            case Constants.LANGUAGE_MY:

                MyLog.d("Locale.MY");
                return "my";

            default:

                MyLog.d("Locale.DEFAULT");
                return "en";
        }

    }


}
