package com.prigro.prigro.utils;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.prigro.prigro.R;

import java.util.List;

/**
 * Created by SzHui on 2016/8/29
 */
public class MyDialog {


    public static void showRemindDialog(Context p_context, String p_strTitle, String p_strContent){
        new MaterialDialog.Builder(p_context)
                .title(p_strTitle)
                .cancelable(false)
                .theme(Theme.LIGHT)
                .content(p_strContent)
                .positiveText(p_context.getString(R.string.confirm))
                .show();
    }

    public static void showErrorDialog(Context p_context, String p_strContent){
        new MaterialDialog.Builder(p_context)
                .title(R.string.error)
                .cancelable(true)
                .theme(Theme.LIGHT)
                .content(p_strContent)
                .positiveText(p_context.getString(android.R.string.ok))
                .show();
    }

    public static void showConfirmDialog(Context p_context, String p_strTitle, String p_strContent, MaterialDialog.SingleButtonCallback onClick){
        new MaterialDialog.Builder(p_context)
                .title(p_strTitle)
                .cancelable(false)
                .theme(Theme.LIGHT)
                .onPositive(onClick)
                .content(p_strContent)
                .positiveText(p_context.getString(R.string.confirm))
                .positiveColor(p_context.getResources().getColor(R.color.skyBlue))
                .cancelable(false)
                .show();
    }

    public static void showConfirmDialog(Context p_context, String p_strTitle, String p_strContent, MaterialDialog.SingleButtonCallback onPositive,MaterialDialog.SingleButtonCallback onNegative){
        new MaterialDialog.Builder(p_context)
                .title(p_strTitle)
                .cancelable(false)
                .theme(Theme.LIGHT)
                .onPositive(onPositive)
                .onNegative(onNegative)
                .content(p_strContent)
                .positiveText(R.string.confirm)
                .positiveColor(p_context.getResources().getColor(R.color.skyBlue))
                .negativeText(R.string.cancel)
                .negativeColor(p_context.getResources().getColor(R.color.backgroundGrey))
                .cancelable(true)
                .show();
    }

    public static void showListingDialog(Context p_context, String title, List listItem, MaterialDialog.ListCallback listCallback) {

        new MaterialDialog.Builder(p_context)
                .title(title)
                .theme(Theme.LIGHT)
                .items(listItem)
                .itemsCallback(listCallback)
                .show();
    }
}
