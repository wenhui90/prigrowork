package com.prigro.prigro.utils;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by vienhui on 22/11/2017
 */

public class ByteDataDownloader extends AsyncTask<String, Void, byte[]> {
    private static final String TAG = "ByteDataDownloader";

    public interface ByteLoadingListener{

        void onByteLoadFinish(byte[] imgvData);
    }
    ByteLoadingListener byteLoadingListener = null;

    @Override protected byte[] doInBackground(final String... params) {
        final String gifUrl = params[0];

        if (gifUrl == null)
            return null;

        try {
            return ByteArrayHttpClient.get(gifUrl);
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "GifDecode OOM: " + gifUrl, e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(byte[] bytes) {
        super.onPostExecute(bytes);
        if (byteLoadingListener!=null)
        byteLoadingListener.onByteLoadFinish(bytes);
    }

    public void setOnSuccessLoadingListener(ByteLoadingListener byteLoadingListener){

        this.byteLoadingListener = byteLoadingListener;

    }



}