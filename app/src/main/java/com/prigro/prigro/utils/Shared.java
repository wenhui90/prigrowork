package com.prigro.prigro.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;

@SuppressLint("CommitPrefEdits")
public final class Shared {

   private static ContextWrapper sInstance;
   private static SharedPreferences sPref;
   private static Context sContext;

   public static void initialize(Context base) {
      sContext = base;
      if (sPref == null || sInstance == null) {
         sInstance = new ContextWrapper(base);
         sPref = sInstance.getSharedPreferences("Mudroc", Context.MODE_PRIVATE);
      }
   }

   public static SharedPreferences getSp(){
      return sPref;
   }


   public static Context getBaseContext() {
      return sInstance.getBaseContext();
   }

   public static int getColor(int intColor) {
      return sContext.getResources().getColor(intColor);
   }

   public static Context getContext() {
      return sContext;
   }

   public static void setContext(Context context) {
      sContext = context;
   }

   public static void clear() {
      SharedPreferences.Editor editor = sPref.edit();
      editor.clear();
      editor.apply();

   }

   public static int readInt(String key, int defualt) {
      return sPref.getInt(key, defualt);
   }

   public static void writeInt(String key, int value) {
      SharedPreferences.Editor editor = sPref.edit();
      editor.putInt(key, value);
      editor.apply();
   }

   public static String getString(int resId) {
      return sInstance.getResources().getString(resId);
   }

   public static void write(String key, String value) {
      SharedPreferences.Editor editor = sPref.edit();
      editor.putString(key, value);
      editor.apply();
   }

   public static String read(String key) {
      return Shared.read(key, null);
   }

   public static String read(String key, String defValue) {
      return sPref.getString(key, defValue);
   }

   public static void writeBoolean(String key, boolean value) {
      SharedPreferences.Editor editor = sPref.edit();
      editor.putBoolean(key, value);
      editor.apply();
   }

   public static boolean readBoolean(String key, boolean defValue) {
      return sPref.getBoolean(key, defValue);
   }

   public static void writeLong(String key, long value) {
      SharedPreferences.Editor editor = sPref.edit();
      editor.putLong(key, value);
      editor.apply();
   }

   public static void remove(String key){

      SharedPreferences.Editor editor = sPref.edit();
      editor.remove(key);
      editor.apply();

   }

   public static long readLong(String key, long defValue) {
      return sPref.getLong(key, defValue);
   }

   public static void writeFloat(String key, float value) {
      SharedPreferences.Editor editor = sPref.edit();
      editor.putFloat(key, value);
      editor.apply();
   }

   public static float readFloat(String key, float defValue) {
      return sPref.getFloat(key, defValue);
   }

   public static float readFloat(String key) {
      return sPref.getFloat(key, 0);
   }
}
