package com.prigro.prigro.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.prigro.prigro.common.Constants;
import com.prigro.prigro.fragment.main.StdDashBoardFragment;
import com.prigro.prigro.fragment.main.TutorDashBoardFragment;

/**
 * Created by vienhui on 04/08/2017
 */

public class FireBaseNotificationReceive  extends WakefulBroadcastReceiver {


    public void onReceive(Context context, Intent intent) {
            if (Shared.getSp()==null)
                return;

            if (Shared.readInt(Constants.SP_USER_TYPE,-1)==Constants.TUTOR){

                TutorDashBoardFragment.getInstance(null).refreshDashboardData();
            }else if (Shared.readInt(Constants.SP_USER_TYPE,-1)==Constants.STUDENT){

                StdDashBoardFragment.getInstance(null).refreshDashboardData();
            }
    }
}
