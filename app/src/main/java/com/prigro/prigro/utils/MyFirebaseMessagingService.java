package com.prigro.prigro.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.MainActivity;
import com.prigro.prigro.common.MyLog;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        MyLog.d("-----------------------------------------*************************::");
        // Check if message contains a notification payload.
        MyLog.d("-----------------------------------------:: "+remoteMessage.getNotification());
        if (remoteMessage.getNotification() != null) {
            handleNow();
//            sendNotification(remoteMessage.getNotification());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
//        long [] pattern = {100,400};   // 停止 开启 停止 开启
        vibrator.vibrate(500);
        Intent intent = new Intent("com.prigro.prigro_FCM-Message");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
//        if (Shared.readInt(Constants.SP_USER_TYPE,-1)==Constants.TUTOR){
//
//            TutorDashBoardFragment.getInstance(null).refreshNotificationData();
//        }else if (Shared.readInt(Constants.SP_USER_TYPE,-1)==Constants.STUDENT){
//
//            StdDashBoardFragment.getInstance(null).refreshNotification();
//        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(RemoteMessage.Notification messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,0);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_play_icon)
                .setContentTitle(messageBody.getTitle())
                .setContentText(messageBody.getBody())
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }
}
