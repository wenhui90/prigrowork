package com.prigro.prigro.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.prigro.prigro.R;
import com.prigro.prigro.bean.DropDownBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.network.CustomVolleyRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SzHui on 2016/7/18
 */
public class Tools {


    public static Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
        int targetWidth = 50;
        int targetHeight = 50;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                targetHeight,Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);

        canvas.clipPath(path);
        Bitmap sourceBitmap = scaleBitmapImage;
        canvas.drawBitmap(sourceBitmap,
                new Rect(0, 0, sourceBitmap.getWidth(),
                        sourceBitmap.getHeight()),
                new Rect(0, 0, targetWidth, targetHeight), null);
        return targetBitmap;
    }

    public static boolean isSuccessCode(int statusCode){

        for (int i = 0; i< Constants.SUCCESS_CODE.length; i++){

            if (statusCode==Constants.SUCCESS_CODE[i]){
                return true;
            }
        }
        return false;
    }

    public static boolean isValidEmailAddress(String email,Context context) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        if (!m.matches()){

            MyDialog.showErrorDialog(context,context.getString(R.string.input_valid_email));

        }
        return m.matches();
    }

    public static ArrayList getDropDownData(List<DropDownBean> list){


        ArrayList<String> arrayList = new ArrayList();

        for (int i = 0; i <list.size() ; i++) {

            arrayList.add(list.get(i).getName());
        }
        return arrayList;
    }


    public static int getDataId(List<DropDownBean> listChecking, String data) {

        for (DropDownBean dropDownBean : listChecking) {
            if (dropDownBean.getName().equals(data)) {

                return dropDownBean.getId();
            }
        }
        return 0;
    }

    public void loadImage(Context context, NetworkImageView imageView, String url) {

        ImageLoader imageLoader = CustomVolleyRequest.getInstance(context)
                .getImageLoader();
        imageLoader.get(url, ImageLoader.getImageListener(imageView,
                R.drawable.place_holder, android.R.drawable
                        .ic_dialog_alert));
        imageView.setImageUrl(url, imageLoader);
    }

    public static String getGender(Context context,String gender) {

        switch (gender){
            case "N":

                return context.getString(R.string.not_preference);

            case "M":
                return context.getString(R.string.male);


            case "F":

                return context.getString(R.string.female);

            default:
                return "";
        }
    }

    public static void sendSMS(String contact,Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
        {
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(context); // Need to change the build to API 19

            Uri uri = Uri.parse("smsto:"+contact);
            Intent it = new Intent(Intent.ACTION_SENDTO, uri);
            context.startActivity(it);

            if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
            // any app that support this intent.
            {
                it.setPackage(defaultSmsPackageName);
            }
            context.startActivity(it);

        }
        else // For early versions, do what worked for you before.
        {
            Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("address",contact);
            context.startActivity(smsIntent);
        }
    }


}
