package com.prigro.prigro.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.felipecsl.gifimageview.library.GifImageView;
import com.prigro.prigro.R;
import com.prigro.prigro.common.MyLog;

/**
 * Created by vienhui on 22/11/2017
 */

public class PopupAdsWindow extends PopupWindow {
    private boolean isGif = false;
    private byte[] imgvData;


    @Override
    public void dismiss() {
        super.dismiss();
    }

    public PopupAdsWindow(LayoutInflater layoutInflater, View parent, byte[] imgvData,boolean isGif) {
        super();

        setContentView(layoutInflater.inflate((isGif ? R.layout.view_pop_up_ads_gif : R.layout.view_pop_up_ads_jpg), null));
        setWidth(FrameLayout.LayoutParams.MATCH_PARENT);
        setHeight(FrameLayout.LayoutParams.MATCH_PARENT);
        setAnimationStyle(R.style.anim_menu_bottombar);
        showAtLocation(parent, Gravity.CENTER, 0, 0);
        this.imgvData = imgvData;
        this.isGif = isGif;
    }

    public PopupAdsWindow(){


    }

    public void popUpWindow() {

        countDownTimer();
        if (isGif) {

            gifInitial(imgvData);
        }else {
            loadImage();

        }

    }


    public boolean isShow() {

        return isShowing();

    }

    public void dismissAds() {

        dismiss();
    }

    private void countDownTimer() {
        final TextView countDown = (TextView) getContentView().findViewById(R.id.tv_count_down);
        new CountDownTimer(4000, 1000) {

            public void onTick(long millisUntilFinished) {
                countDown.setText(millisUntilFinished / 1000 + "sec");
            }

            public void onFinish() {
                countDown.setText("Skip");
                countDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
            }
        }.start();


    }

    private void gifInitial(byte[] url) {
        GifImageView  gifAds = (GifImageView) getContentView().findViewById(R.id.giv_ads);
        gifAds.setOnFrameAvailable(new GifImageView.OnFrameAvailable() {
            @Override
            public Bitmap onFrameAvailable(Bitmap bitmap) {
                return bitmap;
            }
        });
        gifAds.setBytes(url);
        gifAds.startAnimation();
//        .execute("http://katemobile.ru/tmp/sample3.gif");
    }


    private void loadImage() {
      ImageView networkImageView = (ImageView) getContentView().findViewById(R.id.iv_ads);

        MyLog.d("imgvData :: "+imgvData);

        MyLog.d("networkImageView :: "+networkImageView);
        if (imgvData!=null) {
            networkImageView.setImageBitmap(BitmapFactory.decodeByteArray(imgvData, 0, imgvData.length));
        }
    }

}
