package com.prigro.prigro.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.common.MyLog;

import java.lang.reflect.Field;

/**
 * Created by vienhui on 30/04/2017
 */

public class MyEditText extends LinearLayout {


    private TextView tvTitle, tvDetail;
    private EditText etDetail;
    private TypedArray typedArray;

    public MyEditText(Context context) {
        super(context);
    }

    public MyEditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyEditText);
        initView(context);
    }

    private void initView(Context context) {
        setOrientation(LinearLayout.VERTICAL);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        int paddingLeftRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources()
                .getDisplayMetrics());
        int paddinfTopBottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, getResources()
                .getDisplayMetrics());
        setPadding(paddingLeftRight, paddinfTopBottom, paddingLeftRight, paddinfTopBottom);
        setLayoutParams(layoutParams);
        initTitle(context);
        if (typedArray.getBoolean(R.styleable.MyEditText_blnIsFixed, false)) {

            MyLog.d("Fixed");
            initFixedDetail(context);


        } else {

            MyLog.d("Edit");
            initEditDetail(context);
        }
        requestLayout();

    }

    public void initTitle(Context context) {
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources()
                .getDisplayMetrics());

        tvTitle = new TextView(context);
        tvTitle.setLayoutParams(layoutParams);
        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.MyEditText_tvTitle);
            tvTitle.setText(title != null ? title : context.getString(R.string.otherInfo));
        }
        tvTitle.setTextColor(context.getResources().getColor(android.R.color.black));
        tvTitle.setTypeface(Typeface.DEFAULT_BOLD);
        tvTitle.setTextSize(14f);
        tvTitle.setPadding(padding, padding, padding, padding);
        addView(tvTitle);
    }

    public void initEditDetail(Context context) {

        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources()
                .getDisplayMetrics());
        etDetail = new EditText(context);
        etDetail.setTextColor(context.getResources().getColor(android.R.color.black));
        etDetail.setBackgroundColor(context.getResources().getColor(R.color.backgroundGrey));
        etDetail.setLayoutParams(layoutParams);
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(etDetail, R.drawable.ic_cursor);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();

        } catch (IllegalAccessException e1) {

            e1.printStackTrace();
        }

        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.MyEditText_etHint);
            etDetail.setHint(title != null ? title : "");

            int maxLength = typedArray.getInt(R.styleable.MyEditText_intMaxLength,-1);
            setInputLength(maxLength);
        }
        etDetail.setPadding(padding, padding, padding, padding);
        addView(etDetail);
    }


    public void initFixedDetail(Context context) {

        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources()
                .getDisplayMetrics());
        tvDetail = new TextView(context);
        tvDetail.setTextColor(context.getResources().getColor(android.R.color.black));
        tvDetail.setBackgroundColor(context.getResources().getColor(R.color.backgroundGrey));
        tvDetail.setLayoutParams(layoutParams);

        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.MyEditText_etHint);
            tvDetail.setHint(title != null ? title : "");
        }
        tvDetail.setPadding(padding, padding, padding, padding);
        addView(tvDetail);
    }


    public void setTitle(Spanned str) {
        tvTitle.setText(str);

    }

    public void setDetail(String detail) {
        String data = detail != null ? detail : "";
        if (etDetail != null) {
            etDetail.setText(data);
        } else {
            tvDetail.setText(data);

        }

    }

    public String getDetail() {
        if (etDetail != null) {
            return etDetail.getText().toString();
        } else {
            return tvDetail.getText().toString();
        }

    }

    public String getTitle() {
        return tvTitle.getText().toString();

    }


    public void setInputType(int inputType) {

        etDetail.setInputType(inputType);

    }

    public void setPasswordInput() {
        etDetail.setTransformationMethod(PasswordTransformationMethod.getInstance());

    }

    public void setInputLength(int maxLength) {

        if (etDetail != null&&maxLength!=-1) {

            etDetail.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});

        }
    }

    public void setOnDetailClickListener(OnClickListener onClickListener){
        if (tvDetail!=null){

            tvDetail.setOnClickListener(onClickListener);
        }

    }
}
