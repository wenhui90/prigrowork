package com.prigro.prigro.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prigro.prigro.R;

/**
 * Created by SzHui on 2017/4/8
 */

public class ListingDetailTextView extends LinearLayout{

    private TextView tvTitle,tvDetail;
    private TypedArray typedArray;

    public ListingDetailTextView(Context context) {
        super(context);
    }

    public ListingDetailTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        typedArray  = context.obtainStyledAttributes(attrs,R.styleable.listingDetailTextView);
        initView(context);

    }

    private void initView(Context context){
        setOrientation(LinearLayout.VERTICAL);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
        int paddingLeftRight =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources()
                .getDisplayMetrics());
        int paddinfTopBottom =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, getResources()
                .getDisplayMetrics());
        setPadding(paddingLeftRight,paddinfTopBottom,paddingLeftRight,paddinfTopBottom);
        setLayoutParams(layoutParams);
        initTitle(context);
        initDetail(context);
        requestLayout();

    }

    public void initTitle(Context context){
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
        int padding =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources()
                        .getDisplayMetrics());

        tvTitle = new TextView(context);
        tvTitle.setBackgroundColor(context.getResources().getColor(R.color.backgroundGrey));
        tvTitle.setLayoutParams(layoutParams);
        if (typedArray!=null){
            String title = typedArray.getString(R.styleable.listingDetailTextView_tvTitle);
            tvTitle.setText(title!=null?title:context.getString(R.string.otherInfo));
        }
        tvTitle.setTextColor(context.getResources().getColor(android.R.color.black));
        tvTitle.setTypeface(Typeface.DEFAULT_BOLD);
        tvTitle.setPadding(padding,padding,padding,padding);
        addView(tvTitle);
    }

    public void initDetail(Context context){

        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
        int padding =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources()
                .getDisplayMetrics());
        tvDetail = new TextView(context);
        tvDetail.setTextColor(context.getResources().getColor(android.R.color.black));
        tvDetail.setLayoutParams(layoutParams);
        if (typedArray!=null){
            String title = typedArray.getString(R.styleable.listingDetailTextView_tvDetail);
            tvDetail.setText(title!=null?title:"-");
            tvDetail.setVisibility(typedArray.getBoolean(R.styleable.listingDetailTextView_blnIsVisibleDetail,true)?VISIBLE:GONE);
        }
        tvDetail.setPadding(padding,padding,padding,padding);
        addView(tvDetail);
    }

    public void setDetail(String detail){
        tvDetail.setText(detail==null?"-":detail);
    }


    public void setDetailVisible(boolean isVisible){
        tvDetail.setVisibility(isVisible?VISIBLE:GONE);
    }
}
