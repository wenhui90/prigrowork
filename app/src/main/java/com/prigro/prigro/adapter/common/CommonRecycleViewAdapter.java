package com.prigro.prigro.adapter.common;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.prigro.prigro.R;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.network.CustomVolleyRequest;

import java.util.List;

/**
 * Created by SzHui on 2015/2/8
 */
public abstract class   CommonRecycleViewAdapter<T> extends RecyclerView.Adapter<CommonRecycleViewHolder> implements View.OnClickListener{

    public List mListData;
    protected CommonRecycleViewHolder p_viewHolder;

    public interface onItemClickCallBack{

        void onItemClick(int position);

    }

    public interface onViewClickCallBack{

        void onItemClick(View v);

    }

    protected onItemClickCallBack onItemClickCallBack;
    protected onViewClickCallBack onViewClickCallBack;

    public abstract void convert(CommonRecycleViewHolder p_viewHolder, int p_intPosition);

    public abstract int getConvertViewID();

    public CommonRecycleViewAdapter(List mListData) {
        this.mListData = mListData;
    }

    @Override
    public CommonRecycleViewHolder onCreateViewHolder(ViewGroup p_groupParent, int p_intViewType) {

        final View view = LayoutInflater.from(p_groupParent.getContext()).inflate(getConvertViewID(), p_groupParent, false);
        return new CommonRecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommonRecycleViewHolder p_viewHolder, int position) {
        convert(p_viewHolder, position);
    }

    @Override
    public int getItemCount() {

        MyLog.d("item size :: "+mListData.size());
        return mListData.size();
    }

    public void setOnItemClickCallBack(onItemClickCallBack onItemClickCallBack){
        this.onItemClickCallBack = onItemClickCallBack;
    }

    public void setOnViewClickCallBack(onViewClickCallBack onItemClickCallBack){

        this.onViewClickCallBack = onItemClickCallBack;
    }

    @Override
    public void onClick(View v) {
        if (onItemClickCallBack!=null)
            onItemClickCallBack.onItemClick((int)v.getTag());
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public void loadImage(Context context, NetworkImageView imageView, String url) {

        ImageLoader imageLoader = CustomVolleyRequest.getInstance(context)
                .getImageLoader();
        imageLoader.get(url, ImageLoader.getImageListener(imageView,R.color.bg_gray, android.R.drawable
                        .ic_dialog_alert));
        imageView.setImageUrl(url, imageLoader);
    }
}