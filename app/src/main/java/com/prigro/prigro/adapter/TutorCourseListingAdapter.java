package com.prigro.prigro.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.prigro.prigro.R;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewHolder;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.TuitionCenterBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;

import java.util.List;

/**
 * Created by vienhui on 30/04/2017
 */


public class TutorCourseListingAdapter extends CommonRecycleViewAdapter {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public static final int DATA_TYPE_TUTOR_LIST = 0;
    public static final int DATA_TYPE_CENTER_LIST = 1;

    private boolean isGuest;
    private int lastVisibleItem, totalItemCount;
    private int visibleThreshold = 1;
    private int dataType = DATA_TYPE_TUTOR_LIST;
    private boolean isLoading = false;
    private Context context;
    public interface OnLoadMoreListener {

        void onLoadMoreCallBack();

    }

    private OnLoadMoreListener mOnLoadMoreListener;

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    public TutorCourseListingAdapter(List mListData, boolean isGuest,Context context) {
        super(mListData);
        this.isGuest = isGuest;
        this.context =context;

    }

    public TutorCourseListingAdapter(List mListData, RecyclerView mRecyclerView, Context context) {
        super(mListData);
        this.context = context;
        refreshRecyclerView(mRecyclerView);

    }

    public TutorCourseListingAdapter(List mListData, RecyclerView mRecyclerView, Context context,int dataType) {

        super(mListData);
        this.context = context;
        MyLog.d("mListData size :: "+mListData.size());
        MyLog.d("dataType :: "+dataType);
        this.dataType = dataType;
        refreshRecyclerView(mRecyclerView);

    }

    @Override
    public CommonRecycleViewHolder onCreateViewHolder(ViewGroup p_groupParent, int p_intViewType) {
        if (p_intViewType == VIEW_TYPE_ITEM) {
            final View view = LayoutInflater.from(p_groupParent.getContext()).inflate(getConvertViewID(), p_groupParent, false);
            return new CommonRecycleViewHolder(view);
        } else {
            final View view = LayoutInflater.from(p_groupParent.getContext()).inflate(R.layout.view_loading_item, p_groupParent, false);
            return new CommonRecycleViewHolder(view);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void convert(CommonRecycleViewHolder p_viewHolder, int p_intPosition) {
        MyLog.d("Convert dataType :: "+dataType);
        if (dataType==DATA_TYPE_TUTOR_LIST){

            getConvertTutorList(p_viewHolder,p_intPosition);
        }else {

            getConvertCenterList(p_viewHolder,p_intPosition);
        }
    }

    @Override
    public int getConvertViewID() {


        return dataType==DATA_TYPE_TUTOR_LIST?R.layout.view_tutor_course_listing:R.layout.view_tuition_center_item;
    }


    @Override
    public int getItemViewType(int position) {

        return mListData.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public Boolean isLoading(){

        return isLoading;
    }

    private void refreshRecyclerView(RecyclerView mRecyclerView){
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMoreCallBack();
                    }
                    isLoading = true;

                }else if (lastVisibleItem<totalItemCount-visibleThreshold){

                    isLoading = false;
                }

            }
        });
    }

    private void getConvertTutorList(CommonRecycleViewHolder p_viewHolder,int p_intPosition){

        if (getItemViewType(p_intPosition) == VIEW_TYPE_ITEM) {

            CourseBean courseTemp = (CourseBean) mListData.get(p_intPosition);
            View view = p_viewHolder.getConvertView();
            view.setOnClickListener(this);
            view.setTag(Integer.parseInt(courseTemp.getId()));

            Button btnApplyOnClick = ((Button) p_viewHolder.getConvertView().findViewById(R.id.btn_apply));
            TextView tvDistance = (TextView) p_viewHolder.getConvertView().findViewById(R.id.tv_distance);
            if (isGuest) {

                btnApplyOnClick.setVisibility(View.GONE);
            }
            if (courseTemp.getApplied()!=null&&!courseTemp.getApplied().equals(Constants.APPLIED)) {
                btnApplyOnClick.setEnabled(true);
                btnApplyOnClick.setOnClickListener(this);
                btnApplyOnClick.setTag(Integer.parseInt(courseTemp.getId()));
            } else {
                btnApplyOnClick.setEnabled(false);
            }
            ((TextView) p_viewHolder.getConvertView().findViewById(R.id.tv_title)).setText(courseTemp.getTitle());
            ((TextView) p_viewHolder.getConvertView().findViewById(R.id.tv_course)).setText(courseTemp.getSubject_name());
            tvDistance.setText(context.getString(R.string.distance)+Integer.toString(courseTemp.getDistance())+"km");
        } else {
            View view = p_viewHolder.getConvertView();
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
            progressBar.setIndeterminate(true);
        }

    }

    private void getConvertCenterList(CommonRecycleViewHolder p_viewHolder,int p_intPosition){
        if (getItemViewType(p_intPosition) == VIEW_TYPE_ITEM) {

            TuitionCenterBean tuitionTemp = (TuitionCenterBean) mListData.get(p_intPosition);
            View view = p_viewHolder.getConvertView();
            view.setOnClickListener(this);
            view.setTag(Integer.parseInt(tuitionTemp.getId()));
            TextView tvDistance = (TextView) p_viewHolder.getConvertView().findViewById(R.id.tv_distance);
            loadImage(context,
                    ((NetworkImageView)p_viewHolder.getConvertView().findViewById(R.id.networkImageView)),tuitionTemp.getThumbnail_url());
            ((TextView) p_viewHolder.getConvertView().findViewById(R.id.tv_title)).setText(tuitionTemp.getName());
            ((TextView) p_viewHolder.getConvertView().findViewById(R.id.tv_location)).setText(tuitionTemp.getLocation());
            tvDistance.setText(context.getString(R.string.distance)+((int)tuitionTemp.getDistance())+"km");
        } else {
            View view = p_viewHolder.getConvertView();
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
            progressBar.setIndeterminate(true);
        }
    }
}
