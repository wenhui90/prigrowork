package com.prigro.prigro.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewHolder;
import com.prigro.prigro.bean.CourseBean;

import java.util.List;

/**
 * Created by vienhui on 05/05/2017
 */

public class StdCourseListingAdapter extends CommonRecycleViewAdapter implements View.OnClickListener {

    private boolean isStudent = true;

    public StdCourseListingAdapter(List mListData) {
        super(mListData);
    }


    public StdCourseListingAdapter(List mListData,boolean isStudent) {
        super(mListData);
        this.isStudent = isStudent;


    }

    @Override
    public void convert(CommonRecycleViewHolder p_viewHolder, int p_intPosition) {
        CourseBean courseBean = (CourseBean) mListData.get(p_intPosition);
        p_viewHolder.getConvertView().setOnClickListener(this);
        p_viewHolder.getConvertView().setTag(Integer.parseInt(courseBean.getId()));
        ((TextView) p_viewHolder.getView(R.id.tv_title)).setText(courseBean.getTitle());
        ((TextView) p_viewHolder.getView(R.id.tv_subject)).setText(courseBean.getSubject_name() + " : " + courseBean.getLevel_name());
        if (isStudent) {
            ((TextView) p_viewHolder.getView(R.id.tv_location)).setText(courseBean.getAddress());
            ImageView ivDelete = p_viewHolder.getView(R.id.iv_delete);
            ivDelete.setVisibility(View.VISIBLE);
            ivDelete.setTag(courseBean.getId());
            ivDelete.setOnClickListener(this);

        }

    }
        @Override
        public int getConvertViewID () {
            return R.layout.view_std_listing;
        }

        @Override
        public void onClick (View v){

            switch (v.getId()) {
                case R.id.iv_delete:
                    if (onViewClickCallBack != null) {
                        onViewClickCallBack.onItemClick(v);
                    }
                    break;

                default:
                    if (onItemClickCallBack != null) {
                        onItemClickCallBack.onItemClick((int) v.getTag());
                    }
                    break;
            }
        }
    }
