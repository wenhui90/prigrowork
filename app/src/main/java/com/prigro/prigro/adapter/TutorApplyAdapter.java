package com.prigro.prigro.adapter;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.activity.StdCourseDetailActivity;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewHolder;
import com.prigro.prigro.bean.UserBean;

import java.util.List;


/**
 * Created by vienhui on 27/05/2017
 */

public class TutorApplyAdapter extends CommonRecycleViewAdapter{

    public TutorApplyAdapter(List mListData) {
        super(mListData);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void convert(CommonRecycleViewHolder p_viewHolder, int p_intPosition) {
//        this.p_viewHolder =p_viewHolder;
        p_viewHolder.getConvertView().setTag(p_intPosition);
        p_viewHolder.getConvertView().setOnClickListener(this);
        UserBean userTemp = (UserBean) mListData.get(p_intPosition);
        Button btnApplyOnClick = ((Button) p_viewHolder.getConvertView().findViewById(R.id.btn_apply));
        btnApplyOnClick.setText(R.string.approve);
        if (!userTemp.getApplyStatus().equals(StdCourseDetailActivity.APPROVE)){

            btnApplyOnClick.setOnClickListener(this);
            btnApplyOnClick.setTag(p_intPosition);

        }else {
            btnApplyOnClick.setEnabled(false);
        }

        p_viewHolder.getConvertView().findViewById(R.id.tv_distance).setVisibility(View.GONE);
        ((TextView) p_viewHolder.getConvertView().findViewById(R.id.tv_title)).setText("Name : "+userTemp.getName());
        ((TextView) p_viewHolder.getConvertView().findViewById(R.id.tv_course)).setText("Rate : RM"+userTemp.getRates());
    }

    @Override
    public int getConvertViewID() {
        return R.layout.view_tutor_course_listing;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_apply:
                if (onViewClickCallBack!=null){
                    onViewClickCallBack.onItemClick(v);
                }
                break;

            default:
                if (onItemClickCallBack!=null){
                    onItemClickCallBack.onItemClick((int)v.getTag());
                }
                break;

        }

    }
}
