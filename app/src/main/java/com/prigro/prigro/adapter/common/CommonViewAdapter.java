package com.prigro.prigro.adapter.common;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

public abstract class CommonViewAdapter<T> extends BaseAdapter
{
    protected LayoutInflater mInflater;
    protected Context g_context;
    protected List<T> g_listData;
    protected final int g_intLayoutID;

    public CommonViewAdapter(Context p_context, List<T> p_listData, int p_intLayoutID)
    {
        this.g_context = p_context;
        this.mInflater = LayoutInflater.from(g_context);
        this.g_listData = p_listData;
        this.g_intLayoutID = p_intLayoutID;
    }

    @Override
    public int getCount()
    {
        return g_listData.size();
    }

    @Override
    public T getItem(int position)
    {
        return g_listData.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final CommonViewHolder viewHolder = getViewHolder(position, convertView,
                parent);
        convert(viewHolder, position);
        return viewHolder.getConvertView();

    }

    public abstract void convert(CommonViewHolder helper,int p_intPosition);

    private CommonViewHolder getViewHolder(int position, View convertView,
                                           ViewGroup parent)
    {
        return CommonViewHolder.get(g_context, convertView, parent, g_intLayoutID,
                position);
    }

}