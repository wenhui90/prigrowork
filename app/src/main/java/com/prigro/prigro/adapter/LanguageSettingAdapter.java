package com.prigro.prigro.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewHolder;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.utils.Shared;

import java.util.List;

/**
 * Created by vienhui on 16/09/2017
 */

public class LanguageSettingAdapter extends CommonRecycleViewAdapter {

    public LanguageSettingAdapter(List mListData) {
        super(mListData);
    }

    @Override
    public void convert(CommonRecycleViewHolder p_viewHolder, int p_intPosition) {
        String language = (String) mListData.get(p_intPosition);
        p_viewHolder.getConvertView().setTag(p_intPosition);
        p_viewHolder.getConvertView().setOnClickListener(this);
        TextView tvLanguage = p_viewHolder.getView(R.id.tv_language);
        tvLanguage.setText(language);
        ImageView iv_tick = p_viewHolder.getView(R.id.iv_tick);
        if (Shared.readInt(Constants.SP_LANGUAGE,Constants.LANGUAGE_DEFAULT)!=-1&&Shared.readInt(Constants.SP_LANGUAGE,Constants.LANGUAGE_DEFAULT)==p_intPosition){

            iv_tick.setImageResource(R.drawable.ic_bluetick);

        }else if (Shared.readInt(Constants.SP_LANGUAGE,Constants.LANGUAGE_DEFAULT)==-1&&p_intPosition==0){

            iv_tick.setImageResource(R.drawable.ic_bluetick);
        }


    }

    @Override
    public int getConvertViewID() {
        return R.layout.view_setting_item;
    }

}
