package com.prigro.prigro.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewHolder;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.common.MyLog;

import java.util.List;

/**
 * Created by vienhui on 16/06/2017
 */

public class ApproveCourseListingAdapter extends CommonRecycleViewAdapter {


    public ApproveCourseListingAdapter(List mListData) {
        super(mListData);
    }

    @Override
    public void convert(CommonRecycleViewHolder p_viewHolder, int p_intPosition) {
        MyLog.d("p_intPosition :: " + p_intPosition);
        CourseBean courseBean = (CourseBean) mListData.get(p_intPosition);
        TextView tvTitle = p_viewHolder.getView(R.id.tv_title);
        Button btnContactStudent = p_viewHolder.getView(R.id.btn_contact_student);
        tvTitle.setText(courseBean.getTitle() + " - " + courseBean.getSubject_name());

        p_viewHolder.getConvertView().setTag(Integer.parseInt(courseBean.getId()));
        p_viewHolder.getConvertView().setOnClickListener(this);
        if (courseBean.getSeen()!=null&&courseBean.getSeen().equals("0")){

            p_viewHolder.getConvertView().setBackgroundResource(R.color.highlightBlue);
        }

        btnContactStudent.setTag(Integer.parseInt(courseBean.getStudent_id()));
        btnContactStudent.setOnClickListener(this);
    }

    @Override
    public int getConvertViewID() {
        return R.layout.view_approve_listing;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_contact_student:
                if (onViewClickCallBack != null)
                    onViewClickCallBack.onItemClick(v);
                break;

            default:

                if (onItemClickCallBack != null)
                    onItemClickCallBack.onItemClick((int) v.getTag());

                break;

        }
    }

}
