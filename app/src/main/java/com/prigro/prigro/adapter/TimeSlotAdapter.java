package com.prigro.prigro.adapter;

import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewHolder;

import java.util.List;

/**
 * Created by vienhui on 19/05/2017
 */

public class TimeSlotAdapter extends CommonRecycleViewAdapter {
    public TimeSlotAdapter(List mListData) {
        super(mListData);
    }

    @Override
    public void convert(CommonRecycleViewHolder p_viewHolder, int p_intPosition) {

        TextView tvTitle = (TextView) p_viewHolder.getView(R.id.tv_title);
        tvTitle.setTag(p_intPosition);
        tvTitle.setOnClickListener(this);

    }

    @Override
    public int getConvertViewID() {
        return R.layout.view_only_text;
    }
}
