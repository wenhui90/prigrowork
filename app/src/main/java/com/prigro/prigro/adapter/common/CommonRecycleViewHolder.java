package com.prigro.prigro.adapter.common;


import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CommonRecycleViewHolder extends RecyclerView.ViewHolder
{
    private final SparseArray<View> mViews;
    private View mConvertView;

    public CommonRecycleViewHolder(View itemView) {
        super(itemView);
        mViews = new SparseArray<>();
        mConvertView = itemView;
        itemView.setTag(this);
    }


    public View getConvertView()
    {
        return mConvertView;
    }

    public static CommonRecycleViewHolder get(View convertView) {

        if (convertView == null) {
            return new CommonRecycleViewHolder(convertView);
        }
        return (CommonRecycleViewHolder) convertView.getTag();
    }

    public <T extends View> T getView(int viewId)
    {

        View view = mViews.get(viewId);
        if (view == null)
        {
            view = mConvertView.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }


    public void setText(int p_intViewID,String p_strText){

        TextView l_tvTemp =getView(p_intViewID);
        l_tvTemp.setText(p_strText);

    }

    public void setImageResource(int p_intViewID,Drawable p_drawableTemp){

        ImageView l_ivTemp = getView(p_intViewID);
        l_ivTemp.setImageDrawable(p_drawableTemp);

    }

    public void setImageResourceWidthHeight(int p_intViewID,int p_intWidth,int p_intHeight){

        ImageView l_ivTemp = getView(p_intViewID);
        l_ivTemp.getLayoutParams().width =p_intWidth;
        l_ivTemp.getLayoutParams().height =p_intHeight;

    }

}
