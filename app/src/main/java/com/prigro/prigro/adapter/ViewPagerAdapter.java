package com.prigro.prigro.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by SzHui on 2017/4/8
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> alFragmentList;

    public ViewPagerAdapter(FragmentManager fm, ArrayList<Fragment> alFragmentList) {
        super(fm);
        this.alFragmentList = alFragmentList;

    }

    @Override
    public Fragment getItem(int position) {

        return alFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return alFragmentList.size();
    }

}
