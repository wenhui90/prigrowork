package com.prigro.prigro.adapter;

import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewHolder;
import com.prigro.prigro.bean.NotificationBean;

import java.util.List;

/**
 * Created by vienhui on 16/06/2017
 */

public class NotificationListingAdapter extends CommonRecycleViewAdapter {

    public NotificationListingAdapter(List mListData) {
        super(mListData);
    }

    @Override
    public void convert(CommonRecycleViewHolder p_viewHolder, int p_intPosition) {


      NotificationBean notificationBean= (NotificationBean) mListData.get(p_intPosition);
        p_viewHolder.getConvertView().setOnClickListener(this);
        p_viewHolder.getConvertView().setTag(Integer.parseInt(notificationBean.getListing_id()));
        ((TextView)p_viewHolder.getView(R.id.tv_notice)).setText(notificationBean.getNotice());

    }

    @Override
    public int getConvertViewID() {
        return R.layout.view_notification;
    }

}
