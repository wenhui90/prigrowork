package com.prigro.prigro.network;

import android.content.Context;
import android.os.Message;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prigro.prigro.common.CommonHandler;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.network.custom_exception.CNoNetworkException;
import com.prigro.prigro.network.custom_exception.CTimeOutException;

import java.util.Map;


/**
 * Created by SzHui on 2016/1/21
 */
public class NetworkHelper {


//    private String mUrl = "http://prigro.advisoryapps.com/index.php/";
   private String mUrl = "https://prigro.com.my/index.php/";
    private RequestQueue mRequestQueue;

    private Context mContext;
    private CommonHandler mHandler;


    public NetworkHelper(Context mContext, CommonHandler mHandler) {
        this.mContext = mContext;
        this.mHandler = mHandler;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return mRequestQueue;
    }

    public void postAsString(final Map<String, String> mParams, String requestMethod) {

        if (!NetworkUtils.isNetworkAvailable(mContext)) {
            mHandler.createExceptionMessage(new CNoNetworkException());
            return;
        }
        StringRequest request = new StringRequest(Request.Method.POST, mUrl + requestMethod, resListener, errorListener) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                MyLog.d("mParams :: " + mParams);
                return mParams;
            }

        };
        MyLog.d("request :: " + request);

        request.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(request);

    }

    /**
     * 响应回调
     */
    private Response.Listener<String> resListener = new Response.Listener<String>() {

        @Override
        public void onResponse(String response) {
            Message msg = mHandler.obtainMessage();
            msg.obj = response;
            mHandler.handleMessage(msg);
        }
    };

    /**
     * 错误回调
     */
    private Response.ErrorListener errorListener = new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError error) {
            if (error instanceof TimeoutError) {
                mHandler.createExceptionMessage(new CTimeOutException());
            } else if (error instanceof NoConnectionError) {
                mHandler.createExceptionMessage(new CNoNetworkException());
            } else {
                mHandler.createExceptionMessage(new NetworkError());
            }
        }
    };

}
