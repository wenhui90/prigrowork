package com.prigro.prigro.network;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by SzHui on 2016/1/22
 */
public class NetworkUtils {

    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

}
