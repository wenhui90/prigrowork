package com.prigro.prigro.bean;

import java.util.ArrayList;

/**
 * Created by vienhui on 16/06/2017
 */

public class CourseListBean {


    private ArrayList<CourseBean> courseBeans;
    private int endPoint;

    public ArrayList<CourseBean> getCourseBeans() {

        if (courseBeans==null){
            courseBeans = new ArrayList<>();
        }

        return courseBeans;
    }
    public int getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(int endPoint) {
        this.endPoint = endPoint;
    }


}
