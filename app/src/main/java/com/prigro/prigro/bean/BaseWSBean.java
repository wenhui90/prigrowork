package com.prigro.prigro.bean;

import java.io.Serializable;

/**
 * Created by SzHui on 2016/8/29
 */
public class BaseWSBean implements Serializable {

    StatusBean status;

    public String getStatusCode(){

      return  status.getCode();
    }

    public String getStatusMessage(){

        return  status.getMessage();
    }
}
