package com.prigro.prigro.bean;

import java.io.Serializable;

/**
 * Created by vienhui on 13/06/2017
 */

public class GuestBean implements Serializable {

    private String index;
    private String longitude;
    private String latitude;


    public void setIndex(String index) {
        this.index = index;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

}
