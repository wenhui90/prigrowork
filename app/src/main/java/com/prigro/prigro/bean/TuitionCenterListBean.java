package com.prigro.prigro.bean;

import java.util.ArrayList;

/**
 * Created by vienhui on 19/11/2017
 */

public class TuitionCenterListBean {


    private ArrayList<TuitionCenterBean> tuitionCenterBeans;
    private int endPoint;

    public ArrayList<TuitionCenterBean> getCourseBeans() {

        if (tuitionCenterBeans==null){
            tuitionCenterBeans = new ArrayList<>();
        }

        return tuitionCenterBeans;
    }
    public int getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(int endPoint) {
        this.endPoint = endPoint;
    }
}
