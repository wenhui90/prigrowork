package com.prigro.prigro.bean;

/**
 * Created by vienhui on 14/06/2017
 */

public class FAQBean {

    private String learner;
    private String educator;
    private String disclaimer;

    public String getLearner() {
        return learner;
    }


    public String getEducator() {
        return educator;
    }


    public String getDisclaimer() {
        return disclaimer;
    }



}
