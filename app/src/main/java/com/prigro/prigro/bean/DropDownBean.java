package com.prigro.prigro.bean;

import java.io.Serializable;

/**
 * Created by vienhui on 07/06/2017
 */

public class DropDownBean implements Serializable{

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
