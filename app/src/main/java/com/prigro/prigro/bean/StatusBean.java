package com.prigro.prigro.bean;

import java.io.Serializable;

/**
 * Created by SzHui on 2016/8/29
 */
public class StatusBean implements Serializable{

    private String code;
    private String message;

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }

}
