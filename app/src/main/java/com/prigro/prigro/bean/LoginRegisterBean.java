package com.prigro.prigro.bean;

/**
 * Created by SzHui on 2017/4/22
 */

public class LoginRegisterBean extends BaseWSBean {

    private String email;
    private String password;
    private String contact;
    private String age;

    private String gender;
    private String token;
    private String id;

    private int race_id;
    private int type;

    private String nationality_id;

    public String getNationality_id() {
        return nationality_id;
    }

    public void setNationality_id(String nationality_id) {
        this.nationality_id = nationality_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String name;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public LoginRegisterBean(String email, String password) {

        this.email = email;
        this.password = password;

    }

    public LoginRegisterBean() {


    }


    public void setContact(String contact) {
        this.contact = contact;
    }


    public void setAge(String age) {
        this.age = age;
    }


    public void setRace(int race) {
        this.race_id = race;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void setPassword(String password) {
        this.password = password;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


}
