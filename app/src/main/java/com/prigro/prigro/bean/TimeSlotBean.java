package com.prigro.prigro.bean;

import java.io.Serializable;

/**
 * Created by vienhui on 23/05/2017
 */

public class TimeSlotBean implements Serializable {


   private String day,start_time,end_time;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

}
