package com.prigro.prigro.bean;

/**
 * Created by vienhui on 19/11/2017
 */

public class TuitionCenterBean extends BaseWSBean {

    private String id;
    private String name;
    private String location;
    private String thumbnail_url;
    private double distance;
    private String contact;
    private String banner_url;
    private String address;
    private String website;
    private String description;

    public String getContact() {
        return contact;
    }

    public String getWebsite() {
        return website;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getBanner_url() {
        return banner_url;
    }

    public void setBanner_url(String banner_url) {
        this.banner_url = banner_url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public int getDistance() {
        return (int)distance;
    }


}
