package com.prigro.prigro.bean;

import java.io.Serializable;

/**
 * Created by vienhui on 08/06/2017
 */

public class DropDownContentBean implements Serializable {

    private DropDownBean[] qualification;
    private DropDownBean[] lesson_type;
    private DropDownBean[] nationality;
    private DropDownBean[] category;
    private DropDownBean[] subject;
    private DropDownBean[] level;
    private DropDownBean[] race;


    public DropDownBean[] getQualification() {
        return qualification;
    }

    public DropDownBean[] getLesson_type() {
        return lesson_type;
    }


    public DropDownBean[] getNationality() {
        return nationality;
    }


    public DropDownBean[] getCategory() {
        return category;
    }


    public DropDownBean[] getSubject() {
        return subject;
    }


    public DropDownBean[] getLevel() {
        return level;
    }


    public DropDownBean[] getRace() {
        return race;
    }



}
