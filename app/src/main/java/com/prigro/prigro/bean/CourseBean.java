package com.prigro.prigro.bean;

import java.io.Serializable;

/**
 * Created by vienhui on 30/04/2017
 */

public class CourseBean implements Serializable {


    private String id;
    private String title;
    private String latitude;
    private String longitude;
    private String address;
    private String location;
    private String subject_name;
    private String level_name;
    private String lesson_type_name;
    private String notes;
    private String gender;
    private String rates;
    private String student_id;
    private String preferred_gender;
    private String applied;
    private String accepted;
    private String hour_per_lesson;

    public String getBudget_per_month() {
        return budget_per_month;
    }

    private String budget_per_month;

    private int subject_id;
    private int level_id;
    private int lesson_per_week;
    private int lesson_type;

    private double distance;

    private UserBean[] tutors;

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    private String seen;

    public String getAccepted() {
        return accepted;
    }


    public int getDistance() {
        return (int)distance;
    }


    public String getPreferred_gender() {
        return preferred_gender;
    }

    public String getRates() {
        return rates;
    }

    public TimeSlotBean[] getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(TimeSlotBean[] timeslot) {
        this.timeslot = timeslot;
    }

    private TimeSlotBean[] timeslot;

    public String getLesson_type_name() {
        return lesson_type_name;
    }

    public int getLesson_type() {
        return lesson_type;
    }

    public void setLesson_type(int lesson_type) {
        this.lesson_type = lesson_type;
    }


    public String getHour_per_lesson() {
        return hour_per_lesson;
    }

    public void setHour_per_lesson(String hour_per_lesson) {
        this.hour_per_lesson = hour_per_lesson;
    }

    public String getStudent_id() {
        return student_id;
    }

    public UserBean[] getTutors() {
        return tutors;
    }

    public void setTutors(UserBean[] tutors) {
        this.tutors = tutors;
    }

    public String getApplied() {
        return applied;
    }

    public void setApplied(String applied) {
        this.applied = applied;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLesson_per_week() {
        return lesson_per_week;
    }

    public void setLesson_per_week(int lesson_per_week) {
        this.lesson_per_week = lesson_per_week;
    }

    public String getNotes() {
        return notes;
    }

    public String getAddress() {
        return address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject) {
        this.subject_id = subject;
    }

    public int getLevel_id() {
        return level_id;
    }

    public void setLevel_id(int level) {
        this.level_id = level;
    }

    public String getSubject_name() {
        return subject_name;
    }



    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    public String getLevel_name() {
        return level_name;
    }
}
