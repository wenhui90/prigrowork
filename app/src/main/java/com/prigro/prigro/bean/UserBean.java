package com.prigro.prigro.bean;

import java.io.Serializable;

/**
 * Created by SzHui on 2016/8/8
 */
public class UserBean extends BaseWSBean implements Serializable {

    private String email;
    private String gender;
    private String created_at;
    private String updated_at;
    private String seen_at;
    private String contact;
    private String profile_picture;
    private String country;
    private String age;
    private String race_id;
    private String nric;


    private String budget_per_month;


    private String address;


    private String race_name;
    private String password;
    private String confirm_password;
    private String account_status;
    private String longitude;
    private String latitude;


    // TUTOR
    private String job_type;
    private String expertise;
    private String experience;
    private String biography;
    private String description;
    private String rates;
    private String user_id;
    private String qualification_id;
    private String qualification_name;
    private String category_id;
    private String category_name;
    private String nationality_id;
    private String nationality_name;
    private String token;
    private String id;
    private String applyStatus;
    private String filePath;
    private String receipt;
    private String document;

    private TimeSlotBean[] timeslot;
    private CourseBean[] listing_apply;
    private String name;
    private int type;
    private int token_type;


    public String getReceipt() {
        return receipt;
    }


    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getToken_type() {
        return token_type;
    }


    public void setNric(String nric) {
        this.nric = nric;
    }

    public String getNric() {
        return nric;
    }

    public String getQualification_id() {
        return qualification_id;
    }

    public void setQualification_id(String qualification_id) {
        this.qualification_id = qualification_id;
    }

    public String getQualification_name() {
        return qualification_name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public String getNationality_id() {
        return nationality_id;
    }

    public void setNationality_id(String nationality_id) {
        this.nationality_id = nationality_id;
    }

    public String getNationality_name() {
        return nationality_name;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setApplyStatus(String applyStatus) {
        this.applyStatus = applyStatus;
    }

    public String getApplyStatus() {
        return applyStatus;
    }

    public String getDocument() {
        return document;
    }

    public String getRates() {
        return rates;
    }

    public String getUser_id() {
        return user_id;
    }


    public CourseBean[] getListing_approve() {
        return listing_approve;
    }

    public void setListing_approve(CourseBean[] listing_approve) {
        this.listing_approve = listing_approve;
    }

    private CourseBean[] listing_approve;

    public CourseBean[] getListing_apply() {
        return listing_apply;
    }

    public void setListing_apply(CourseBean[] listing_apply) {
        this.listing_apply = listing_apply;
    }

    public TimeSlotBean[] getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(TimeSlotBean[] timeslot) {
        this.timeslot = timeslot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getToken() {
        return token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getProfilePicture() {
        return profile_picture;
    }

    public void setProfilePicture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getAccount_status() {
        return account_status;
    }

    public String getAge() {

        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getRace_id() {
        return race_id;
    }

    public void setRace_id(String race_id) {
        this.race_id = race_id;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }


    public String getRace_name() {
        return race_name;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String addresss) {
        this.address = addresss;
    }


    public String getBudget() {
        return budget_per_month;
    }

    public void setBudget(String budget) {
        this.budget_per_month = budget;
    }

}
