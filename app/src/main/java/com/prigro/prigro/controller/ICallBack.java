package com.prigro.prigro.controller;

/**
 * Created by SzHui on 2016/1/21
 */

public interface ICallBack {

    /**
     * @param key status
     * @param params any object pass to activity
     */
    void showResult(int key, Object... params);


    /**
     * @param key status
     * @param params any object pass to activity
     */
    void showException(int key, Object... params);


    /**
     * @param key status
     */
    void showUnknownError(int key);
}
