package com.prigro.prigro.fragment.main;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.activity.TutorCourseDetailActivity;
import com.prigro.prigro.adapter.TutorCourseListingAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.CourseListBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.fragment.base.BaseFragment;
import com.prigro.prigro.fragment.base.BaseWSFragment;
import com.prigro.prigro.model.SearchModel;
import com.prigro.prigro.model.StdCourseDetailModel;
import com.prigro.prigro.model.TutorCourseModel;
import com.prigro.prigro.utils.GPSTracker;

import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by SzHui on 2017/4/15
 */

public class TutorCourseListingFragment extends BaseWSFragment implements CommonRecycleViewAdapter.onItemClickCallBack, TutorCourseListingAdapter.OnLoadMoreListener {

    private static TutorCourseListingFragment tutorCourseListingFragment;
    private final int MAX_DATA_LOAD = 25;

    @InjectView(R.id.list_course)
    RecyclerView rvCourseListing;
    @InjectView(R.id.pb_loading)
    ProgressBar pbLoading;
    @InjectView(R.id.tv_no_data)
    TextView tvNoData;
    @InjectView(R.id.sv_list)
    SearchView svSearchCourse;
    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private ArrayList<CourseBean> tutorCourseBeans;
    private TutorCourseListingAdapter tutorCourseListingAdapter;
    private GPSTracker gpsTracker;
    private Snackbar snackbar;
    private int indexEndPoint;
    private String latitude = "";
    private String longitude = "";

    public static BaseFragment getInstance() {

        if (tutorCourseListingFragment == null) {
            tutorCourseListingFragment = new TutorCourseListingFragment();
        }
        return tutorCourseListingFragment;

    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_tutor_course_listing;
    }

    @Override
    protected void initData() {
        tutorCourseBeans = new ArrayList<>();

    }

    @Override
    protected void initView() {

        rvCourseListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        tutorCourseListingAdapter = new TutorCourseListingAdapter(tutorCourseBeans, rvCourseListing,getActivity());
        rvCourseListing.setAdapter(tutorCourseListingAdapter);

    }

    @Override
    protected void initListener() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (indexEndPoint<MAX_DATA_LOAD||!tutorCourseListingAdapter.isLoading()) {

                    getTutorList(true);

                } else {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        svSearchCourse.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        if (gpsTracker.getIsGPSTrackingEnabled()) {
                            latitude = String.valueOf(gpsTracker.getLatitude()).startsWith("0")?latitude:String.valueOf(gpsTracker.getLatitude());
                            longitude = String.valueOf(gpsTracker.getLongitude()).startsWith("0")?longitude:String.valueOf(gpsTracker.getLongitude());
                        }
                        initialGPS();
                        showLoadingDialog();
                        SearchModel searchModel = new SearchModel((BaseActivity) getActivity(), query, longitude, latitude);
                        searchModel.setICallBackListener(TutorCourseListingFragment.this);
                        searchModel.executeData();
                    }
                });
                return false;

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        tutorCourseListingAdapter.setOnItemClickCallBack(this);
        tutorCourseListingAdapter.setOnLoadMoreListener(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && tutorCourseBeans.size() <= 0 && tvNoData.getVisibility() == View.GONE) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvNoData.setVisibility(View.GONE);
                    rvCourseListing.setVisibility(View.GONE);
                    pbLoading.setVisibility(View.VISIBLE);
                }
            });
            getTutorList(true);
        }

    }

    @SuppressWarnings("uncheked")
    @Override
    public void showSuccessResult(int key, final Object... params) {
        dismissLoadingDialog();
        swipeRefreshLayout.setRefreshing(false);
        switch (key) {

            case Constants.STD_COURSE_DETAIL_MODEL:
                Intent intent = new Intent(getActivity(), TutorCourseDetailActivity.class);
                intent.putExtra(TutorCourseDetailActivity.KEY_COURSE_DETAIL, (CourseBean) params[0]);
                getActivity().startActivityForResult(intent, Constants.REQUEST_LISTING_CODE);
                break;
            case Constants.TUTOR_COURSE_MODEL:
                tutorCourseBeans.clear();

            case Constants.ADD_COURSE_LIST_MODEL:
                removeBottomPullLoading();
                CourseListBean courseListBean = (CourseListBean) params[0];
                pbLoading.setVisibility(View.GONE);
                rvCourseListing.setVisibility(View.VISIBLE);
                tutorCourseBeans.addAll(courseListBean.getCourseBeans());
                tutorCourseListingAdapter.notifyDataSetChanged();
                indexEndPoint = courseListBean.getEndPoint();
                snackbar = Snackbar
                        .make(getView(), R.string.refresh_success, Snackbar.LENGTH_SHORT);
                snackbar.show();
                if (tutorCourseBeans.size() != 0) {
                    tvNoData.setVisibility(View.GONE);

                }
                break;

            case Constants.SEARCH_MODEL:
                getActivity().getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
                pbLoading.setVisibility(View.GONE);
                rvCourseListing.setVisibility(View.VISIBLE);
                tutorCourseBeans.clear();
                tutorCourseBeans.addAll((ArrayList<CourseBean>) params[0]);
                tutorCourseListingAdapter.notifyDataSetChanged();
                indexEndPoint = tutorCourseBeans.size();
                tutorCourseListingAdapter.setLoaded();
                svSearchCourse.onActionViewCollapsed();
                rvCourseListing.requestFocus();
                removeBottomPullLoading();
                snackbar = Snackbar
                        .make(getView(), R.string.refresh_success, Snackbar.LENGTH_SHORT);
                snackbar.show();
                break;
        }

    }

    @Override
    public void showException(int key, Object... params) {
        super.showException(key, params);
        handlerError();
    }

    @Override
    public void showUnknownError(int key) {
        super.showUnknownError(key);
        handlerError();

    }

    @Override
    public void onItemClick(int position) {
        showLoadingDialog();
        StdCourseDetailModel stdCourseDetailModel = new StdCourseDetailModel((BaseActivity) getActivity(), Integer.toString(position));
        stdCourseDetailModel.setICallBackListener(this);
        stdCourseDetailModel.executeData();
    }

    @OnClick(R.id.sv_list)
    public void searchOnClick() {

        if (pbLoading.getVisibility() != View.VISIBLE) {

            svSearchCourse.onActionViewExpanded();
        }

    }

    private void getTutorList(final boolean isRefresh) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (gpsTracker.getIsGPSTrackingEnabled()) {
                    latitude = String.valueOf(gpsTracker.getLatitude()).startsWith("0")?latitude:String.valueOf(gpsTracker.getLatitude());
                    longitude = String.valueOf(gpsTracker.getLongitude()).startsWith("0")?longitude:String.valueOf(gpsTracker.getLongitude());
                }
                initialGPS();
                final TutorCourseModel tutorCourseModel = new TutorCourseModel((BaseActivity) getActivity(), longitude, latitude, isRefresh,tutorCourseBeans.size());
                tutorCourseModel.setICallBackListener(TutorCourseListingFragment.this);
                tutorCourseModel.executeData();
            }
        });
    }

    public void setListingIsApplied(String id) {

        for (CourseBean courseBean : tutorCourseBeans) {

            if (courseBean.getId().equals(id)) {
                courseBean.setApplied(Constants.APPLIED);
                tutorCourseListingAdapter.notifyDataSetChanged();
                return;
            }

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (gpsTracker != null)
            gpsTracker.stopUsingGPS();
    }

    @Override
    public void onResume() {
        super.onResume();
        gpsTracker = new GPSTracker(getActivity());
        if (!gpsTracker.getIsGPSTrackingEnabled()) {
            gpsTracker.showSettingsAlert();
        }
    }

    @OnClick(R.id.tv_no_data)
    public void noDataOnClick() {

        pbLoading.setVisibility(View.VISIBLE);
        tvNoData.setVisibility(View.GONE);
    }


    @Override
    public void onLoadMoreCallBack() {
        if (!swipeRefreshLayout.isRefreshing()&&tutorCourseBeans.get(tutorCourseBeans.size()-1)!=null&&tutorCourseBeans.size() < indexEndPoint) {

            tutorCourseBeans.add(null);
            tutorCourseListingAdapter.setLoaded();
            tutorCourseListingAdapter.notifyItemInserted(tutorCourseBeans.size() - 1);
            getTutorList(false);

        } else {
            snackbar = Snackbar
                    .make(getView(), R.string.no_more_list, Snackbar.LENGTH_SHORT);
            snackbar.show();
            tutorCourseListingAdapter.setLoaded();
        }
    }

    private void removeBottomPullLoading() {
        //Remove loading item
        if (tutorCourseBeans.size() != 0 && tutorCourseBeans.get(tutorCourseBeans.size() - 1) == null) {
            tutorCourseBeans.remove(tutorCourseBeans.size() - 1);
            tutorCourseListingAdapter.notifyItemRemoved(tutorCourseBeans.size());
            tutorCourseListingAdapter.setLoaded();
        }
    }

    private void handlerError() {

        swipeRefreshLayout.setRefreshing(false);
        removeBottomPullLoading();
        if (tutorCourseListingAdapter.getItemCount() == 0) {
            pbLoading.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
        }
        snackbar = Snackbar
                .make(getView(), R.string.refresh_failed, Snackbar.LENGTH_SHORT);
        snackbar.show();
        tutorCourseListingAdapter.setLoaded();

    }

    private void initialGPS(){

        if (longitude.startsWith("0")&&latitude.startsWith("0")){

            gpsTracker = new GPSTracker(getActivity());

        }
    }
}

