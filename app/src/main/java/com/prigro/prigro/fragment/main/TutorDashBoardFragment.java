package com.prigro.prigro.fragment.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.activity.ListingActivity;
import com.prigro.prigro.activity.MainActivity;
import com.prigro.prigro.activity.StdProfileActivity;
import com.prigro.prigro.activity.TutorCourseDetailActivity;
import com.prigro.prigro.activity.TutorProfileActivity;
import com.prigro.prigro.adapter.StdCourseListingAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.fragment.base.BaseWSFragment;
import com.prigro.prigro.model.GetStdModel;
import com.prigro.prigro.model.GetTutorModel;
import com.prigro.prigro.model.SetSeenModel;
import com.prigro.prigro.model.StdCourseDetailModel;
import com.prigro.prigro.utils.Shared;
import com.prigro.prigro.utils.SimpleDividerItemDecoration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by SzHui on 2017/4/15
 */

public class TutorDashBoardFragment extends BaseWSFragment implements CommonRecycleViewAdapter.onItemClickCallBack {

    private final int MAX_APPLY_SHOW = 2;
    private static TutorDashBoardFragment dashBoardFragment;

    @InjectView(R.id.tv_email)
    TextView tvEmail;
    @InjectView(R.id.tv_name)
    TextView tvName;
    @InjectView(R.id.tv_account_status)
    TextView tvAccountStatus;
    @InjectView(R.id.rv_apply_contain)
    RecyclerView rvApplyList;
    @InjectView(R.id.btn_view_all)
    Button btnViewAll;
    @InjectView(R.id.btn_view_all_approve)
    Button btnViewAllApprove;
    @InjectView(R.id.ll_approve_contain)
    LinearLayout llApproveContain;

    private UserBean tutorBean;
    private StdCourseListingAdapter stdListingAdapter;
    private List<CourseBean> applyCourseBeans;
    private List<CourseBean> approveCourseBeans;
    private ArrayList listTemp;

    public static TutorDashBoardFragment getInstance(Bundle bundle) {
        if (dashBoardFragment == null) {
            dashBoardFragment = new TutorDashBoardFragment();

        }
        if (bundle != null) {
            dashBoardFragment.setArguments(bundle);
        }
        return dashBoardFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_tutor_dashboard;
    }

    @Override
    protected void initData() {

        Bundle bundle = getArguments();
        tutorBean = (UserBean) bundle.getSerializable(MainActivity.KEY_USER_BEAN);
        if (tutorBean!=null) {
            if (tutorBean.getListing_apply() != null) {
                applyCourseBeans = Arrays.asList(tutorBean.getListing_apply());
            }

            if (tutorBean.getListing_approve() != null) {
                approveCourseBeans = Arrays.asList(tutorBean.getListing_approve());
                initApplyList();
            }
        }else {
            showLoadingDialog();
            refreshDashboardData();
        }
    }



    @Override
    protected void initView() {
        if (tutorBean==null){

            return;
        }
        final String UN_ACTIVE = "0";
        tvEmail.setText(tutorBean.getEmail());
        tvName.setText(tutorBean.getName());
        tvAccountStatus.setText(tvAccountStatus.getText().toString().concat(tutorBean.getAccount_status().equals(UN_ACTIVE) ? getString(R.string.unactive) : getString(R.string.active)));

        rvApplyList.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        rvApplyList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvApplyList.setAdapter(stdListingAdapter);
        btnViewAll.setVisibility(applyCourseBeans.size() > MAX_APPLY_SHOW ? View.VISIBLE : View.GONE);
        btnViewAllApprove.setVisibility(approveCourseBeans.size()>MAX_APPLY_SHOW?View.VISIBLE:View.GONE);
        initApproveList();
    }

    @Override
    protected void initListener() {

    }

    @OnClick(R.id.btn_edit_profile)
    public void editProfile() {

        GetTutorModel getTutorModel = new GetTutorModel((BaseActivity) getActivity());
        getTutorModel.setICallBackListener(this);
        getTutorModel.executeData(Integer.toString(tutorBean.getType()), tutorBean.getId(), Shared.read(Constants.SP_TOKEN));
        showLoadingDialog();
    }



    @Override
    public void showSuccessResult(int key, Object... params) {

        switch (key) {

            case Constants.GET_TUTOR_MODEL:
                Intent intentEdit = new Intent(getActivity(), TutorProfileActivity.class);
                intentEdit.putExtra(TutorProfileActivity.GET_TUTOR_BEAN, (UserBean) params[0]);
                startActivity(intentEdit);
                break;

            case Constants.STD_COURSE_DETAIL_MODEL:

                Intent intentDetail = new Intent(getActivity(), TutorCourseDetailActivity.class);
                intentDetail.putExtra(TutorCourseDetailActivity.KEY_COURSE_DETAIL, (CourseBean) params[0]);
                startActivity(intentDetail);
                break;

            case Constants.GET_STD_MODEL:
                Intent intent = new Intent(getActivity(), StdProfileActivity.class);
                intent.putExtra(StdProfileActivity.GET_STD_BEAN, (UserBean) params[0]);
                intent.putExtra(StdProfileActivity.GET_USER_TYPE, true);
                startActivity(intent);
                break;
            case Constants.GET_LISTING_DETAIL:
                initListing((UserBean) params[0]);
                break;
        }

    }

    @Override
    public void onItemClick(int position) {
        showLoadingDialog();
        StdCourseDetailModel stdCourseDetailModel = new StdCourseDetailModel((BaseActivity) getActivity(), Integer.toString(position));
        stdCourseDetailModel.setICallBackListener(this);
        stdCourseDetailModel.executeData();
    }

    @OnClick(R.id.btn_view_all)
    public void viewAllOnClick() {

        Intent intent = new Intent(getActivity(), ListingActivity.class);
        intent.putExtra(ListingActivity.LISTING_DATA, (Serializable) applyCourseBeans);
        intent.putExtra(ListingActivity.LISTING_TYPE, ListingActivity.TYPE_APPLY);
        startActivity(intent);

    }

    @OnClick(R.id.btn_view_all_approve)
    public void viewAllApproveOnClick() {

        Intent intent = new Intent(getActivity(), ListingActivity.class);
        intent.putExtra(ListingActivity.LISTING_DATA, (Serializable) approveCourseBeans);
        intent.putExtra(ListingActivity.LISTING_TYPE, ListingActivity.TYPE_APPROVE);
        startActivity(intent);

    }


    public void initListing(UserBean userBean) {
        final String UN_ACTIVE = "0";
        tvEmail.setText(userBean.getEmail());
        tvName.setText(userBean.getName());
        tvAccountStatus.setText(userBean.getAccount_status().equals(UN_ACTIVE) ? getString(R.string.unactive) : getString(R.string.active));
        if (userBean.getListing_apply() != null) {
            applyCourseBeans = Arrays.asList(userBean.getListing_apply());
            initApplyList();
        }

        if (userBean.getListing_approve() != null) {
            approveCourseBeans = Arrays.asList(userBean.getListing_approve());
            initApproveList();
        }
    }

    private void initApplyList() {

        if (listTemp == null) {
            listTemp = new ArrayList();
            stdListingAdapter = new StdCourseListingAdapter(listTemp, false);
            stdListingAdapter.setOnItemClickCallBack(this);

        }
        listTemp.clear();
        int loopCount = applyCourseBeans.size() > MAX_APPLY_SHOW ? MAX_APPLY_SHOW : applyCourseBeans.size();

        for (int i = 0; i < loopCount; i++) {
            listTemp.add(applyCourseBeans.get(i));

        }
        stdListingAdapter.notifyDataSetChanged();
        if (applyCourseBeans.size()>2){
            btnViewAll.setVisibility(View.VISIBLE);
        }

    }

    private void initApproveList() {
        llApproveContain.removeAllViews();

        int loopCount = approveCourseBeans.size() > MAX_APPLY_SHOW ? MAX_APPLY_SHOW : approveCourseBeans.size();
        for (int i = 0; i < loopCount; i++) {
            llApproveContain.addView(getApproveList(i));
        }
        if (approveCourseBeans.size()>2){
            btnViewAllApprove.setVisibility(View.VISIBLE);
        }

    }

    public View getApproveList(int p_intPosition) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.view_approve_listing, null);
        CourseBean courseBean = approveCourseBeans.get(p_intPosition);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        Button btnContactStudent = (Button) view.findViewById(R.id.btn_contact_student);
        tvTitle.setText(courseBean.getTitle() + " - " + courseBean.getSubject_name());
        if (courseBean.getSeen().equals("0")){
            view.setBackgroundResource(R.color.highlightBlue);

        }
        view.setTag(courseBean.getId());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoadingDialog();
                StdCourseDetailModel stdCourseDetailModel = new StdCourseDetailModel((BaseActivity) getActivity(), v.getTag().toString());
                stdCourseDetailModel.setICallBackListener(TutorDashBoardFragment.this);
                stdCourseDetailModel.executeData();
                SetSeenModel setSeenModel = new SetSeenModel((BaseActivity) getActivity(),v.getTag().toString());
                setSeenModel.setICallBackListener(TutorDashBoardFragment.this);
                setSeenModel.executeData();
                v.setBackgroundResource(android.R.color.transparent);
                localSetSeen(v.getTag().toString());

            }
        });

        btnContactStudent.setTag(courseBean.getStudent_id());
        btnContactStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoadingDialog();
                GetStdModel getStdModel = new GetStdModel((BaseActivity) getActivity(), v.getTag().toString());
                getStdModel.setICallBackListener(TutorDashBoardFragment.this);
                getStdModel.executeData();
            }
        });
        return view;
    }

    public void setUserName(String userName) {

        tvName.setText(userName);

    }

    public void refreshNotificationData(){
        Looper.prepare();
        GetTutorModel getTutorModel = new GetTutorModel((BaseActivity) getActivity(),true);
        getTutorModel.setICallBackListener(this);
        getTutorModel.executeData();

    }

    public void refreshDashboardData(){
        GetTutorModel getTutorModel = new GetTutorModel((BaseActivity) getActivity(),true);
        getTutorModel.setICallBackListener(this);
        getTutorModel.executeData();
    }

    private void localSetSeen(String id){

        for (CourseBean courseBean:approveCourseBeans) {
            if (courseBean.getId().equals(id)){

                courseBean.setSeen("1");
            }
        }

    }
}