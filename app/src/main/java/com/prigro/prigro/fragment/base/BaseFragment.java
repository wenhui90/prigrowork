package com.prigro.prigro.fragment.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by SzHui on 2017/4/8
 */

public abstract class BaseFragment extends Fragment{

    protected View mViewFragment;
    protected LayoutInflater mLayoutInflater;

    protected FragmentManager mFragmentManage;
    protected ProgressDialog dialog;

    protected abstract int getLayoutID();

    protected abstract void initData();

    protected abstract void initView();

    protected abstract void initListener();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFragmentManage = getFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        dialog = new ProgressDialog(getActivity());
        mViewFragment = inflater.inflate(getLayoutID(), container, false);
        mLayoutInflater = LayoutInflater.from(getActivity());
        ButterKnife.inject(this, mViewFragment);
        initData();
        initView();
        initListener();
        return mViewFragment;
    }

    public void showLoadingDialog() {
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void dismissLoadingDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
