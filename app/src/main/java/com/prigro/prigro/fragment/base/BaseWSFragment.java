package com.prigro.prigro.fragment.base;

import android.content.Intent;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.login.LoginManager;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.LoginActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.controller.ICallBack;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.Shared;

/**
 * Created by SzHui on 2017/4/28
 */

public abstract class BaseWSFragment extends BaseFragment implements ICallBack {

    public abstract void showSuccessResult(int key, Object... params);

    @Override
    public void showResult(int key, Object... params) {
        dismissLoadingDialog();
        if (key != Constants.STATUS_EXCEPTION) {
            showSuccessResult(key, params);
        }
    }


    @Override
    public void showException(int key, Object... params) {
        dismissLoadingDialog();
        if (key == Constants.STATUS_ERROR) {

            BaseWSBean baseWSBean = (BaseWSBean) params[0];

            if (!isTokenInvalid(baseWSBean)) {
                MyDialog.showRemindDialog(getActivity(), getString(R.string.error), baseWSBean.getStatusMessage());
            }

        } else if (key == Constants.STATUS_EXCEPTION) {
            MyDialog.showRemindDialog(getActivity(), getString(R.string.error), params[0].toString());
        }
    }

    @Override
    public void showUnknownError(int key) {
        dismissLoadingDialog();
        MyDialog.showErrorDialog(getActivity(), getString(R.string.unknown_error));
    }

    private boolean isTokenInvalid(BaseWSBean baseWSBean) {
        if (baseWSBean.getStatusCode().equals("410")) {
            Shared.clear();
            LoginManager.getInstance().logOut();
            MyDialog.showConfirmDialog(getActivity(), getString(R.string.error), baseWSBean.getStatusMessage(), new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
            });
            return true;
        }

        return false;

    }
}
