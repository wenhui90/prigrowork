package com.prigro.prigro.fragment.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.activity.StdAddCourseActivity;
import com.prigro.prigro.activity.StdCourseDetailActivity;
import com.prigro.prigro.adapter.StdCourseListingAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.DropDownContentBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.fragment.base.BaseFragment;
import com.prigro.prigro.fragment.base.BaseWSFragment;
import com.prigro.prigro.model.DeleteCourseModel;
import com.prigro.prigro.model.GetDropDownModel;
import com.prigro.prigro.model.StdCourseDetailModel;
import com.prigro.prigro.model.StdCourseModel;
import com.prigro.prigro.utils.SimpleDividerItemDecoration;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by SzHui on 2017/4/8
 */

public class StdCourseListingFragment extends BaseWSFragment implements CommonRecycleViewAdapter.onItemClickCallBack, CommonRecycleViewAdapter.onViewClickCallBack {

    @InjectView(R.id.fab)
    FloatingActionButton fab;
    @InjectView(R.id.rv_listing)
    RecyclerView rvListing;
    @InjectView(R.id.pb_loading)
    ProgressBar pbLoading;
    @InjectView(R.id.tv_listing_empty)
    TextView tvListingEmpty;
    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    StdCourseListingAdapter stdListingAdapter;
    ArrayList<CourseBean> courseBeans;
    private String listingID;


    private Snackbar snackbar;

    public static BaseFragment getInstance() {
        Bundle bundle = new Bundle();
        StdCourseListingFragment listingFragment = new StdCourseListingFragment();
        listingFragment.setArguments(bundle);
        return listingFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_std_course_listing;
    }

    @Override
    protected void initData() {
        courseBeans = new ArrayList();
        stdListingAdapter = new StdCourseListingAdapter(courseBeans);
        stdListingAdapter.setOnItemClickCallBack(this);
        stdListingAdapter.setOnViewClickCallBack(this);
    }

    @Override
    protected void initView() {
        rvListing.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        rvListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvListing.setAdapter(stdListingAdapter);

    }

    @Override
    protected void initListener() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoadingDialog();
                GetDropDownModel getDropDownModel = new GetDropDownModel((BaseActivity) getActivity());
                getDropDownModel.setICallBackListener(StdCourseListingFragment.this);
                getDropDownModel.executeData();


            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCourseListing();
            }
        });

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && courseBeans.size() <= 0) {
            rvListing.setVisibility(View.GONE);
            tvListingEmpty.setVisibility(View.GONE);
            pbLoading.setVisibility(View.VISIBLE);
            getCourseListing();
        }

    }

    @Override
    public void showSuccessResult(int key, Object... params) {

        switch (key) {

            case Constants.STD_COURSE_MODEL:
                swipeRefreshLayout.setRefreshing(false);
                pbLoading.setVisibility(View.GONE);
                courseBeans.clear();
                courseBeans.addAll((ArrayList) params[0]);
                stdListingAdapter.notifyDataSetChanged();
                showListing();
                snackbar = Snackbar
                        .make(getView(), R.string.refresh_success, Snackbar.LENGTH_SHORT);
                snackbar.show();
                break;

            case Constants.STD_COURSE_DETAIL_MODEL:
                Intent intent = new Intent(getActivity(), StdCourseDetailActivity.class);
                intent.putExtra(StdCourseDetailActivity.KEY_COURSE_DETAIL, (CourseBean) params[0]);
                startActivity(intent);
                break;

            case Constants.DELETE_COURSE_MODEL:
                for (CourseBean courseBean : courseBeans) {
                    if (courseBean.getId().equals(listingID)) {
                        courseBeans.remove(courseBean);
                        stdListingAdapter.notifyDataSetChanged();
                        break;
                    }
                }
                snackbar = Snackbar
                        .make(getView(), R.string.delete_success, Snackbar.LENGTH_SHORT);
                snackbar.show();
                showListing();
                break;

            case Constants.GET_DROPDOWN_TYPE:
                MyLog.d("params :: " + params);
                Intent intentAddCourse = new Intent(getActivity(), StdAddCourseActivity.class);
                intentAddCourse.putExtra(StdAddCourseActivity.KEY_GET_DROP_DOWN,(DropDownContentBean)params[0]);
                startActivity(intentAddCourse);

                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        showLoadingDialog();
        StdCourseDetailModel stdCourseDetailModel = new StdCourseDetailModel((BaseActivity) getActivity(), Integer.toString(position));
        stdCourseDetailModel.setICallBackListener(this);
        stdCourseDetailModel.executeData();
    }

    @Override
    public void onItemClick(View v) {
        showLoadingDialog();
        listingID = (String) v.getTag();
        DeleteCourseModel deleteCourseModel = new DeleteCourseModel((BaseActivity) getActivity(), listingID);
        deleteCourseModel.setICallBackListener(this);
        deleteCourseModel.executeData();
    }

    private void showListing() {

        if (courseBeans.size() <= 0) {
            tvListingEmpty.setVisibility(View.VISIBLE);
            rvListing.setVisibility(View.GONE);
        } else {

            tvListingEmpty.setVisibility(View.GONE);
            rvListing.setVisibility(View.VISIBLE);
        }

    }

    private void getCourseListing() {
        StdCourseModel stdCourseModel = new StdCourseModel((BaseActivity) getActivity());
        stdCourseModel.setICallBackListener(this);
        stdCourseModel.executeData();

    }

    @Override
    public void showUnknownError(int key) {
        super.showUnknownError(key);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showException(int key, Object... params) {
        super.showException(key, params);
        swipeRefreshLayout.setRefreshing(false);
    }
}
