package com.prigro.prigro.fragment.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.activity.MainActivity;
import com.prigro.prigro.activity.StdCourseDetailActivity;
import com.prigro.prigro.activity.StdProfileActivity;
import com.prigro.prigro.adapter.NotificationListingAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.NotificationBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.fragment.base.BaseWSFragment;
import com.prigro.prigro.model.GetStdModel;
import com.prigro.prigro.model.NotificationDetailsModel;
import com.prigro.prigro.model.NotificationSetSeenModel;
import com.prigro.prigro.model.StdCourseDetailModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by SzHui on 2017/4/8
 */

public class StdDashBoardFragment extends BaseWSFragment implements CommonRecycleViewAdapter.onItemClickCallBack {

    private static StdDashBoardFragment dashBoardFragment;
    @InjectView(R.id.tv_email)
    TextView tvEmail;
    @InjectView(R.id.tv_name)
    TextView tvName;
    @InjectView(R.id.rv_notification_list)
    RecyclerView rvNotification;

    UserBean stdBean;
    private NotificationListingAdapter notificationListingAdapter;
    private List<NotificationBean> notificationList;

    public static StdDashBoardFragment getInstance(Bundle bundle) {
        if (dashBoardFragment == null) {
            dashBoardFragment = new StdDashBoardFragment();
        }
        if (bundle != null) {
            dashBoardFragment.setArguments(bundle);
        }
        return dashBoardFragment;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_std_dashboard;
    }

    @Override
    protected void initData() {
        Bundle bundle = getArguments();
        stdBean = (UserBean) bundle.getSerializable(MainActivity.KEY_USER_BEAN);

    }

    @Override
    protected void initView() {
        notificationList = new ArrayList();
        notificationListingAdapter = new NotificationListingAdapter(notificationList);
        notificationListingAdapter.setOnItemClickCallBack(this);
        rvNotification.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvNotification.setAdapter(notificationListingAdapter);
        tvEmail.setText(stdBean.getEmail());
        tvName.setText(stdBean.getName());
        NotificationDetailsModel notificationDetailsModel = new NotificationDetailsModel((BaseActivity) getActivity());
        notificationDetailsModel.setICallBackListener(this);
        notificationDetailsModel.executeData();
    }

    @Override
    protected void initListener() {

    }

    @OnClick(R.id.btn_edit_profile)
    public void editProfile() {

        GetStdModel getStdModel = new GetStdModel((BaseActivity) getActivity());
        getStdModel.setICallBackListener(this);
        getStdModel.executeData();
        showLoadingDialog();
    }

    @Override
    public void showSuccessResult(int key, Object... params) {
        switch (key) {

            case Constants.GET_STD_MODEL:
                Intent intent = new Intent(getActivity(), StdProfileActivity.class);
                intent.putExtra(StdProfileActivity.GET_STD_BEAN, (UserBean) params[0]);
                startActivity(intent);
                break;

            case Constants.GET_NOTIFICATION_LIST:
                notificationList.clear();
                notificationList.addAll(Arrays.asList((NotificationBean[]) params));
                notificationListingAdapter.notifyDataSetChanged();
                break;

            case Constants.SET_NOTIFICATION_SEEN:
                removeNotification((NotificationBean) params[0]);
                break;

            case Constants.STD_COURSE_DETAIL_MODEL:
                Intent intentDetail = new Intent(getActivity(), StdCourseDetailActivity.class);
                intentDetail.putExtra(StdCourseDetailActivity.KEY_COURSE_DETAIL, (CourseBean) params[0]);
                intentDetail.putExtra(StdCourseDetailActivity.KEY_FROM_NOTIFICATION,true);
                startActivity(intentDetail);
                break;

        }
    }

    public void setUserName(String userName) {
        tvName.setText(userName);

    }

    @Override
    public void onItemClick(int position) {
        showLoadingDialog();
        NotificationSetSeenModel notificationSetSeenModel = new NotificationSetSeenModel((BaseActivity) getActivity(), position);
        notificationSetSeenModel.setICallBackListener(this);
        notificationSetSeenModel.executeData();
    }

    private void removeNotification(NotificationBean notificationBean) {
        showLoadingDialog();
        for (NotificationBean notificationTemp : notificationList) {
            if (notificationTemp.getListing_id().equals(notificationBean.getListing_id())) {

                StdCourseDetailModel stdCourseDetailModel = new StdCourseDetailModel((BaseActivity) getActivity(),notificationBean.getListing_id());
                stdCourseDetailModel.setICallBackListener(this);
                stdCourseDetailModel.executeData();
                notificationList.remove(notificationTemp);
                notificationListingAdapter.notifyDataSetChanged();
                return;
            }
        }
    }

    public void refreshNotification(){
        Looper.prepare();
        NotificationDetailsModel notificationDetailsModel = new NotificationDetailsModel((BaseActivity) getActivity());
        notificationDetailsModel.setICallBackListener(StdDashBoardFragment.this);
        notificationDetailsModel.executeData();
    }

    public void refreshDashboardData(){
        NotificationDetailsModel notificationDetailsModel = new NotificationDetailsModel((BaseActivity) getActivity());
        notificationDetailsModel.setICallBackListener(StdDashBoardFragment.this);
        notificationDetailsModel.executeData();
    }
}
