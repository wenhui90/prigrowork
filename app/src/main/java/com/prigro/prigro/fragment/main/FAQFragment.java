package com.prigro.prigro.fragment.main;

import android.text.Html;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.bean.FAQBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.fragment.base.BaseFragment;
import com.prigro.prigro.fragment.base.BaseWSFragment;
import com.prigro.prigro.model.FaqModel;
import com.prigro.prigro.utils.Shared;

import butterknife.InjectView;

/**
 * Created by vienhui on 19/05/2017
 */

public class FAQFragment extends BaseWSFragment {

    @InjectView(R.id.tv_faq)
    TextView tvFaq;


    public static BaseFragment getInstance() {
        FAQFragment faqFragment = new FAQFragment();
        return faqFragment;
    }

    @Override
    public void showSuccessResult(int key, Object... params) {

        FAQBean faqBean = (FAQBean) params[0];
        String textShowing = (Shared.readInt(Constants.SP_USER_TYPE,-1)==Constants.TUTOR?faqBean.getEducator():faqBean.getLearner())+faqBean.getDisclaimer();
        tvFaq.setText(Html.fromHtml(textShowing));
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_faq;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initListener() {

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            if (tvFaq.getText().toString().length() <= 0) {
                FaqModel faqModel = new FaqModel((BaseActivity) getActivity());
                faqModel.setICallBackListener(this);
                faqModel.executeData();
            }
        }

    }
}
