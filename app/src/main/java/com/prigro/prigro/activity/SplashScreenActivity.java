package com.prigro.prigro.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessTokenTracker;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.model.GetStdModel;
import com.prigro.prigro.model.GetTutorModel;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.Shared;

/**
 * Created by vienhui on 17/05/2017
 */

public class SplashScreenActivity extends BaseNavigationActivity {

    private AccessTokenTracker accessTokenTracker;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_splash_screen;
    }

    @Override
    protected void initData() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        requestPermission();
        initNotification();
    }

    @Override
    protected void initView() {


    }

    @Override
    protected void initListener() {

    }

    private void getUserDetail(int userType) {

        if (userType == Constants.TUTOR) {
            GetTutorModel getTutorModel = new GetTutorModel(this);
            getTutorModel.setICallBackListener(this);
            getTutorModel.executeData();
        } else if (userType == Constants.STUDENT) {
            GetStdModel getStdModel = new GetStdModel(this);
            getStdModel.setICallBackListener(this);
            getStdModel.executeData();
        } else {

            Shared.clear();
            finish();
        }

    }

    @Override
    public void showSuccessResult(int key, final Object... params) {

        switch (key) {

            case Constants.GET_TUTOR_MODEL:
            case Constants.GET_STD_MODEL:
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intentStd = new Intent(SplashScreenActivity.this, MainActivity.class);
                        intentStd.putExtra(MainActivity.KEY_USER_BEAN, (UserBean) params[0]);
                        startActivity(intentStd);
                        finish();
                    }
                }, 1000);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 101: {

                switch (requestCode) {
                    case 101:
                        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            if (Shared.read(Constants.SP_UNIQUE_ID) != null) {
                                Shared.readInt(Constants.SP_USER_TYPE, -1);
                                getUserDetail(Shared.readInt(Constants.SP_USER_TYPE, -1));

                            } else {
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();

                                        LoginManager.getInstance().logOut();
                                    }
                                }, 2000);
                            }
                        } else {
                            finish();
                        }
                        break;
                    default:
                        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }
        }
    }


    @SuppressLint("InlinedApi")
    private void requestPermission() {

        if (checkIfAlreadyhavePermission()) {
            if (Shared.read(Constants.SP_UNIQUE_ID) != null) {
                Shared.readInt(Constants.SP_USER_TYPE, -1);
                getUserDetail(Shared.readInt(Constants.SP_USER_TYPE, -1));

            } else {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, 2000);
            }

        } else {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.CALL_PHONE}, 101);
        }
    }


    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void showUnknownError(int key) {
        super.showUnknownError(key);
        Shared.clear();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1000);
    }

    @Override
    public void showException(int key, Object... params) {

        if (key == Constants.STATUS_ERROR) {
            BaseWSBean baseWSBean = (BaseWSBean) params[0];
            if (!isTokenInvalid(baseWSBean)) {
                MyDialog.showConfirmDialog(this, getString(R.string.error),baseWSBean.getStatusMessage(), new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        getUserDetail(Shared.readInt(Constants.SP_USER_TYPE, -1));
                    }
                });
            }

        } else if (key == Constants.STATUS_EXCEPTION) {
            MyDialog.showConfirmDialog(this, getString(R.string.error), params[0].toString(), new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    dialog.dismiss();
                    getUserDetail(Shared.readInt(Constants.SP_USER_TYPE, -1));
                }
            });
        }
    }

    private void initNotification(){


        // If a notification message is tapped, any data accompanying the notification
        // message is available in the intent extras. In this sample the launcher
        // intent is fired when the notification is tapped, so any accompanying data would
        // be handled here. If you want a different intent fired, set the click_action
        // field of the notification message to the desired intent. The launcher intent
        // is used when no click_action is specified.
        //
        // Handle possible data accompanying notification message.
        // [START handle_data_extras]
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                MyLog.d("Key: " + key + " Value: " + value);
            }
        }
        // [END handle_data_extras]


    }
}
