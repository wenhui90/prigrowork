package com.prigro.prigro.activity;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.bean.DropDownBean;
import com.prigro.prigro.bean.DropDownContentBean;
import com.prigro.prigro.bean.LoginRegisterBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.model.RegisterModel;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.Tools;

import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by vienhui on 01/05/2017
 */

public class RegisterActivity extends BaseNavigationActivity implements View.OnClickListener{

    public static String KEY_REGISTER;

    @InjectView(R.id.ll_selector_contain)
    LinearLayout llContain;
    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.et_email)
    EditText etEmail;
    @InjectView(R.id.et_password)
    EditText etPassword;
    @InjectView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @InjectView(R.id.et_contact)
    EditText etContact;
    @InjectView(R.id.et_name)
    EditText etName;
    @InjectView(R.id.et_age)
    EditText etAge;
    @InjectView(R.id.tv_race)
    TextView tvRace;
    @InjectView(R.id.tv_national)
    TextView tvNational;
    @InjectView(R.id.rg_gender)
    RadioGroup rgGender;

    private int type = -1;
    //0 equal not selected
    private int raceID = 0;
    private int nationality_id = 0;
    List<DropDownBean> listRace;
    List<DropDownBean> listNational;


    @Override
    public void showSuccessResult(int key, Object... params) {

        MyDialog.showConfirmDialog(this,getString(R.string.success),params[0].toString(), new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                if (type==Constants.TUTOR){

                    startNextActivity(PaymentInfoActivity.class,true);
                }else {
                    onBackPressed();
                }
            }
        });
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_register;
    }

    @Override
    protected void initData() {

        Intent intent = getIntent();
        DropDownContentBean contentBean = (DropDownContentBean) intent.getSerializableExtra(KEY_REGISTER);
        listRace = Arrays.asList(contentBean.getRace());
        listNational = Arrays.asList(contentBean.getNationality());
    }

    @Override
    protected void initView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void initListener() {
        for (int i = 0; i < llContain.getChildCount(); i++) {
            llContain.getChildAt(i).setOnClickListener(this);
        }
        tvRace.setOnClickListener(this);
        tvNational.setOnClickListener(this);
    }

    @OnClick(R.id.btn_register)
    public void registerOnClick() {

        if (!checkInputFieldEmpty().equals("")) {

            MyDialog.showErrorDialog(this, String.format(getString(R.string.input_cant_empty), checkInputFieldEmpty()));
        } else if (!passwordIsMatch()) {
            MyDialog.showErrorDialog(this, getString(R.string.password_not_match));
        }else if(!isTypeSelected()){

            MyDialog.showErrorDialog(this, getString(R.string.select_user_type));

        } else {
            showLoadingDialog();
            RegisterModel registerModel = new RegisterModel(this,getRegisterData());
            registerModel.setICallBackListener(this);
            registerModel.executeData();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private String checkInputFieldEmpty() {

        String emptyInput = "";

        for (int i = 0; i < llContain.getChildCount(); i++) {

            if (llContain.getChildAt(i) instanceof EditText && ((EditText) llContain.getChildAt(i)).getText().toString().isEmpty()&&((EditText)llContain.getChildAt(i)).getVisibility()==View.VISIBLE) {
                String temp = ((EditText) llContain.getChildAt(i)).getHint().toString();
                emptyInput = emptyInput.concat(emptyInput.equals("") ? temp : "," + temp);
            }
        }
        return emptyInput;
    }

    private boolean passwordIsMatch() {

        return etPassword.getText().toString().equals(etConfirmPassword.getText().toString());
    }

    private LoginRegisterBean getRegisterData() {

        LoginRegisterBean loginRegisterBean = new LoginRegisterBean();
        loginRegisterBean.setEmail(etEmail.getText().toString());
        loginRegisterBean.setName(etName.getText().toString());
        loginRegisterBean.setPassword(etPassword.getText().toString());
        loginRegisterBean.setType(type);
        loginRegisterBean.setContact(etContact.getText().toString());
        loginRegisterBean.setAge(etAge.getText().toString());
        loginRegisterBean.setRace(raceID);
        loginRegisterBean.setNationality_id(Integer.toString(nationality_id));
        loginRegisterBean.setGender(rgGender.getCheckedRadioButtonId()==R.id.btn_male?"M":"F");

        return loginRegisterBean;
    }

    private void selectType(int id) {

        for (int i = 0; i < llContain.getChildCount(); i++) {
            if (llContain.getChildAt(i) instanceof TextView) {
                TextView tvType = (TextView) llContain.getChildAt(i);

                if (tvType.getId() == id) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tvType.setTextColor(getResources().getColorStateList(R.color.skyBlue, this.getTheme()));
                    } else {

                        tvType.setTextColor(getResources().getColorStateList(R.color.skyBlue));
                    }

                    tvType.setBackgroundResource(R.drawable.background_btn_transparent_click);
                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tvType.setTextColor(getResources().getColorStateList(R.color.white, this.getTheme()));
                    } else {

                        tvType.setTextColor(getResources().getColorStateList(R.color.white));
                    }
                    tvType.setBackgroundResource(R.drawable.background_btn_transparent);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_std:
            case R.id.tv_tutor:
                setType(v.getId());
                selectType(v.getId());
                break;

            case R.id.tv_race:
                MyDialog.showListingDialog(this, getString(R.string.please_select), Tools.getDropDownData(listRace), new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                      DropDownBean dropDownBean = listRace.get(position);
                        tvRace.setText(dropDownBean.getName());
                        raceID = dropDownBean.getId();

                    }
                });
            break;
            case R.id.tv_national:
                MyDialog.showListingDialog(this, getString(R.string.please_select), Tools.getDropDownData(listNational), new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        DropDownBean dropDownBean = listNational.get(position);
                        tvNational.setText(dropDownBean.getName());
                        nationality_id = dropDownBean.getId();

                    }
                });
                break;
        }
    }

    private void setType(int id) {

        if (id == R.id.tv_std) {
            type = Constants.STUDENT;

        } else if (id == R.id.tv_tutor) {
            type = Constants.TUTOR;
        }
    }

    private boolean isTypeSelected() {

        return type != -1;
    }

}
