package com.prigro.prigro.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.adapter.ViewPagerAdapter;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.fragment.main.FAQFragment;
import com.prigro.prigro.fragment.main.StdCourseListingFragment;
import com.prigro.prigro.fragment.main.StdDashBoardFragment;
import com.prigro.prigro.fragment.main.TuitionCenterFragment;
import com.prigro.prigro.fragment.main.TutorCourseListingFragment;
import com.prigro.prigro.fragment.main.TutorDashBoardFragment;
import com.prigro.prigro.model.AdsModel;
import com.prigro.prigro.model.RegisterNotificationTokenModel;
import com.prigro.prigro.utils.ByteDataDownloader;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.PopupAdsWindow;
import com.prigro.prigro.utils.Shared;

import java.util.ArrayList;

import butterknife.InjectView;

public class MainActivity extends BaseNavigationActivity implements ByteDataDownloader.ByteLoadingListener {

    public static String KEY_USER_BEAN = "userbean";
    public static String KEY_LISTING_DETAIL = "listingdetail";

    @InjectView(R.id.view_pager)
    ViewPager viewPager;
    @InjectView(R.id.tab_layout)
    TabLayout tabLayout;
    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.coordinator_layout)
    CoordinatorLayout rootLayout;

    ArrayList<Fragment> mFragmentArrayList;
    AdsModel adsModel;
    String[] mTabName;
    private boolean isGif;
    final int[] mIconTutor = new int[]{
            R.mipmap.ic_home,
            R.mipmap.ic_list,
            R.mipmap.ic_faq
    };

    final int[] mIconStd = new int[]{
            R.mipmap.ic_home,
            R.mipmap.ic_list,
            R.mipmap.ic_tuition_list,
            R.mipmap.ic_faq
    };

    private static UserBean userBean;
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            MyLog.d("RECEIVE!!!!!");
//            viewPager.setCurrentItem(0,true);
        }
    };

    @Override
    protected int getLayoutID() {
        return R.layout.activity_main;
    }

    @Override
    protected void initData() {
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("com.prigro.prigro_FCM-Message"));
        if (FirebaseInstanceId.getInstance().getToken() != null) {
            RegisterNotificationTokenModel registerNotificationTokenModel = new RegisterNotificationTokenModel(this, FirebaseInstanceId.getInstance().getToken());
            registerNotificationTokenModel.setICallBackListener(this);
            registerNotificationTokenModel.executeData();
        }
        FacebookSdk.sdkInitialize(getApplicationContext());
        Intent intent = getIntent();
        userBean = (UserBean) intent.getSerializableExtra(KEY_USER_BEAN);
        mTabName = getResources().getStringArray(userBean.getType() == Constants.TUTOR ? R.array.main_tab_tutor : R.array.main_tab_std);
        adsModel = new AdsModel(this);
        adsModel.setICallBackListener(this);
        adsModel.executeData();

    }

    @Override
    protected void initView() {
        initFragmentList();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.white));
        View root = tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(getResources().getColor(R.color.white));
            drawable.setSize(2, 1);
            ((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }

    }

    @Override
    protected void initListener() {

    }

    private void initFragmentList() {

        if (Shared.readInt(Constants.SP_USER_TYPE, -1) == Constants.TUTOR)
            initTutorFragment();
        else if (Shared.readInt(Constants.SP_USER_TYPE, -1) == Constants.STUDENT)
            initStdFragment();


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), mFragmentArrayList);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(mFragmentArrayList.size());
        tabLayout.setupWithViewPager(viewPager);
        setCustomViewPager();
    }

    private void initTutorFragment() {
        UserBean tutorBean = (UserBean) getIntent().getSerializableExtra(KEY_USER_BEAN);
        mFragmentArrayList = new ArrayList<>();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_USER_BEAN, tutorBean);
        mFragmentArrayList.add(TutorDashBoardFragment.getInstance(bundle));
        mFragmentArrayList.add(TutorCourseListingFragment.getInstance());
        mFragmentArrayList.add(FAQFragment.getInstance());
    }

    private void initStdFragment() {
        UserBean stdBean = (UserBean) getIntent().getSerializableExtra(KEY_USER_BEAN);
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_USER_BEAN, stdBean);
        mFragmentArrayList = new ArrayList<>();
        mFragmentArrayList.add(StdDashBoardFragment.getInstance(bundle));
        mFragmentArrayList.add(StdCourseListingFragment.getInstance());
        mFragmentArrayList.add(TuitionCenterFragment.getInstance());
        mFragmentArrayList.add(FAQFragment.getInstance());

    }

    public void setCustomViewPager() {
        for (int i = 0; i < mFragmentArrayList.size(); i++) {

            if (tabLayout.getTabAt(i) != null) {

                tabLayout.getTabAt(i).setText(mTabName[i]).setIcon(getIcon()[i]);
            }
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case R.id.menu_setting:
                startNextActivity(SettingActivity.class, false);
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSuccessResult(int key, final Object... params) {

        switch (key) {

            case Constants.LOGOUT_MODEL:
                MyDialog.showConfirmDialog(this, getString(R.string.success), params[0].toString(), new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        Shared.clear();
                        LoginManager.getInstance().logOut();
                        startActivity(intent);
                        finish();
                    }
                });

                break;

            case Constants.GET_LISTING_DETAIL:

                TutorDashBoardFragment.getInstance(new Bundle()).initListing((UserBean) params[0]);
                break;

            case Constants.ADS_MODEL:
//                String temp ="https://ichef.bbci.co.uk/news/660/cpsprodpb/37B5/production/_89716241_thinkstockphotos-523060154.jpg";
//                String temp ="http://katemobile.ru/tmp/sample3.gif";
                final String temp = (String) params[0];
                isGif = temp.substring(temp.length() - 3).equals("gif");
                MyLog.d("isGif :: "+isGif);
                ByteDataDownloader byteDataDownloader = new ByteDataDownloader();
                byteDataDownloader.execute(temp);
                byteDataDownloader.setOnSuccessLoadingListener(this);

                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case Constants.REQUEST_LISTING_CODE:

                if (resultCode == RESULT_OK) {
                    ((TutorCourseListingFragment) TutorCourseListingFragment.getInstance()).setListingIsApplied(data.getStringExtra(KEY_LISTING_DETAIL));
                }

                break;

        }

    }

    public static void setUserName(String userName) {

        if (userBean.getType() == Constants.TUTOR) {

            TutorDashBoardFragment.getInstance(null).setUserName(userName);
        } else {

            StdDashBoardFragment.getInstance(null).setUserName(userName);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    private int[] getIcon() {

        if (userBean.getType() == Constants.TUTOR) {

            return mIconTutor;
        } else {

            return mIconStd;
        }
    }

    @Override
    public void onByteLoadFinish(byte[] imgvData) {

        PopupAdsWindow popupAdsWindow = new PopupAdsWindow(getLayoutInflater(), rootLayout, imgvData, isGif);
        popupAdsWindow.popUpWindow();
    }
}