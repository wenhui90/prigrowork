package com.prigro.prigro.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.adapter.TutorApplyAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.TimeSlotBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.controller.ICallBack;
import com.prigro.prigro.model.ApproveCourseModel;
import com.prigro.prigro.model.GetTutorModel;
import com.prigro.prigro.ui.ListingDetailTextView;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.SimpleDividerItemDecoration;
import com.prigro.prigro.utils.Tools;

import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by SzHui on 2017/4/8
 */

public class StdCourseDetailActivity extends BaseNavigationActivity implements CommonRecycleViewAdapter.onItemClickCallBack,CommonRecycleViewAdapter.onViewClickCallBack, ICallBack, OnMapReadyCallback {

    public static final String KEY_COURSE_DETAIL = "coursedetail";
    public static final String KEY_FROM_NOTIFICATION ="notification";

    public static final String APPROVE = "2";

    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.tv_subject)
    ListingDetailTextView tvSubject;
    @InjectView(R.id.tv_title)
    ListingDetailTextView tvTitle;
    @InjectView(R.id.tv_level)
    ListingDetailTextView tvLevel;
    @InjectView(R.id.tv_gender)
    ListingDetailTextView tvGender;
    @InjectView(R.id.tv_lesson_type)
    ListingDetailTextView tvLessonType;
    @InjectView(R.id.tv_location)
    ListingDetailTextView tvLocation;
    @InjectView(R.id.tv_lesson_per_week)
    ListingDetailTextView tvLessonPerWeek;
    @InjectView(R.id.tv_hour_per_lesson)
    ListingDetailTextView tvHourPerLesson;
    @InjectView(R.id.tv_time_slot)
    ListingDetailTextView tvTimeSlot;
    @InjectView(R.id.rv_tutor_apply)
    RecyclerView rvTutorApply;
    @InjectView(R.id.tv_tutor_apply)
    TextView tvTutorApply;
    @InjectView(R.id.tv_budget)
    ListingDetailTextView tvBudget;
    @InjectView(R.id.ll_timeslot_contain)
    LinearLayout llTimeSlotContain;
    @InjectView(R.id.sv_view)
    ScrollView scrollView;

    private CourseBean courseBean;
    private TutorApplyAdapter tutorApplyAdapter;
    private List<UserBean> tutorList;
    private View view;
    private SupportMapFragment mapFragment;
    private boolean isNotification;
    private boolean isApprove;
    private UserBean approveBean;


    @Override
    protected int getLayoutID() {
        return R.layout.activity_std_course_detail;
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        courseBean = (CourseBean) intent.getSerializableExtra(KEY_COURSE_DETAIL);
        isNotification = intent.getBooleanExtra(KEY_FROM_NOTIFICATION,false);

        if (courseBean.getTutors() != null) {
            tutorList = Arrays.asList(courseBean.getTutors());
            tutorApplyAdapter = new TutorApplyAdapter(tutorList);
            tutorApplyAdapter.setOnViewClickCallBack(this);
            tutorApplyAdapter.setOnItemClickCallBack(this);

        }
    }

    @Override
    protected void initView() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map);
        mapFragment.getMapAsync(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tvSubject.setDetail(courseBean.getSubject_name());
        tvTitle.setDetail(courseBean.getTitle());
        tvLevel.setDetail(courseBean.getLevel_name());
        tvGender.setDetail(Tools.getGender(this,courseBean.getPreferred_gender()));
        tvLessonType.setDetail(courseBean.getLesson_type_name());
        tvLocation.setDetail(courseBean.getLocation());
        tvLessonPerWeek.setDetail(courseBean.getLesson_per_week()+"");
        tvHourPerLesson.setDetail(courseBean.getHour_per_lesson()+"");
        tvBudget.setDetail(courseBean.getBudget_per_month());
        setTimeSlot();
        if (courseBean.getTutors() != null) {
            rvTutorApply.addItemDecoration(new SimpleDividerItemDecoration(this));
            rvTutorApply.setLayoutManager(new LinearLayoutManager(this));
            rvTutorApply.setAdapter(tutorApplyAdapter);
            tvTutorApply.setVisibility(View.VISIBLE);
        }
        if ((courseBean.getAccepted()!=null&&courseBean.getAccepted().equals("0")))
        {
            mapFragment.getView().setVisibility(View.GONE);
        }

        if (isNotification){

            scrollToView();

        }

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void showSuccessResult(int key, Object... params) {
        dismissLoadingDialog();
        switch (key) {

            case Constants.APPROVE_COURSE_MODEL:
                approveBean.setApplyStatus(StdCourseDetailActivity.APPROVE);
                MyDialog.showConfirmDialog(this, getString(R.string.success), (String) params[0], new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        view.setEnabled(false);
                        dialog.dismiss();
                    }
                });
                break;

            case Constants.GET_TUTOR_MODEL:
                Intent intent = new Intent(StdCourseDetailActivity.this,TutorProfileActivity.class);
                intent.putExtra(TutorProfileActivity.GET_TUTOR_BEAN,(UserBean)params[0]);
                intent.putExtra(TutorProfileActivity.GET_USER_TYPE,true);
                intent.putExtra(TutorProfileActivity.GET_APPROVE_STATUS,isApprove);
                startActivity(intent);

                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (courseBean.getLatitude()!=null&&courseBean.getLongitude()!=null){

            LatLng latLng = new LatLng(Double.parseDouble(courseBean.getLatitude()), Double.parseDouble(courseBean.getLongitude()));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

        }
        googleMap.getUiSettings().setScrollGesturesEnabled(false);

    }

    @Override
    public void onItemClick(View v) {

        view = v;
        approveBean = tutorList.get((int)v.getTag());
        MyDialog.showConfirmDialog(this, getString(R.string.confirm), String.format(getString(R.string.approve_tutor), approveBean.getName()), new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                showLoadingDialog();
                ApproveCourseModel approveCourseModel = new ApproveCourseModel(StdCourseDetailActivity.this, approveBean.getUser_id(), courseBean.getId());
                approveCourseModel.setICallBackListener(StdCourseDetailActivity.this);
                approveCourseModel.executeData();
                dialog.dismiss();
            }
        }, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onItemClick(int position) {
        showLoadingDialog();
        final UserBean userBean = tutorList.get(position);
        isApprove = userBean.getApplyStatus().equals(StdCourseDetailActivity.APPROVE);
        GetTutorModel getTutorModel = new GetTutorModel(this,userBean.getUser_id());
        getTutorModel.setICallBackListener(this);
        getTutorModel.executeData();


    }


    private void setTimeSlot() {

        if (courseBean.getTimeslot()==null||courseBean.getTimeslot().length<=0) {

            tvTimeSlot.setDetail(getString(R.string.not_preference));
            tvTimeSlot.setDetailVisible(true);
            return;
        }


        List<String> list = Arrays.asList(getResources().getStringArray(R.array.days));
        for (int i = 0; i < courseBean.getTimeslot().length; i++) {
            TimeSlotBean timeslotBean = courseBean.getTimeslot()[i];
            String day = list.get(Integer.parseInt(timeslotBean.getDay())-1);
            String startTime = timeslotBean.getStart_time().substring(0, timeslotBean.getStart_time().length() - 3);
            String endTime = timeslotBean.getEnd_time().substring(0, timeslotBean.getEnd_time().length() - 3);
            llTimeSlotContain.addView(getTimeSlot(day, startTime, endTime));
        }
    }


    private View getTimeSlot(String day, String startTime, String endTime) {

        View view = getLayoutInflater().inflate(R.layout.view_timeslot_item, null);
        TextView tvDay = (TextView) view.findViewById(R.id.tv_day);
        tvDay.setText(day);
        TextView tvStartTime = (TextView) view.findViewById(R.id.tv_start_time);
        tvStartTime.setText(startTime);
        TextView tvEndTime = (TextView) view.findViewById(R.id.tv_end_time);
        tvEndTime.setText(endTime);

        ImageView imageView = (ImageView) view.findViewById(R.id.iv_delete);
        imageView.setVisibility(View.GONE);
        return view;
    }

    private void scrollToView(){

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, rvTutorApply.getBottom());
            }
        });

    }
}
