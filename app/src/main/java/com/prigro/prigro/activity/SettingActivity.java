package com.prigro.prigro.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.login.LoginManager;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.adapter.LanguageSettingAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.model.ChangeLanguageModel;
import com.prigro.prigro.model.LogoutModel;
import com.prigro.prigro.utils.LanguageUtils;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.Shared;
import com.prigro.prigro.utils.SimpleDividerItemDecoration;

import java.util.Arrays;

import butterknife.InjectView;
import butterknife.OnClick;


/**
 * Created by vienhui on 16/09/2017
 */

public class SettingActivity extends BaseNavigationActivity implements CommonRecycleViewAdapter.onItemClickCallBack{

    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.rv_setting)
    RecyclerView rvSetting;

    private LanguageSettingAdapter languageSettingAdapter;
    private int languageType;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initData() {
        languageSettingAdapter  =new LanguageSettingAdapter(Arrays.asList(getResources().getStringArray(R.array.language)));
    }

    @Override
    protected void initView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rvSetting.addItemDecoration(new SimpleDividerItemDecoration(this));
        rvSetting.setLayoutManager(new LinearLayoutManager(this));
        rvSetting.setAdapter(languageSettingAdapter);
        languageSettingAdapter.setOnItemClickCallBack(this);
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void showSuccessResult(int key, Object... params) {

        switch (key) {

            case Constants.LOGOUT_MODEL:
                MyDialog.showConfirmDialog(this, getString(R.string.success), params[0].toString(), new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        clearData();
                        LoginManager.getInstance().logOut();
                        startActivity(intent);
                        finish();
                    }
                });

                break;

            case Constants.CHANGE_LANGUAGE:

                MyDialog.showConfirmDialog(this, getString(R.string.success), params[0].toString(), new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Shared.writeInt(Constants.SP_LANGUAGE,languageType);
                        LanguageUtils.setLanguage(SettingActivity.this,LanguageUtils.getSettingLocaleValue(languageType));
                        Intent intent = new Intent(SettingActivity.this, SplashScreenActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });


                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.ll_logout)
    public void logout() {


        MyDialog.showConfirmDialog(this, getString(R.string.logout), getString(R.string.confirm_logout), new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                showLoadingDialog();
                LogoutModel logoutModel = new LogoutModel(SettingActivity.this);
                logoutModel.setICallBackListener(SettingActivity.this);
                logoutModel.executeData();
                dialog.dismiss();

            }
        }, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                dialog.dismiss();
            }
        });
    }

    @Override
    public void onItemClick(final int position) {
        MyDialog.showConfirmDialog(this,getString(R.string.confirm),getString(R.string.change_language_message), new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                languageType =position;
                showLoadingDialog();
                ChangeLanguageModel changeLanguageModel = new ChangeLanguageModel(SettingActivity.this,languageType);
                changeLanguageModel.setICallBackListener(SettingActivity.this);
                changeLanguageModel.executeData();
                dialog.dismiss();

            }
        }, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                dialog.dismiss();
            }
        });
    }


    private void clearData(){

        Shared.remove(Constants.SP_USER_TYPE);
        Shared.remove(Constants.SP_UNIQUE_ID);
        Shared.remove(Constants.SP_TOKEN);

    }


}
