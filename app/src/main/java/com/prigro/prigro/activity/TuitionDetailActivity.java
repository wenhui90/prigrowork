package com.prigro.prigro.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.bean.TuitionCenterBean;
import com.prigro.prigro.ui.ListingDetailTextView;
import com.prigro.prigro.utils.Tools;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by vienhui on 21/11/2017
 */

public class TuitionDetailActivity extends BaseNavigationActivity {

    public static final String KEY_TUITION_DETAIL = "tuition_detail";

    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.networkImageView)
    NetworkImageView ivBanner;
    @InjectView(R.id.tv_phone_call)
    TextView tvPhoneCall;
    @InjectView(R.id.tv_email)
    TextView tvEmail;
    @InjectView(R.id.tv_tuition_name)
    TextView tvTuitionName;
    @InjectView(R.id.tv_desc)
    ListingDetailTextView tvDesc;
    @InjectView(R.id.tv_address)
    ListingDetailTextView tvAddress;
    @InjectView(R.id.tv_website)
    ListingDetailTextView tvWebsite;

    private TuitionCenterBean tuitionCenterBean;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_tuition_center_detail;
    }

    @Override
    protected void initData() {
        tuitionCenterBean = (TuitionCenterBean) getIntent().getSerializableExtra(KEY_TUITION_DETAIL);
    }

    @Override
    protected void initView() {
        tvTuitionName.setText(tuitionCenterBean.getName());
        tvAddress.setDetail(tuitionCenterBean.getAddress());
        tvDesc.setDetail(tuitionCenterBean.getDescription());
        tvWebsite.setDetail(tuitionCenterBean.getWebsite());
        loadImage(this, ivBanner, tuitionCenterBean.getBanner_url());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void showSuccessResult(int key, Object... params) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.tv_phone_call)
    public void phoneCall(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 101);
        } else {

            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tuitionCenterBean.getContact()));
            startActivity(intent);
        }

    }

    @OnClick(R.id.tv_email)
    public void sendSMS(){

        Tools.sendSMS(tuitionCenterBean.getContact(),this);

    }
}
