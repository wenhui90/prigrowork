package com.prigro.prigro.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.ListCallback;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.bean.DropDownContentBean;
import com.prigro.prigro.bean.TimeSlotBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.model.EditStdProfileModel;
import com.prigro.prigro.model.EditTutorProfileModel;
import com.prigro.prigro.ui.MyEditText;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.Shared;
import com.prigro.prigro.utils.Tools;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

/**
 * Created by vienhui on 30/04/2017
 */

public class ProfileEditActivity extends BaseNavigationActivity implements View.OnClickListener {

    public static final String KEY_GET_DROP_DOWN = "getdropdown";
    public static String KEY_USER_BEAN = "tutorbean";
    private final String defaultStartTime = "18:00";
    private final String defaultEndTime = "20:00";
    private final String[] fileTypes = new String[]{".pdf", ".jpg", ".jpeg"};
    private final String QUALIFICATION_FLAG = "QF";
    private final String RECEIPT_FLAG="RF";

    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.et_current_password)
    MyEditText etPassword;
    @InjectView(R.id.et_new_password)
    MyEditText etNewPassword;
    @InjectView(R.id.et_confirm_password)
    MyEditText etConfirmPassword;
    @InjectView(R.id.et_name)
    MyEditText etName;
    @InjectView(R.id.et_contact)
    MyEditText etContact;
    @InjectView(R.id.et_country)
    MyEditText etCountry;
    @InjectView(R.id.et_age)
    MyEditText etAge;
    @InjectView(R.id.et_race)
    MyEditText etRace;
    @InjectView(R.id.et_desc)
    MyEditText etDesc;
    @InjectView(R.id.et_nric)
    MyEditText etNric;
    @InjectView(R.id.et_budget)
    MyEditText etBudget;
    @InjectView(R.id.et_experience)
    MyEditText etExperience;
    @InjectView(R.id.et_address)
    MyEditText etAddress;
    @InjectView(R.id.tv_faq)
    TextView tvFaq;

    @InjectView(R.id.et_qualification)
    MyEditText etQualification;
    @InjectView(R.id.et_category)
    MyEditText etCategory;

    @InjectView(R.id.tv_add_timeslot)
    TextView tvTimeSlot;
    @InjectView(R.id.ll_timeslot_contain)
    LinearLayout llTimeSlotContain;
    @InjectView(R.id.rg_gender)
    RadioGroup rgGender;
    @InjectView(R.id.btn_male)
    RadioButton btnMale;
    @InjectView(R.id.btn_female)
    RadioButton btnFemale;
    @InjectView(R.id.tv_select_photo)
    TextView tvPhotoPath;
    @InjectView(R.id.tv_select_file)
    TextView tvFilePath;
    @InjectView(R.id.tv_receipt)
    TextView tvReceipt;

    @InjectView(R.id.ll_file_selector)
    LinearLayout llFileSelector;
    @InjectView(R.id.ll_receipt)
    LinearLayout llReceipt;

    private UserBean userBean;
    private Bitmap profileImage;
    private List lisRace, listCountry, listCategory, lisQualification, listFileType;
    private TextView textSelect;
    private ArrayList<String> filePath;
    private String qualificationPath = "",receiptPath="";
    private String filePathFlag;

    @Override
    public void showSuccessResult(int key, Object... params) {

        BaseWSBean statusBeen = (BaseWSBean) params[0];
        MyDialog.showConfirmDialog(this, getString(R.string.success), statusBeen.getStatusMessage(), new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                Intent intent = new Intent(ProfileEditActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                MainActivity.setUserName(etName.getDetail());
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected int getLayoutID() {

        return R.layout.activity_edit_profile;

    }

    @Override
    protected void initData() {
        userBean = (UserBean) getIntent().getSerializableExtra(KEY_USER_BEAN);
        DropDownContentBean dropDownContentBean = (DropDownContentBean) getIntent().getSerializableExtra(KEY_GET_DROP_DOWN);
        lisRace = Arrays.asList(dropDownContentBean.getRace());
        listCountry = Arrays.asList(dropDownContentBean.getNationality());
        listCategory = Arrays.asList(dropDownContentBean.getCategory());
        lisQualification = Arrays.asList(dropDownContentBean.getQualification());
        listFileType = Arrays.asList(getResources().getStringArray(R.array.file_types));
        filePath = new ArrayList<>();
    }

    @Override
    protected void initView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        etName.setDetail(userBean.getName());
        etDesc.setDetail(userBean.getDescription());
        etNric.setDetail(userBean.getNric());
        etExperience.setDetail(userBean.getExperience());
        etAge.setDetail(userBean.getAge());
        etRace.setDetail(userBean.getRace_name());
        etContact.setDetail(userBean.getContact());
        etCountry.setDetail(userBean.getNationality_name());
        etQualification.setDetail(userBean.getQualification_name());
        etCategory.setDetail(userBean.getCategory_name());
        etAddress.setDetail(userBean.getAddress());

        setGender(userBean.getGender());
        hideComponent();
        etAge.setInputType(InputType.TYPE_CLASS_NUMBER);
        etExperience.setInputType(InputType.TYPE_CLASS_NUMBER);
        etNric.setInputType(InputType.TYPE_CLASS_NUMBER);
        etPassword.setPasswordInput();
        etPassword.setTitle(Html.fromHtml(getString(R.string.password_star)));
        etConfirmPassword.setPasswordInput();
        etNewPassword.setPasswordInput();

    }

    @Override
    protected void initListener() {
        etRace.setOnDetailClickListener(this);
        etCountry.setOnDetailClickListener(this);
        etCategory.setOnDetailClickListener(this);
        etQualification.setOnDetailClickListener(this);
    }

    @OnClick(R.id.btn_save)
    public void saveOnClick() {
        if (!passwordIsMatch()) {
            MyDialog.showErrorDialog(this, getString(R.string.password_not_match));
        } else if (userBean.getType() == Constants.TUTOR) {
            showLoadingDialog();
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    EditTutorProfileModel editTutorProfileModel = new EditTutorProfileModel(ProfileEditActivity.this, getInputData());
                    editTutorProfileModel.setICallBackListener(ProfileEditActivity.this);
                    editTutorProfileModel.executeData();

                }
            });


        } else if (userBean.getType() == Constants.STUDENT) {

            showLoadingDialog();
            EditStdProfileModel editStdProfileModel = new EditStdProfileModel(this, getInputData(), getTimeSlotBeans());
            editStdProfileModel.setICallBackListener(this);
            editStdProfileModel.executeData();
        }
    }

    @OnClick(R.id.btn_upload)
    public void uploadOnClick() {

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .start(this);

    }


    @OnClick(R.id.btn_upload_file)
    public void uploadFileOnClick() {
        filePathFlag = QUALIFICATION_FLAG;
        MyDialog.showListingDialog(this, getString(R.string.select_file_type), listFileType, new ListCallback() {
            @Override
            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                if (position == 0) {
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setActivityTheme(R.style.AppTheme)
                            .enableCameraSupport(false)
                            .pickPhoto(ProfileEditActivity.this);

                } else {
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setActivityTheme(R.style.AppTheme)
                            .pickFile(ProfileEditActivity.this);
                }
            }
        });
    }

    @OnClick(R.id.btn_receipt)
    public void uploadReceiptOnClick() {
        filePathFlag = RECEIPT_FLAG;
        MyDialog.showListingDialog(this, getString(R.string.select_file_type), listFileType, new ListCallback() {
            @Override
            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                if (position == 0) {
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setActivityTheme(R.style.AppTheme)
                            .enableCameraSupport(false)
                            .pickPhoto(ProfileEditActivity.this);

                } else {
                    FilePickerBuilder.getInstance().setMaxCount(1)
                            .setActivityTheme(R.style.AppTheme)
                            .pickFile(ProfileEditActivity.this);
                }
            }
        });
    }



    @OnClick(R.id.tv_add_timeslot)
    public void addTimeSlotOnClick() {

        showDayListingDialog();

    }

    @OnClick(R.id.tv_faq)
    public void showFAQ(){

            MyDialog.showRemindDialog(this,getString(R.string.faq),getString(R.string.faq_remind));

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE:
                break;

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:

                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                final ContentResolver contentResolver = this.getContentResolver();
                if (resultCode == RESULT_OK) {
                    final Uri resultUri = result.getUri();
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                profileImage = MediaStore.Images.Media.getBitmap(contentResolver, resultUri);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    tvPhotoPath.setText(getFileName(result.getOriginalUri()));

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                    MyDialog.showErrorDialog(this, error.getMessage());
                }
                break;

            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {

                    filePath = new ArrayList<>();
                    filePath.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));

                    if (isFileTypeAccepted(filePath.get(0))) {
                        setFilePath();
                    }
                }else {
                        initFilePath();
                }
                break;

            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    filePath = new ArrayList<>();
                    filePath.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                    final File imgFile = new File(filePath.get(0));
                    if (imgFile.exists()) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                setImagePath(imgFile.getAbsolutePath());
                            }
                        });
                    }
                }else {
                    initFilePath();
                }
                break;

        }
    }

    private boolean passwordIsMatch() {

        return etNewPassword.getDetail().equals(etConfirmPassword.getDetail());
    }


    @SuppressWarnings("unchecked")
    private UserBean getInputData() {

        UserBean userBean = new UserBean();

        userBean.setId(Shared.read(Constants.SP_UNIQUE_ID));
        userBean.setToken(Shared.read(Constants.SP_TOKEN));
        userBean.setEmail(this.userBean.getEmail());
        userBean.setName(etName.getDetail());
        userBean.setPassword(etPassword.getDetail());
        userBean.setConfirm_password(etConfirmPassword.getDetail());
        userBean.setContact(etContact.getDetail());
        userBean.setGender(rgGender.getCheckedRadioButtonId() == R.id.btn_male ? "M" : "F");
        userBean.setAge(etAge.getDetail());
        userBean.setDescription(etDesc.getDetail());
        userBean.setExperience(etExperience.getDetail());
        userBean.setNationality_id(Tools.getDataId(listCountry, etCountry.getDetail()) + "");
        userBean.setCategory_id(Tools.getDataId(listCategory, etCategory.getDetail()) + "");
        userBean.setRace_id(Tools.getDataId(lisRace, etRace.getDetail()) + "");
        userBean.setQualification_id(Tools.getDataId(lisQualification, etQualification.getDetail()) + "");
        userBean.setAddress(etAddress.getDetail());
        userBean.setProfilePicture(getImage(resize(profileImage,240,240)));
        userBean.setNric(etNric.getDetail());
        userBean.setFilePath(qualificationPath);
        userBean.setReceipt(receiptPath);
        userBean.setBudget(etBudget.getDetail());
        return userBean;
    }


    private void setGender(String gender) {

        if (gender.equals("F")) {

            btnFemale.setChecked(true);
        } else {
            btnMale.setChecked(true);
        }
    }

    private void hideComponent() {

        if (userBean.getType() == Constants.STUDENT) {
            etDesc.setVisibility(View.GONE);
            etExperience.setVisibility(View.GONE);
            etQualification.setVisibility(View.GONE);
            etNric.setVisibility(View.GONE);
            etCategory.setVisibility(View.GONE);
            llFileSelector.setVisibility(View.GONE);
            llReceipt.setVisibility(View.GONE);

            setTimeSlot();
        } else {
            tvTimeSlot.setVisibility(View.GONE);
            etBudget.setVisibility(View.GONE);
            tvFaq.setVisibility(View.GONE);
        }

        if (userBean.getToken_type() == 1) {

            etPassword.setVisibility(View.GONE);
            etNewPassword.setVisibility(View.GONE);
            etConfirmPassword.setVisibility(View.GONE);

        }
    }

    private void showDayListingDialog() {

        int MAX_CONTAIN = 7;
        if (llTimeSlotContain.getChildCount() < MAX_CONTAIN) {

            MyDialog.showListingDialog(this, getString(R.string.timeslot), Arrays.asList(getResources().getStringArray(R.array.days)), new ListCallback() {
                @Override
                public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                    llTimeSlotContain.addView(addTimeSlot(text.toString(), llTimeSlotContain.getChildCount(), defaultStartTime, defaultEndTime));
                }
            });
        } else {
            MyDialog.showErrorDialog(this, getString(R.string.max_timeslot));
        }

    }


    private void showTimeListingDialog(final TextView tvTimeSlot) {

        new MaterialDialog.Builder(this)
                .title(R.string.timeslot)
                .items(R.array.times)
                .itemsCallback(new ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        tvTimeSlot.setText(text);

                    }
                })
                .show();
    }

    private String getImage(Bitmap bitmap) {
        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
            return Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
        }
        return "";
    }

    @SuppressLint("InflateParams")
    private View addTimeSlot(String day, int position, String startTime, String endTime) {

        View view = getLayoutInflater().inflate(R.layout.view_timeslot_item, null);
        TextView tvDay = (TextView) view.findViewById(R.id.tv_day);
        tvDay.setText(day);

        TextView tvStartTime = (TextView) view.findViewById(R.id.tv_start_time);
        tvStartTime.setText(startTime);
        tvStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeListingDialog((TextView) v);
            }
        });

        TextView tvEndTime = (TextView) view.findViewById(R.id.tv_end_time);
        tvEndTime.setText(endTime);
        tvEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showTimeListingDialog((TextView) v);
            }
        });

        ImageView imageView = (ImageView) view.findViewById(R.id.iv_delete);
        imageView.setTag(position);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llTimeSlotContain.removeViewAt((int) v.getTag());
                resetPosition();

            }
        });
        return view;
    }

    private void setTimeSlot() {
        List<String> list = Arrays.asList(getResources().getStringArray(R.array.days));
        for (int i = 0; i < userBean.getTimeslot().length; i++) {
            TimeSlotBean timeslotBean = userBean.getTimeslot()[i];
            String day = list.get(Integer.parseInt(timeslotBean.getDay()) - 1);
            String startTime = timeslotBean.getStart_time().substring(0, timeslotBean.getStart_time().length() - 3);
            String endTime = timeslotBean.getEnd_time().substring(0, timeslotBean.getEnd_time().length() - 3);
            llTimeSlotContain.addView(addTimeSlot(day, i, startTime, endTime));
        }
    }

    private void resetPosition() {

        for (int i = 0; i < llTimeSlotContain.getChildCount(); i++) {

            ImageView imageView = (ImageView) llTimeSlotContain.getChildAt(i).findViewById(R.id.iv_delete);
            imageView.setTag(i);

        }

    }

    private ArrayList<TimeSlotBean> getTimeSlotBeans() {

        ArrayList<TimeSlotBean> timeSlotBeans = new ArrayList<>();
        TimeSlotBean timeSlotBean;
        for (int i = 0; i < llTimeSlotContain.getChildCount(); i++) {

            View view = llTimeSlotContain.getChildAt(i);
            TextView tvDay = (TextView) view.findViewById(R.id.tv_day);
            TextView tvStartTime = (TextView) view.findViewById(R.id.tv_start_time);
            TextView tvEndTime = (TextView) view.findViewById(R.id.tv_end_time);
            timeSlotBean = new TimeSlotBean();
            timeSlotBean.setDay(getDaysPosition(tvDay.getText().toString()));
            timeSlotBean.setStart_time(tvStartTime.getText().toString() + ":00");
            timeSlotBean.setEnd_time(tvEndTime.getText().toString() + ":00");
            timeSlotBeans.add(timeSlotBean);
        }

        return timeSlotBeans;
    }

    public String getDaysPosition(String day) {

        List<String> list = Arrays.asList(getResources().getStringArray(R.array.days));
        for (int i = 0; i < list.size(); i++) {

            if (list.get(i).equals(day)) {

                return Integer.toString(i + 1);
            }
        }
        return "-1";
    }

    @Override
    public void onClick(View v) {

        MyDialog.showListingDialog(ProfileEditActivity.this, getString(R.string.please_select), getDialogList(v), new MaterialDialog.ListCallback() {
            @Override
            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                textSelect.setText(text.toString());
            }
        });
    }


    @SuppressWarnings("unchecked")
    private List getDialogList(View v) {
        textSelect = (TextView) v;
        switch (((View) v.getParent()).getId()) {

            case R.id.et_race:
                return Tools.getDropDownData(lisRace);

            case R.id.et_category:

                return Tools.getDropDownData(listCategory);

            case R.id.et_country:

                return Tools.getDropDownData(listCountry);

            case R.id.et_qualification:
                return Tools.getDropDownData(lisQualification);

            default:

                return new ArrayList();
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private boolean isFileTypeAccepted(String path) {

        for (String fileType : fileTypes) {
            if (path.contains(fileType)) {
                return true;
            }
        }
        MyDialog.showErrorDialog(this, getString(R.string.file_type_error));
        return false;
    }

    private static byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return bytesArray;
    }

    private Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {

        if (image==null)
            return null;

        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    private void setFilePath(){
        if (filePathFlag==QUALIFICATION_FLAG){


            qualificationPath = Base64.encodeToString(readBytesFromFile(filePath.get(0)), Base64.DEFAULT)+Constants.SEPARATOR+"pdf";
            tvFilePath.setText(filePath.get(0).substring(filePath.get(0).lastIndexOf("/")+1));

        }else if(filePathFlag == RECEIPT_FLAG){

            receiptPath = Base64.encodeToString(readBytesFromFile(filePath.get(0)), Base64.DEFAULT)+Constants.SEPARATOR+"pdf";
            tvReceipt.setText(filePath.get(0).substring(filePath.get(0).lastIndexOf("/")+1));

        }

    }
    
    public void setImagePath(String path){
        
        if (filePathFlag==QUALIFICATION_FLAG){


            qualificationPath = getImage(BitmapFactory.decodeFile(path))+Constants.SEPARATOR+"jpeg";
            tvFilePath.setText(filePath.get(0).substring(filePath.get(0).lastIndexOf("/")+1));

        }else if(filePathFlag == RECEIPT_FLAG){

            receiptPath = getImage(BitmapFactory.decodeFile(path))+Constants.SEPARATOR+"jpeg";
            tvReceipt.setText(filePath.get(0).substring(filePath.get(0).lastIndexOf("/")+1));

        }
    }

    public void initFilePath(){

        if (filePathFlag==QUALIFICATION_FLAG){


            qualificationPath = "";
            tvFilePath.setText(R.string.select_file);

        }else if(filePathFlag == RECEIPT_FLAG){

            receiptPath = "";
            tvReceipt.setText(R.string.select_receipt);

        }

    }
}
