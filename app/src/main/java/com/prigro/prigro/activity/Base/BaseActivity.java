package com.prigro.prigro.activity.Base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.prigro.prigro.R;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.network.NetworkUtils;
import com.prigro.prigro.utils.LanguageUtils;
import com.prigro.prigro.utils.Shared;

import butterknife.ButterKnife;

/**
 * Created by SzHui on 2017/4/5
 */

public abstract class BaseActivity extends AppCompatActivity{

    public static final String KEY_ANIMATION= "0";
    public static final int SLIDE_IN_RIGHT =0;
    public static final int FADE_IN =1;
    private static boolean isForeground;
    private static BaseActivity context;

    protected abstract int getLayoutID();

    protected abstract void initData();

    protected abstract void initView();

    protected abstract void initListener();

    protected ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Shared.initialize(getBaseContext());
        MyLog.d("LanguageUtils.isSetLanguageValue() :: "+LanguageUtils.isSetLanguageValue());
        if (LanguageUtils.isSetLanguageValue()){

            LanguageUtils.setLanguage(this,LanguageUtils.getSettingLocaleValue(Shared.readInt(Constants.SP_LANGUAGE, Constants.LANGUAGE_DEFAULT)));

        }
        transitionAnimation();
        initData();
        dialog = new ProgressDialog(this);
        setContentView(getLayoutID());
        ButterKnife.inject(this);
        initView();
        initListener();
        context = this;
        if (findViewById(R.id.tool_bar)!=null&&findViewById(R.id.tv_app_name)!=null){
           TextView tvAppName =(TextView) findViewById(R.id.tv_app_name);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/cooper_std_black.ttf");
            tvAppName.setTypeface(custom_font);
        }

    }

    public static BaseActivity getContext(){

        return context;

    }

    public void transitionAnimation(){
        Intent intent = getIntent();
        switch (intent.getIntExtra(KEY_ANIMATION,-1)){

            case SLIDE_IN_RIGHT:
                overridePendingTransition(R.anim.activity_slide_in_right, R.anim.stay);
                break;

            case FADE_IN:
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            default:
                overridePendingTransition(R.anim.activity_slide_in_right, R.anim.stay);
        }
    }


    public void showLoadingDialog() {
            if (NetworkUtils.isNetworkAvailable(this)&&dialog != null && !dialog.isShowing()) {
                dialog.setMessage(getString(R.string.loading));
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
    }

    public void dismissLoadingDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void startNextActivity(Class targetActivity,boolean finsih){

        Intent intent = new Intent(BaseActivity.this,targetActivity);
        startActivity(intent);
        if (finsih){
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }




}
