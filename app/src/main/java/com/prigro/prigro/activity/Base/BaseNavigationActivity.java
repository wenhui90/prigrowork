package com.prigro.prigro.activity.Base;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.facebook.login.LoginManager;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.LoginActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.controller.ICallBack;
import com.prigro.prigro.network.CustomVolleyRequest;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.Shared;

/**
 * Created by SzHui on 2017/4/15
 */

public abstract class BaseNavigationActivity extends BaseActivity implements ICallBack {


    public abstract void showSuccessResult(int key, Object... params);

    @Override
    public void showResult(int key, Object... params) {
        MyLog.d(" key :: " + key);
        dismissLoadingDialog();
        if (key != Constants.STATUS_EXCEPTION) {

            showSuccessResult(key, params);
        }
    }

    @Override
    public void showException(int key, Object... params) {
        MyLog.d("showException!!!!!");

        dismissLoadingDialog();
        if (key == Constants.STATUS_ERROR) {

            BaseWSBean baseWSBean = (BaseWSBean) params[0];

            if (!isTokenInvalid(baseWSBean)) {
                MyDialog.showRemindDialog(this, getString(R.string.error), baseWSBean.getStatusMessage());
            }

        } else if (key == Constants.STATUS_EXCEPTION) {
            MyDialog.showRemindDialog(this, getString(R.string.error), params[0].toString());
        }
        dismissLoadingDialog();
    }

    @Override
    public void showUnknownError(int key) {
        MyLog.d("showUnknownError!!!!!");
        dismissLoadingDialog();
        MyDialog.showErrorDialog(this, getString(R.string.unknown_error));
    }

    public void loadImage(Context context, NetworkImageView imageView, String url) {

        ImageLoader imageLoader = CustomVolleyRequest.getInstance(context)
                .getImageLoader();
        imageLoader.get(url, ImageLoader.getImageListener(imageView,
                R.color.bg_gray, android.R.drawable
                        .ic_dialog_alert));
        imageView.setImageUrl(url, imageLoader);
    }


    protected boolean isTokenInvalid(BaseWSBean baseWSBean) {
        if (baseWSBean.getStatusCode().equals("410")) {
            Shared.clear();
            LoginManager.getInstance().logOut();
            MyDialog.showConfirmDialog(this, getString(R.string.error), baseWSBean.getStatusMessage(), new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    Intent intent = new Intent(BaseNavigationActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        finishAffinity();
                    }else {
                        ActivityCompat.finishAffinity(BaseNavigationActivity.this);
                    }
                }
            });
            return true;
        }

        return false;

    }

}
