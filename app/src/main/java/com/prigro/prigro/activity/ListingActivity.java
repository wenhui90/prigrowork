package com.prigro.prigro.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.adapter.ApproveCourseListingAdapter;
import com.prigro.prigro.adapter.StdCourseListingAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.model.GetStdModel;
import com.prigro.prigro.model.SetSeenModel;
import com.prigro.prigro.model.StdCourseDetailModel;
import com.prigro.prigro.utils.SimpleDividerItemDecoration;

import java.util.List;

import butterknife.InjectView;

/**
 * Created by vienhui on 26/05/2017
 */

public class ListingActivity extends BaseNavigationActivity implements CommonRecycleViewAdapter.onItemClickCallBack, CommonRecycleViewAdapter.onViewClickCallBack {

    public static final String LISTING_DATA = "listdata";
    public static final String LISTING_TYPE = "listingtype";
    public static final int TYPE_APPLY = 0;
    public static final int TYPE_APPROVE = 1;


    @InjectView(R.id.rv_listing)
    RecyclerView rvListing;
    @InjectView(R.id.tool_bar)
    Toolbar toolbar;

    private int listType;
    private List<CourseBean> courseBeans;
    private ApproveCourseListingAdapter approveCourseListingAdapter;

    @Override
    public void showSuccessResult(int key, Object... params) {

        switch (key) {

            case Constants.STD_COURSE_DETAIL_MODEL:
                Intent intent2 = new Intent(this, TutorCourseDetailActivity.class);
                intent2.putExtra(TutorCourseDetailActivity.KEY_COURSE_DETAIL, (CourseBean) params[0]);
                startActivity(intent2);

                break;
            case Constants.GET_STD_MODEL:
                Intent intent = new Intent(this, StdProfileActivity.class);
                intent.putExtra(StdProfileActivity.GET_STD_BEAN, (UserBean) params[0]);
                intent.putExtra(StdProfileActivity.GET_USER_TYPE, true);
                startActivity(intent);
                break;
        }

    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_listing;
    }

    @Override
    protected void initData() {

        Intent intent = getIntent();
        courseBeans = (List<CourseBean>) intent.getSerializableExtra(LISTING_DATA);
        listType = intent.getIntExtra(LISTING_TYPE, -1);


    }

    @Override
    protected void initView() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        rvListing.addItemDecoration(new SimpleDividerItemDecoration(this));
        rvListing.setLayoutManager(new LinearLayoutManager(this));

        if (listType == TYPE_APPLY) {

            StdCourseListingAdapter stdListingAdapter = new StdCourseListingAdapter(courseBeans, false);
            stdListingAdapter.setOnItemClickCallBack(this);
            rvListing.setAdapter(stdListingAdapter);

        } else if (listType == TYPE_APPROVE) {
            approveCourseListingAdapter = new ApproveCourseListingAdapter(courseBeans);
            approveCourseListingAdapter.setOnItemClickCallBack(this);
            approveCourseListingAdapter.setOnViewClickCallBack(this);
            rvListing.setAdapter(approveCourseListingAdapter);
        }

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onItemClick(int position) {
        showLoadingDialog();
        if (listType==TYPE_APPROVE){
            SetSeenModel setSeenModel = new SetSeenModel(this,Integer.toString(position));
            setSeenModel.setICallBackListener(this);
            setSeenModel.executeData();
            itemSetSeen(Integer.toString(position));
        }
        StdCourseDetailModel stdCourseDetailModel = new StdCourseDetailModel(this, Integer.toString(position));
        stdCourseDetailModel.setICallBackListener(this);
        stdCourseDetailModel.executeData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(View v) {
        showLoadingDialog();
        GetStdModel getStdModel = new GetStdModel(this, v.getTag().toString());
        getStdModel.setICallBackListener(this);
        getStdModel.executeData();
    }


    private void itemSetSeen(String listingId){

        for (int i = 0; i <courseBeans.size() ; i++) {

            if (courseBeans.get(i).getId().equals(listingId)){

                courseBeans.get(i).setSeen("1");
            }
        }

        approveCourseListingAdapter.notifyDataSetChanged();
    }
}
