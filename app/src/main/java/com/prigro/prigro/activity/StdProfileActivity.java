package com.prigro.prigro.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.bean.DropDownContentBean;
import com.prigro.prigro.bean.TimeSlotBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.fragment.main.TutorCourseListingFragment;
import com.prigro.prigro.model.GetDropDownModel;
import com.prigro.prigro.ui.ListingDetailTextView;
import com.prigro.prigro.utils.Tools;

import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

import static com.prigro.prigro.activity.MainActivity.KEY_LISTING_DETAIL;

/**
 */

public class StdProfileActivity extends BaseNavigationActivity {

    public static final String GET_STD_BEAN = "getstdbean";
    public static final String GET_USER_TYPE = "getusertype";
    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.tv_name)
    ListingDetailTextView tvName;
    @InjectView(R.id.tv_email)
    ListingDetailTextView tvEmail;
    @InjectView(R.id.tv_age)
    ListingDetailTextView tvAge;
    @InjectView(R.id.tv_gender)
    ListingDetailTextView tvgender;
    @InjectView(R.id.tv_contact)
    ListingDetailTextView tvContact;
    @InjectView(R.id.tv_race)
    ListingDetailTextView tvRace;
    @InjectView(R.id.tv_national)
    ListingDetailTextView tvNational;
    @InjectView(R.id.ll_contact_contain)
    LinearLayout llContactContain;
    @InjectView(R.id.tv_address)
    ListingDetailTextView tvAddress;
    @InjectView(R.id.ll_timeslot_contain)
    LinearLayout llTimeSlotContain;
    @InjectView(R.id.tv_budget)
    ListingDetailTextView tvBudget;
    @InjectView(R.id.iv_profile_picture)
    NetworkImageView networkImageView;
    @InjectView(R.id.tv_time_slot)
    ListingDetailTextView tvTimeSlot;

    private UserBean userBean;
    private boolean isTutor;

    @Override
    public void showSuccessResult(int key, Object... params) {
        dismissLoadingDialog();

        switch (key) {
            case Constants.GET_DROPDOWN_TYPE:
                Intent initEditProfile = new Intent(this, ProfileEditActivity.class);
                initEditProfile.putExtra(StdAddCourseActivity.KEY_GET_DROP_DOWN,(DropDownContentBean)params[0]);
                initEditProfile.putExtra(ProfileEditActivity.KEY_USER_BEAN, userBean);
                startActivity(initEditProfile);

                break;
        }
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_std_profile;
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        isTutor = intent.getBooleanExtra(GET_USER_TYPE, false);
        userBean = (UserBean) intent.getSerializableExtra(GET_STD_BEAN);
    }

    @Override
    protected void initView() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvName.setDetail(userBean.getName());
        tvEmail.setDetail(userBean.getEmail());
        tvAge.setDetail(userBean.getAge());
        tvgender.setDetail(userBean.getGender());
        tvContact.setDetail(userBean.getContact());
        tvRace.setDetail(userBean.getRace_name());
        tvAddress.setDetail(userBean.getAddress());
        tvNational.setDetail(userBean.getNationality_name());
        tvBudget.setDetail(userBean.getBudget());
        llContactContain.setVisibility(isTutor?View.VISIBLE:View.GONE);

        networkImageView.setImageBitmap(Tools.getRoundedShape(BitmapFactory.decodeResource(getResources(),
                R.drawable.place_holder)));
        loadImage(this,networkImageView,userBean.getProfilePicture());
        setTimeSlot();

    }

    @Override
    protected void initListener() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (!isTutor) {
            getMenuInflater().inflate(R.menu.edit_profile, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menu_edit:
                showLoadingDialog();
                GetDropDownModel getDropDownModel = new GetDropDownModel(this);
                getDropDownModel.setICallBackListener(this);
                getDropDownModel.executeData();
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.tv_phone_call)
    public void phoneCallOnClick() {

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + userBean.getContact()));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }


    @OnClick(R.id.tv_send_email)
    public void emailOnClick(){

//        Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.setType("text/plain");
//        intent.putExtra(Intent.EXTRA_EMAIL,userBean.getEmail());
//        startActivityForResult(intent,Constants.REQUEST_USER_CODE);
        sendSMS();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

          case  Constants.REQUEST_USER_CODE:
              if (resultCode==RESULT_OK){

                  ((TutorCourseListingFragment)TutorCourseListingFragment.getInstance()).setListingIsApplied(data.getStringExtra(KEY_LISTING_DETAIL));
              }
            break;
        }

    }



    private View addTimeSlot(String day, String startTime, String endTime) {

        View view = getLayoutInflater().inflate(R.layout.view_timeslot_item, null);
        TextView tvDay = (TextView) view.findViewById(R.id.tv_day);
        tvDay.setText(day);
        TextView tvStartTime = (TextView) view.findViewById(R.id.tv_start_time);
        tvStartTime.setText(startTime);
        TextView tvEndTime = (TextView) view.findViewById(R.id.tv_end_time);
        tvEndTime.setText(endTime);

        ImageView imageView = (ImageView) view.findViewById(R.id.iv_delete);
        imageView.setVisibility(View.GONE);
        return view;
    }

    private void setTimeSlot() {


        if (userBean.getTimeslot()==null||userBean.getTimeslot().length<=0) {

            tvTimeSlot.setDetail(getString(R.string.not_preference));
            tvTimeSlot.setDetailVisible(true);
            return;
        }


        List<String> list = Arrays.asList(getResources().getStringArray(R.array.days));
        for (int i = 0; i < userBean.getTimeslot().length; i++) {
            TimeSlotBean timeslotBean = userBean.getTimeslot()[i];
            String day = list.get(Integer.parseInt(timeslotBean.getDay())-1);
            String startTime = timeslotBean.getStart_time().substring(0, timeslotBean.getStart_time().length() - 3);
            String endTime = timeslotBean.getEnd_time().substring(0, timeslotBean.getEnd_time().length() - 3);
            llTimeSlotContain.addView(addTimeSlot(day, startTime, endTime));
        }
    }


    private void sendSMS() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
        {
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this); // Need to change the build to API 19

            Uri uri = Uri.parse("smsto:"+userBean.getContact());
            Intent it = new Intent(Intent.ACTION_SENDTO, uri);
            startActivity(it);

            if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
            // any app that support this intent.
            {
                it.setPackage(defaultSmsPackageName);
            }
            startActivity(it);

        }
        else // For early versions, do what worked for you before.
        {
            Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("address",userBean.getContact());
            startActivity(smsIntent);
        }
    }
}
