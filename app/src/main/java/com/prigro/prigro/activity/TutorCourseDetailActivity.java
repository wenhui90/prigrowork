package com.prigro.prigro.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.bean.BaseWSBean;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.TimeSlotBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.fragment.main.TutorDashBoardFragment;
import com.prigro.prigro.model.ApplyCourseModel;
import com.prigro.prigro.ui.ListingDetailTextView;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.Tools;

import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by SzHui on 2017/4/15
 */

public class TutorCourseDetailActivity extends BaseNavigationActivity implements OnMapReadyCallback {

    public static final String KEY_COURSE_DETAIL = "coursedetail";
    public static final String KEY_IS_GUEST ="isguest";

    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.tv_subject)
    ListingDetailTextView tvSubject;
    @InjectView(R.id.tv_title)
    ListingDetailTextView tvTitle;
    @InjectView(R.id.tv_level)
    ListingDetailTextView tvLevel;
    @InjectView(R.id.tv_gender)
    ListingDetailTextView tvGender;
    @InjectView(R.id.tv_lesson_type)
    ListingDetailTextView tvLessonType;
    @InjectView(R.id.tv_location)
    ListingDetailTextView tvLocation;
    @InjectView(R.id.tv_lesson_per_week)
    ListingDetailTextView tvLessonPerWeek;
    @InjectView(R.id.tv_hour_per_lesson)
    ListingDetailTextView tvHourPerLesson;
    @InjectView(R.id.tv_price_bid)
    ListingDetailTextView tvPriceBid;
    @InjectView(R.id.tv_budget)
    ListingDetailTextView tvBudget;
    @InjectView(R.id.tv_time_slot)
    ListingDetailTextView tvTimeSlot;

    @InjectView(R.id.ll_timeslot_contain)
    LinearLayout llTimeSlotContain;
    @InjectView(R.id.fab)
    FloatingActionButton fab;
    private CourseBean courseBean;
    private boolean isGuest;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_tutor_course_detail;
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        courseBean = (CourseBean) intent.getSerializableExtra(KEY_COURSE_DETAIL);
        isGuest = intent.getBooleanExtra(KEY_IS_GUEST,false);
    }

    @Override
    protected void initView() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map);
        mapFragment.getMapAsync(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tvSubject.setDetail(courseBean.getSubject_name());
        tvTitle.setDetail(courseBean.getTitle());
        tvLevel.setDetail(courseBean.getLevel_name());
        tvGender.setDetail(Tools.getGender(this,courseBean.getPreferred_gender()));
        tvLessonType.setDetail(courseBean.getLesson_type_name());
        tvLocation.setDetail(courseBean.getLocation());
        tvLessonPerWeek.setDetail(courseBean.getLesson_per_week()+"");
        tvHourPerLesson.setDetail(courseBean.getHour_per_lesson());
        tvBudget.setDetail(courseBean.getBudget_per_month());
        setTimeSlot();
        if (courseBean.getApplied()!=null&&courseBean.getApplied().equals(Constants.APPLIED)){
            fab.setImageResource(R.mipmap.ic_edit_white);
            tvPriceBid.setVisibility(View.VISIBLE);
            tvPriceBid.setDetail("RM"+courseBean.getRates());
        }

        if (isGuest||(courseBean.getAccepted()!=null&&!courseBean.getAccepted().equals(Constants.APPROVE))){
            mapFragment.getView().setVisibility(View.GONE);
        }else {
            fab.setVisibility(View.GONE);
        }

        fab.setVisibility(isGuest||courseBean.getAccepted().equals(Constants.APPROVE)?View.GONE:View.VISIBLE);

    }

    @Override
    protected void initListener() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isGuest){
                    Intent intentPayment = new Intent(TutorCourseDetailActivity.this, PaymentInfoActivity.class);
                    intentPayment.putExtra(PaymentInfoActivity.KEY_IS_INACTIVE,true);
                    startActivity(intentPayment);
                }else {
                    showInputDialog();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showSuccessResult(int key, Object... params) {
        MyDialog.showConfirmDialog(this, getString(R.string.success), (String) params[0], new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                Intent intent = new Intent(TutorCourseDetailActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra(MainActivity.KEY_LISTING_DETAIL,courseBean.getId());
                setResult(RESULT_OK,intent);
                finish();
            }
        });
        TutorDashBoardFragment.getInstance(null).refreshDashboardData();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (courseBean.getLatitude()!=null&&courseBean.getLongitude()!=null){

            LatLng latLng = new LatLng(Double.parseDouble(courseBean.getLatitude()), Double.parseDouble(courseBean.getLongitude()));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        }
        googleMap.getUiSettings().setScrollGesturesEnabled(false);

    }

    private void setTimeSlot() {

        if (courseBean.getTimeslot()==null||courseBean.getTimeslot().length<=0) {

            tvTimeSlot.setDetail(getString(R.string.not_preference));
            tvTimeSlot.setDetailVisible(true);
            return;
        }


        List<String> list = Arrays.asList(getResources().getStringArray(R.array.days));
        for (int i = 0; i < courseBean.getTimeslot().length; i++) {
            TimeSlotBean timeslotBean = courseBean.getTimeslot()[i];
            String day = list.get(Integer.parseInt(timeslotBean.getDay())-1);
            String startTime = timeslotBean.getStart_time().substring(0, timeslotBean.getStart_time().length() - 3);
            String endTime = timeslotBean.getEnd_time().substring(0, timeslotBean.getEnd_time().length() - 3);
            llTimeSlotContain.addView(getTimeSlot(day, startTime, endTime));
        }
    }


    private View getTimeSlot(String day, String startTime, String endTime) {

        View view = getLayoutInflater().inflate(R.layout.view_timeslot_item, null);
        TextView tvDay = (TextView) view.findViewById(R.id.tv_day);
        tvDay.setText(day);
        TextView tvStartTime = (TextView) view.findViewById(R.id.tv_start_time);
        tvStartTime.setText(startTime);
        TextView tvEndTime = (TextView) view.findViewById(R.id.tv_end_time);
        tvEndTime.setText(endTime);

        ImageView imageView = (ImageView) view.findViewById(R.id.iv_delete);
        imageView.setVisibility(View.GONE);
        return view;
    }

    public void showInputDialog() {

        new MaterialDialog.Builder(this)
                .title(getString(R.string.input_fee))
                .content(getInputDialogMessage())
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .input(getString(R.string.fee), "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
                    }
                }).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                if (dialog.getInputEditText().getText().toString().length() > 0) {
                    showLoadingDialog();
                    ApplyCourseModel applyCourseModel = new ApplyCourseModel(
                            TutorCourseDetailActivity.this, courseBean.getId(),
                            dialog.getInputEditText().getText().toString(), courseBean.getApplied().equals(Constants.APPLIED));

                    applyCourseModel.setICallBackListener(TutorCourseDetailActivity.this);
                    applyCourseModel.executeData();
                    dialog.dismiss();
                }
            }
        }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        }).negativeText(getString(R.string.cancel )).positiveText(R.string.confirm).show();
    }


    private String getInputDialogMessage(){

        if (!courseBean.getApplied().equals(Constants.APPLIED)){
            return getString(R.string.input_charge);

        }else {
            return "Your last charge is RM"+courseBean.getRates()+",do you want to edit it?";

        }
    }

    @Override
    public void showException(int key, Object... params) {
            dismissLoadingDialog();
        if (key == Constants.STATUS_ERROR&&((BaseWSBean)params[0]).getStatusCode().equals("440")){

            showActiveDialog(((BaseWSBean)params[0]).getStatusMessage());
            return;
        }

        super.showException(key, params);
    }

    private void showActiveDialog(String message){

        new MaterialDialog.Builder(this)
                .title(R.string.error)
                .cancelable(false)
                .theme(Theme.LIGHT)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dismissLoadingDialog();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent intent = new Intent(TutorCourseDetailActivity.this,PaymentInfoActivity.class);
                        intent.putExtra(PaymentInfoActivity.KEY_IS_INACTIVE,true);
                        startActivity(intent);
                    }
                })
                .content(message)
                .positiveText(android.R.string.ok)
                .positiveColor(getResources().getColor(R.color.skyBlue))
                .negativeText(R.string.active_now)
                .negativeColor(getResources().getColor(R.color.backgroundGrey))
                .cancelable(true)
                .show();
    }
}
