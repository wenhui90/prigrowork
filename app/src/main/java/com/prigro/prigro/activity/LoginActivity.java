package com.prigro.prigro.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.bean.DropDownContentBean;
import com.prigro.prigro.bean.LoginRegisterBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.common.MyLog;
import com.prigro.prigro.model.AdsModel;
import com.prigro.prigro.model.EmailLoginModel;
import com.prigro.prigro.model.FacebookLoginModel;
import com.prigro.prigro.model.FacebookUserTypeAuthModel;
import com.prigro.prigro.model.GetDropDownModel;
import com.prigro.prigro.model.GetStdModel;
import com.prigro.prigro.model.GetTutorModel;
import com.prigro.prigro.model.GuestLoginModel;
import com.prigro.prigro.utils.ByteDataDownloader;
import com.prigro.prigro.utils.GPSTracker;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.PopupAdsWindow;
import com.prigro.prigro.utils.Shared;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.InjectView;
import butterknife.OnClick;

import static com.prigro.prigro.activity.RegisterActivity.KEY_REGISTER;
import static java.util.Arrays.asList;

/**
 * Created by vienhui on 12/06/2017
 */

public class LoginActivity extends BaseNavigationActivity implements View.OnClickListener, ByteDataDownloader.ByteLoadingListener {

    @InjectView(R.id.rl_contain)
    RelativeLayout rlContain;

    private GPSTracker gpsTracker;
    private LoginButton loginButton;
    private AccessToken accessToken;
    private UserBean fbUserBean;
    private CallbackManager callbackManager;
    private EditText etEmail;
    private EditText etPassword;
    private AdsModel adsModel;
    private boolean isGif;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_login_type;
    }

    @Override
    protected void initData() {
        MyLog.d("::::::: FireBase Token ::::::: "+ FirebaseInstanceId.getInstance().getToken());
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    @Override
    protected void initView() {

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(asList(
                "public_profile", "email"));
        callbackManager = CallbackManager.Factory.create();


        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                MyLog.d("loginResult :: " + loginResult);
                getUserData(loginResult);
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {

                MyDialog.showErrorDialog(LoginActivity.this, error.getMessage());

            }
        });

        resizeFacebookLoginButton();
        adsModel = new AdsModel(this);
        adsModel.setICallBackListener(this);
        adsModel.executeData();
    }

    @Override
    protected void initListener() {

    }

    @OnClick(R.id.btn_login_email)
    public void emailLoginOnClick() {

//        startNextActivity(LoginActivity.class,true);
        showPopUpWindow();


    }

    @OnClick(R.id.btn_login_guest)
    public void guestLoginOnClick() {
        showLoadingDialog();
        getGuestListing();
    }

    @Override
    public void showSuccessResult(int key, final Object... params) {

        switch (key) {

            case Constants.FB_SUCCESS_ADD_TYPE:
                LoginRegisterBean userBean1 = new LoginRegisterBean();
                userBean1.setId(fbUserBean.getId());
                userBean1.setToken(fbUserBean.getToken());
                userBean1.setType(fbUserBean.getType());
                saveDataToSP(userBean1);
                getUserDetail(userBean1);
                break;

            case Constants.FB_ADD_TYPE:

                new MaterialDialog.Builder(this)
                        .title(R.string.select_account_type)
                        .items(R.array.types)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                showLoadingDialog();
                                UserBean userBean1 = (UserBean) params[0];
                                userBean1.setToken(accessToken.getToken());
                                userBean1.setType(which == 0 ? Constants.TUTOR : Constants.STUDENT);
                                fbUserBean = userBean1;
                                FacebookUserTypeAuthModel facebookUserTypeAuthModel = new FacebookUserTypeAuthModel(LoginActivity.this, userBean1);
                                facebookUserTypeAuthModel.setICallBackListener(LoginActivity.this);
                                facebookUserTypeAuthModel.executeData();
                            }
                        })
                        .show();
                break;

            case Constants.FB_LOGIN_MODEL:
                UserBean statusBean = (UserBean) params[0];
                LoginRegisterBean loginRegisterBean = new LoginRegisterBean();
                loginRegisterBean.setId(statusBean.getId());
                loginRegisterBean.setType(statusBean.getType());
                loginRegisterBean.setToken(accessToken.getToken());
                saveDataToSP(loginRegisterBean);
                getUserDetail(loginRegisterBean);
                break;

            case Constants.GUEST_LOGIN:
                Intent intent = new Intent(LoginActivity.this, GuestListingActivity.class);
                intent.putExtra(GuestListingActivity.KEY_LISTING, params);
                startActivity(intent);
                break;

            case Constants.GET_TUTOR_MODEL:
                Intent intentTutor = new Intent(LoginActivity.this, MainActivity.class);
                intentTutor.putExtra(MainActivity.KEY_USER_BEAN, (UserBean) params[0]);
                startActivity(intentTutor);
                break;

            case Constants.GET_STD_MODEL:
                Intent intentStd = new Intent(LoginActivity.this, MainActivity.class);
                intentStd.putExtra(MainActivity.KEY_USER_BEAN, (UserBean) params[0]);
                startActivity(intentStd);
                break;

            case Constants.EMAIL_LOGIN__MODEL:
                LoginRegisterBean userBean = (LoginRegisterBean) params[0];
                saveDataToSP(userBean);
                getUserDetail(userBean);
                break;

            case Constants.GET_DROPDOWN_TYPE:
                Intent intentRegister = new Intent(this, RegisterActivity.class);
                intentRegister.putExtra(KEY_REGISTER,(DropDownContentBean)params[0]);
                startActivity(intentRegister);
                break;


            case Constants.ADS_MODEL:
//                String temp ="https://ichef.bbci.co.uk/news/660/cpsprodpb/37B5/production/_89716241_thinkstockphotos-523060154.jpg";
                final String temp = (String) params[0];
//                String temp ="http://katemobile.ru/tmp/sample3.gif";
                isGif = temp.substring(temp.length() - 3).equals("gif");
                MyLog.d("isGif :: "+isGif);
                ByteDataDownloader byteDataDownloader = new ByteDataDownloader();
                byteDataDownloader.execute(temp);
                byteDataDownloader.setOnSuccessLoadingListener(this);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressWarnings("deprecation")
    private void resizeFacebookLoginButton() {
        float fbIconScale = 1.25F;
        Drawable drawable = getResources().getDrawable(
                com.facebook.R.drawable.com_facebook_button_icon);
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * fbIconScale),
                (int) (drawable.getIntrinsicHeight() * fbIconScale));
        loginButton.setCompoundDrawables(drawable, null, null, null);
        loginButton.setCompoundDrawablePadding(getResources().
                getDimensionPixelSize(R.dimen.fb_margin_override_textpadding));
        loginButton.setPadding(
                getResources().getDimensionPixelSize(
                        R.dimen.fb_margin_override_lr),
                getResources().getDimensionPixelSize(
                        R.dimen.fb_margin_override_top),
                0, getResources().getDimensionPixelSize(
                        R.dimen.fb_margin_override_bottom));
    }

    private void getUserData(LoginResult loginResult) {
        accessToken = loginResult.getAccessToken();
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                MyLog.d("response ::" + response.toString());
                if (object != null) {
                    MyLog.d("object ::" + object.toString());
                    LoginRegisterBean loginRegisterBean = new LoginRegisterBean();
                    try {
                        loginRegisterBean.setName(object.has("name") ? object.getString("name") : "");
                        loginRegisterBean.setEmail(object.has("email") ? object.getString("email") : "");
                        loginRegisterBean.setGender(object.has("gender") ? object.getString("gender") : "");
                        showLoadingDialog();
                        FacebookLoginModel facebookLoginModel = new FacebookLoginModel(LoginActivity.this, loginRegisterBean, accessToken.getToken());
                        facebookLoginModel.setICallBackListener(LoginActivity.this);
                        facebookLoginModel.executeData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();

    }


    @Override
    public void onBackPressed() {
        finish();
    }

    private void saveDataToSP(LoginRegisterBean userBean) {

        Shared.write(Constants.SP_TOKEN, userBean.getToken());
        Shared.write(Constants.SP_UNIQUE_ID, userBean.getId());
        Shared.writeInt(Constants.SP_USER_TYPE, userBean.getType());
    }


    private void getUserDetail(LoginRegisterBean userBean) {
        showLoadingDialog();
        if (userBean.getType() == Constants.TUTOR) {
            GetTutorModel getTutorModel = new GetTutorModel(this);
            getTutorModel.setICallBackListener(this);
            getTutorModel.executeData();
        } else if (userBean.getType() == Constants.STUDENT) {

            GetStdModel getStdModel = new GetStdModel(this);
            getStdModel.setICallBackListener(this);
            getStdModel.executeData();
        }
    }


    private void getGuestListing() {
        String latitude = "";
        String longitude = "";
        if (gpsTracker.getIsGPSTrackingEnabled()) {
            latitude = String.valueOf(gpsTracker.getLatitude());
            longitude = String.valueOf(gpsTracker.getLongitude());
        }

        GuestLoginModel guestLoginModel = new GuestLoginModel(this, longitude, latitude);
        guestLoginModel.setICallBackListener(this);
        guestLoginModel.executeData();
    }


    @Override
    public void onPause() {
        super.onPause();
        if (gpsTracker != null)
            gpsTracker.stopUsingGPS();
    }

    @Override
    public void onResume() {
        super.onResume();
        gpsTracker = new GPSTracker(this);
        if (!gpsTracker.getIsGPSTrackingEnabled()) {
            gpsTracker.showSettingsAlert();
        }

    }

    public void showPopUpWindow() {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popUpContentView = inflater.inflate(R.layout.pop_up_login, null);
        final PopupWindow popupWindow = new PopupWindow(popUpContentView, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(false);
        popUpContentView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    popupWindow.dismiss();
                    return true;
                }
                return false;
            }
        });

        popupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popupWindow.setAnimationStyle(R.style.anim_menu_bottombar);
        popupWindow.showAtLocation(rlContain, Gravity.BOTTOM, 0, 0);
        setPopUpItemListener(popUpContentView);

    }

    public void setPopUpItemListener(View popupWindow) {

        Button btnLogin = (Button) popupWindow.findViewById(R.id.btn_login);
        TextView tvForgetPassword = (TextView) popupWindow.findViewById(R.id.tv_forgot_password);
        TextView tvRegisterEmail = (TextView) popupWindow.findViewById(R.id.tv_register);
        etEmail = (EditText) popupWindow.findViewById(R.id.et_email);
        etPassword = (EditText) popupWindow.findViewById(R.id.et_password);
        btnLogin.setOnClickListener(this);
        tvForgetPassword.setOnClickListener(this);
        tvRegisterEmail.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                handleUserData();
                break;

            case R.id.tv_forgot_password:
                Intent intent = new Intent(this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;

            case R.id.tv_register:
                showLoadingDialog();
                GetDropDownModel getDropDownModel = new GetDropDownModel(this);
                getDropDownModel.setICallBackListener(this);
                getDropDownModel.executeData();
                break;

        }
    }
    private void handleUserData() {

        if (etEmail.getText().toString().isEmpty() || etPassword.getText().toString().isEmpty()) {

            MyDialog.showErrorDialog(this, getString(R.string.email_password) + getString(R.string.cant_empty));
        } else {
            showLoadingDialog();
            EmailLoginModel emailLoginModel = new EmailLoginModel(this, new LoginRegisterBean(etEmail.getText().toString(), etPassword.getText().toString()));
            emailLoginModel.setICallBackListener(this);
            emailLoginModel.executeData();
        }
    }

    @Override
    public void onByteLoadFinish(byte[] imgvData) {

        PopupAdsWindow popupAdsWindow = new PopupAdsWindow(getLayoutInflater(), rlContain, imgvData, isGif);
        popupAdsWindow.popUpWindow();
    }
}
