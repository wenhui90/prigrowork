package com.prigro.prigro.activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.model.ForgetPasswordModel;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.Tools;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by vienhui on 04/05/2017
 */

public class ForgotPasswordActivity extends BaseNavigationActivity {


    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.et_email)
    EditText etEmail;


    @Override
    public void showSuccessResult(int key, Object... params) {
        MyDialog.showConfirmDialog(this, getString(R.string.success), (String) params[0], new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                onBackPressed();
            }
        });
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_forgot_password;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void initListener() {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_submit)
    public void submitOnClick() {

        if (Tools.isValidEmailAddress(etEmail.getText().toString(),this)) {
            showLoadingDialog();
            ForgetPasswordModel forgetPasswordModel = new ForgetPasswordModel(this, etEmail.getText().toString());
            forgetPasswordModel.setICallBackListener(this);
            forgetPasswordModel.executeData();
        }

    }
}
