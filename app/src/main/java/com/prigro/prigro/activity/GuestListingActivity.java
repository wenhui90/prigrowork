package com.prigro.prigro.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.adapter.TutorCourseListingAdapter;
import com.prigro.prigro.adapter.common.CommonRecycleViewAdapter;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.model.GuestListingModel;
import com.prigro.prigro.utils.SimpleDividerItemDecoration;

import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by vienhui on 13/06/2017
 */

public class GuestListingActivity extends BaseNavigationActivity implements CommonRecycleViewAdapter.onItemClickCallBack {

    public static final String KEY_LISTING = "listing";

    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.rv_listing)
    RecyclerView rvListing;

    private List<CourseBean> listCourse;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_guest_listing;
    }

    @Override
    protected void initData() {
        CourseBean[] courseBean = (CourseBean[]) getIntent().getSerializableExtra(KEY_LISTING);
        listCourse = Arrays.asList(courseBean);
    }

    @Override
    protected void initView() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TutorCourseListingAdapter tutorCourseListingAdapter = new TutorCourseListingAdapter(listCourse,true,this);
        tutorCourseListingAdapter.setOnItemClickCallBack(this);
        rvListing.addItemDecoration(new SimpleDividerItemDecoration(this));
        rvListing.setLayoutManager(new LinearLayoutManager(this));
        rvListing.setAdapter(tutorCourseListingAdapter);
    }

    @Override
    protected void initListener() {

    }

    @Override
    public void showSuccessResult(int key, Object... params) {
        switch (key) {
            case Constants.STD_COURSE_DETAIL_MODEL:
                Intent intentDetail = new Intent(this, TutorCourseDetailActivity.class);
                intentDetail.putExtra(TutorCourseDetailActivity.KEY_COURSE_DETAIL, (CourseBean) params[0]);
                intentDetail.putExtra(TutorCourseDetailActivity.KEY_IS_GUEST,true);
                startActivity(intentDetail);
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        showLoadingDialog();
        GuestListingModel guestListingModel = new GuestListingModel(this, Integer.toString(position));
        guestListingModel.setICallBackListener(this);
        guestListingModel.executeData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
