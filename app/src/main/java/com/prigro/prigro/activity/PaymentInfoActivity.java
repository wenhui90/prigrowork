package com.prigro.prigro.activity;

import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseActivity;

import butterknife.OnClick;

/**
 * Created by vienhui on 22/05/2017
 */

public class PaymentInfoActivity extends BaseActivity {

    public final static String KEY_IS_INACTIVE = "inactive";

    private boolean isInActive;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_payment_info;
    }

    @Override
    protected void initData() {
        isInActive = getIntent().getBooleanExtra(KEY_IS_INACTIVE,false);
    }

    @Override
    protected void initView() {
    }

    @Override
    protected void initListener() {

    }

    @OnClick(R.id.btn_continue)
    public void continueOnClick(){

        finish();


    }
}
