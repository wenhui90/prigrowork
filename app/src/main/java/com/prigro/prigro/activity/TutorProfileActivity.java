package com.prigro.prigro.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.bean.DropDownContentBean;
import com.prigro.prigro.bean.UserBean;
import com.prigro.prigro.common.Constants;
import com.prigro.prigro.model.GetDropDownModel;
import com.prigro.prigro.ui.ListingDetailTextView;
import com.prigro.prigro.utils.Tools;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by SzHui on 2017/4/15
 */

public class TutorProfileActivity extends BaseNavigationActivity {

    public static final String GET_TUTOR_BEAN = "getstdbean";
    public static final String GET_USER_TYPE = "getusertype";
    public static final String GET_APPROVE_STATUS = "getapprovestatus";

    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.tv_name)
    ListingDetailTextView tvName;
    @InjectView(R.id.tv_email)
    ListingDetailTextView tvEmail;
    @InjectView(R.id.tv_race)
    ListingDetailTextView tvRace;
    @InjectView(R.id.tv_contact)
    ListingDetailTextView tvContact;
    @InjectView(R.id.tv_country)
    ListingDetailTextView tvCountry;
    @InjectView(R.id.tv_qualification)
    ListingDetailTextView tvQualification;
    @InjectView(R.id.tv_experience)
    ListingDetailTextView tvExperience;
    @InjectView(R.id.tv_desc)
    ListingDetailTextView tvDesc;
    @InjectView(R.id.tv_address)
    ListingDetailTextView tvAddress;
    @InjectView(R.id.tv_file)
    ListingDetailTextView tvDocument;
    @InjectView(R.id.tv_receipt)
    ListingDetailTextView tvReceipt;
    @InjectView(R.id.tv_age)
    TextView tvAge;
    @InjectView(R.id.tv_gender)
    TextView tvGender;
    @InjectView(R.id.tv_job_type)
    TextView tvJobType;
    @InjectView(R.id.ll_contact_contain)
    LinearLayout llContactContain;

    @InjectView(R.id.iv_user_picture)
    NetworkImageView ivUserPicture;

    UserBean userBean;
    boolean isStudent, isApprove;

    @Override
    protected int getLayoutID() {
        return R.layout.activity_tutor_profile;
    }

    @Override
    protected void initData() {

        Intent intent = getIntent();
        userBean = (UserBean) intent.getSerializableExtra(GET_TUTOR_BEAN);
        isStudent = intent.getBooleanExtra(GET_USER_TYPE, false);
        isApprove = intent.getBooleanExtra(GET_APPROVE_STATUS, false);
    }

    @Override
    protected void initView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tvName.setDetail(userBean.getName());
        tvEmail.setDetail(userBean.getEmail());
        tvContact.setDetail(userBean.getContact());
        tvRace.setDetail(userBean.getRace_name());
        tvExperience.setDetail(userBean.getExperience());
        tvCountry.setDetail(userBean.getNationality_name());
        tvDesc.setDetail(userBean.getDescription());
        tvQualification.setDetail(userBean.getQualification_name());
        tvReceipt.setDetail(userBean.getReceipt());
        tvAddress.setDetail(userBean.getAddress());
        tvDocument.setDetail(userBean.getDocument());
        tvAge.setText(String.format(getString(R.string.ageN), userBean.getAge() != null ? userBean.getAge() : "-"));
        tvGender.setText(String.format(getString(R.string.genderN), userBean.getGender() != null ? userBean.getGender() : "-"));
        tvJobType.setText(String.format(getString(R.string.job_typeN), userBean.getCategory_name() != null ? userBean.getCategory_name() : "-"));
        ivUserPicture.setImageBitmap(Tools.getRoundedShape(BitmapFactory.decodeResource(getResources(),
                R.drawable.place_holder)));
        loadImage(this, ivUserPicture, userBean.getProfilePicture());
        llContactContain.setVisibility(isStudent && isApprove ? View.VISIBLE : View.GONE);
        tvAddress.setVisibility(isStudent && isApprove ? View.VISIBLE : View.GONE);
        tvContact.setVisibility(isStudent && isApprove ? View.VISIBLE : View.GONE);
        tvEmail.setVisibility(isStudent && isApprove ? View.VISIBLE : View.GONE);
        tvDocument.setVisibility(!isStudent ? View.VISIBLE : View.GONE);
        tvReceipt.setVisibility(!isStudent ? View.VISIBLE : View.GONE);

    }

    @Override
    protected void initListener() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_right);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (!isStudent) {
            getMenuInflater().inflate(R.menu.edit_profile, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_edit:
                showLoadingDialog();
                GetDropDownModel getDropDownModel = new GetDropDownModel(this);
                getDropDownModel.setICallBackListener(this);
                getDropDownModel.executeData();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSuccessResult(int key, Object... params) {
        dismissLoadingDialog();

        switch (key) {

            case Constants.GET_DROPDOWN_TYPE:
                Intent initEditProfile = new Intent(this, ProfileEditActivity.class);
                initEditProfile.putExtra(StdAddCourseActivity.KEY_GET_DROP_DOWN, (DropDownContentBean) params[0]);
                initEditProfile.putExtra(ProfileEditActivity.KEY_USER_BEAN, userBean);
                startActivity(initEditProfile);

                break;
        }
    }


    @OnClick(R.id.tv_phone_call)
    public void phoneCallOnClick() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 101);
        } else {

            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + userBean.getContact()));
            startActivity(intent);
        }


    }

    @OnClick(R.id.tv_send_email)
    public void emailOnClick() {

//        Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.setType("text/plain");
//        intent.putExtra(Intent.EXTRA_EMAIL,userBean.getEmail());
//        startActivityForResult(intent,Constants.REQUEST_USER_CODE);
        sendSMS();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 101: {

                switch (requestCode) {
                    case 101:

                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + userBean.getContact()));
                        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(intent);
                        } else {
                            return;
                        }
                        break;
                    default:
                        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }
        }
    }

    private void sendSMS() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
        {
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this); // Need to change the build to API 19

            Uri uri = Uri.parse("smsto:"+userBean.getContact());
            Intent it = new Intent(Intent.ACTION_SENDTO, uri);
            startActivity(it);

            if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
            // any app that support this intent.
            {
                it.setPackage(defaultSmsPackageName);
            }
            startActivity(it);

        }
        else // For early versions, do what worked for you before.
        {
            Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("address",userBean.getContact());
            startActivity(smsIntent);
        }
    }

}
