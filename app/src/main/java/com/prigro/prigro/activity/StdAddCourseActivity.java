package com.prigro.prigro.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseNavigationActivity;
import com.prigro.prigro.bean.CourseBean;
import com.prigro.prigro.bean.DropDownContentBean;
import com.prigro.prigro.model.AddCourseModel;
import com.prigro.prigro.ui.MyEditText;
import com.prigro.prigro.utils.GPSTracker;
import com.prigro.prigro.utils.MyDialog;
import com.prigro.prigro.utils.Tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by vienhui on 05/05/2017
 */

public class StdAddCourseActivity extends BaseNavigationActivity implements OnMapReadyCallback, View.OnClickListener {

    public static final String KEY_GET_DROP_DOWN = "getdropdown";

    @InjectView(R.id.tool_bar)
    Toolbar toolbar;
    @InjectView(R.id.et_title)
    MyEditText etTitle;
    @InjectView(R.id.et_subject)
    MyEditText etSubject;
    @InjectView(R.id.et_level)
    MyEditText etLevel;
    @InjectView(R.id.et_lesson_per_week)
    MyEditText etLessonPerWeek;
    @InjectView(R.id.et_hour_per_lesson)
    MyEditText etHourPerLesson;
    @InjectView(R.id.et_lesson_type)
    MyEditText etLessonType;
    @InjectView(R.id.ll_contain)
    LinearLayout llContain;
    @InjectView(R.id.iv_map)
    ImageView ivMap;
    @InjectView(R.id.rg_gender)
    RadioGroup rgGender;
    @InjectView(R.id.sv_main_scroll)
    ScrollView svMainScroll;
    @InjectView(R.id.sv_list)
    SearchView svSearchCourse;

    private GoogleMap googleMap = null;
    private List listLessonPerWeek, listHourPerLesson, listSubject, listLevel, listLessonType;
    private TextView textSelect;

    @Override
    public void showSuccessResult(int key, Object... params) {
        MyDialog.showConfirmDialog(this, getString(R.string.success), params[0].toString(), new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                finish();
            }
        });
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_std_add_course;
    }


    @Override
    protected void initData() {

        listLessonPerWeek = getIntegerArray(R.array.lesson_per_week);
        listHourPerLesson = Arrays.asList(getResources().getStringArray(R.array.hour_per_lesson));
        Intent intent = getIntent();
        DropDownContentBean dropDownContentBean = (DropDownContentBean) intent.getSerializableExtra(KEY_GET_DROP_DOWN);
        listSubject = Arrays.asList(dropDownContentBean.getSubject());
        listLevel = Arrays.asList(dropDownContentBean.getLevel());
        listLessonType = Arrays.asList(dropDownContentBean.getLesson_type());
    }

    @Override
    protected void initView() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map);
        mapFragment.getMapAsync(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void initListener() {

        ivMap.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        svMainScroll.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        svMainScroll.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        svMainScroll.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });


        etLessonPerWeek.setOnDetailClickListener(this);
        etSubject.setOnDetailClickListener(this);
        etLevel.setOnDetailClickListener(this);
        etLessonType.setOnDetailClickListener(this);
        etHourPerLesson.setOnDetailClickListener(this);
        svSearchCourse.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                new GeocoderTask().execute(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @OnClick(R.id.sv_list)
    public void searchOnClick() {
        svSearchCourse.onActionViewExpanded();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("unchecked")
    @OnClick(R.id.btn_post)
    public void postOnClick() {

        if (!checkInputFieldEmpty().equals("")) {

            MyDialog.showErrorDialog(this, String.format(getString(R.string.input_cant_empty), checkInputFieldEmpty()));
        }else if(!mapIsEnable()){


            MyDialog.showErrorDialog(this, getString(R.string.set_location));

        } else {
            showLoadingDialog();
            CourseBean courseBean = new CourseBean();
            courseBean.setTitle(etTitle.getDetail());
            courseBean.setSubject_id(Tools.getDataId(listSubject, etSubject.getDetail()));
            courseBean.setLevel_id(Tools.getDataId(listLevel, etLevel.getDetail()));
            courseBean.setLesson_type(Tools.getDataId(listLessonType, etLessonType.getDetail()));
            if (googleMap != null) {
                LatLng center = googleMap.getCameraPosition().target;
                courseBean.setLatitude(Double.toString(center.latitude));
                courseBean.setLongitude(Double.toString(center.longitude));
            }
            courseBean.setLesson_per_week(Integer.parseInt(etLessonPerWeek.getDetail()));
            courseBean.setHour_per_lesson(etHourPerLesson.getDetail());
            courseBean.setGender(getPreferenceGender());
            AddCourseModel stdAddCourseModel = new AddCourseModel(this, courseBean);
            stdAddCourseModel.setICallBackListener(this);
            stdAddCourseModel.executeData();
        }
    }


    private String checkInputFieldEmpty() {

        String emptyInput = "";

        for (int i = 0; i < llContain.getChildCount(); i++) {

            if (llContain.getChildAt(i) instanceof MyEditText && ((MyEditText) llContain.getChildAt(i)).getDetail().isEmpty() && llContain.getChildAt(i).getVisibility() == View.VISIBLE) {
                String temp = ((MyEditText) llContain.getChildAt(i)).getTitle();
                emptyInput = emptyInput.concat(emptyInput.equals("") ? temp : "," + temp);
            }
        }


        return emptyInput;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        new GeocoderTask().execute("");


    }

    @Override
    public void onClick(View v) {

        MyDialog.showListingDialog(StdAddCourseActivity.this, getString(R.string.please_select), getDialogList(v), new MaterialDialog.ListCallback() {
            @Override
            public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                textSelect.setText(text.toString());
            }
        });
    }

    @SuppressWarnings("unchecked")
    private List getDialogList(View v) {
        textSelect = (TextView) v;
        switch (((View) v.getParent()).getId()) {

            case R.id.et_subject:
                return Tools.getDropDownData(listSubject);

            case R.id.et_hour_per_lesson:

                return listHourPerLesson;


            case R.id.et_lesson_per_week:

                return listLessonPerWeek;

            case R.id.et_level:

                return Tools.getDropDownData(listLevel);

            case R.id.et_lesson_type:

                return Tools.getDropDownData(listLessonType);

            default:

                return new ArrayList();
        }
    }


    @SuppressWarnings("unchecked")
    private List getIntegerArray(int arrayID) {


        int[] dataTemp = getResources().getIntArray(arrayID);
        List listData = new ArrayList(dataTemp.length);

        for (int i : dataTemp) {
            listData.add(i);
        }
        return listData;
    }


    private String getPreferenceGender() {

        switch (rgGender.getCheckedRadioButtonId()) {


            case R.id.btn_female:
                return "F";


            case R.id.btn_male:
                return "M";


            default:
                return "N";

        }
    }


    private boolean mapIsEnable() {

        if (googleMap == null)
            return false;

        LatLng center = googleMap.getCameraPosition().target;
        return !Double.toString(center.latitude).equals("0") && !Double.toString(center.longitude).equals("0");

    }

    // An AsyncTask class for accessing the GeoCoding Web Service
    private class GeocoderTask extends AsyncTask<String, Void, List<Address>> {


        @Override
        protected List<Address> doInBackground(String... locationName) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null;

            try {
                // Getting a maximum of 3 Address that matches the input text
                addresses = geocoder.getFromLocationName(locationName[0], 3);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return addresses;
        }


        @Override
        protected void onPostExecute(List<Address> addresses) {

            // Locate the first location
            if (ActivityCompat.checkSelfPermission(StdAddCourseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(StdAddCourseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;


            }
            LatLng latLng;

            svSearchCourse.onActionViewCollapsed();
            if (addresses == null || addresses.size() == 0) {
                googleMap.setMyLocationEnabled(true);
                GPSTracker gpsTracker = new GPSTracker(StdAddCourseActivity.this);

                if (gpsTracker.getIsGPSTrackingEnabled()) {
                    String latitude = String.valueOf(gpsTracker.getLatitude());

                    String longitude = String.valueOf(gpsTracker.getLongitude());
                    latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gpsTracker.showSettingsAlert();
                }
                return;
            }

            // Clears all the existing markers on the map
            googleMap.clear();

            // Adding Markers on Google Map for each matching address
            for (int i = 0; i < addresses.size(); i++) {

                Address address = addresses.get(i);

                // Creating an instance of GeoPoint, to display in Google Map
                latLng = new LatLng(address.getLatitude(), address.getLongitude());
//                String addressText = String.format("%s, %s",
//                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
//                        address.getCountryName());

                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            }
        }

    }
}
