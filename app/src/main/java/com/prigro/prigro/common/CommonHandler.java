package com.prigro.prigro.common;

import android.os.Handler;
import android.os.Message;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prigro.prigro.R;
import com.prigro.prigro.activity.Base.BaseActivity;
import com.prigro.prigro.controller.ICallBack;
import com.prigro.prigro.network.NetworkHelper;
import com.prigro.prigro.network.custom_exception.CNoNetworkException;
import com.prigro.prigro.network.custom_exception.CTimeOutException;


/**
 * Created by SzHui on 2016/1/22
 */
public abstract class CommonHandler extends Handler {

    protected BaseActivity mActivity;
    private NetworkHelper mNetworkHelper;


    protected ICallBack mCallBack;
    protected Gson mGson;


    /**
     * Let another do the handler Message
     *
     * @param msg handler msg
     */
    protected abstract void handlerMainMessage(Message msg);
    protected abstract void handlerExceptionMessage(Message msg);


    public CommonHandler(BaseActivity mActivity) {

        this.mActivity = mActivity;
        mGson = new GsonBuilder().create();
    }

    public NetworkHelper getNetworkHelper() {

        if (mNetworkHelper == null) {

            mNetworkHelper = new NetworkHelper(mActivity, this);
        }

        return mNetworkHelper;
    }


    @Override
    public void handleMessage(Message msg) {

        switch (msg.what) {

            case Constants.STATUS_NO_NETWORK:
            case Constants.STATUS_TIME_OUT:
            case Constants.STATUS_UNKNOWN:
            case Constants.STATUS_SERVER_ERROR:
                handlerExceptionMessage(msg);
                break;
            default:
                handlerMainMessage(msg);
                break;
        }
    }

    public void createExceptionMessage(Exception mExc) {
        Message msg = this.obtainMessage();
        if (mExc instanceof CNoNetworkException) {
            msg.what = Constants.STATUS_NO_NETWORK;
            msg.obj = mActivity.getString(R.string.status_no_network);

        } else if (mExc instanceof CTimeOutException) {
            msg.what = Constants.STATUS_TIME_OUT;
            msg.obj = mActivity.getString(R.string.status_time_out);
        }
        else {
            msg.what = Constants.STATUS_UNKNOWN;
            msg.obj = mActivity.getString(R.string.status_unknown_error);
        }
        handleMessage(msg);
    }


    public void setICallBackListener(ICallBack mCallBack){
        this.mCallBack = mCallBack;
    }

}
