package com.prigro.prigro.common;

import android.util.Log;

/**
 * Created by SzHui on 2016/1/20
 */
public  class MyLog {

    private static boolean blnIsLog = false;

    public static void d(String p_strLog){

        if (blnIsLog){

            Log.d("debug", "MyLog : " + p_strLog);
        }
    }

}
