package com.prigro.prigro.common;

/**
 * Created by SzHui on 2016/1/22
 */
public class Constants {

    public static final String RSA_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA47p+zqI5zSpmVGBFc93d\n" +
            "TqP+GHcJYDq+cvf6H+O8G4T1XvfIDopLD4g6u27Gz8adh+TgBR0dJu1IqPo3IRVg\n" +
            "iwnDkB8e5jC01uOokq5JwkiBxbu8UGkMHC/2m1JppCVVnSxvop8fjlFiTysPRSXG\n" +
            "o8XoENEsWps+duaLws+AEUHW4R6mQg9DlgFtGV5TCq1oubnpgetZVMgrP5TamtOb\n" +
            "EDl6C1R7Qn9Rg9j6mTSEvBjegncU7kCoiJj9ALJ6LBdNCwtDqAByB/esSKNxWYFG\n" +
            "OFsfgOXWdstuD6BTY4SFNnFSlPV9ht2tsVEwBZpnp9tBN690fijRRQN97RKr2WOB\n" +
            "TQIDAQAB";
    public static final String RSA_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDjun7OojnNKmZU\n" +
            "YEVz3d1Oo/4YdwlgOr5y9/of47wbhPVe98gOiksPiDq7bsbPxp2H5OAFHR0m7Uio\n" +
            "+jchFWCLCcOQHx7mMLTW46iSrknCSIHFu7xQaQwcL/abUmmkJVWdLG+inx+OUWJP\n" +
            "Kw9FJcajxegQ0Sxamz525ovCz4ARQdbhHqZCD0OWAW0ZXlMKrWi5uemB61lUyCs/\n" +
            "lNqa05sQOXoLVHtCf1GD2PqZNIS8GN6CdxTuQKiImP0AsnosF00LC0OoAHIH96xI\n" +
            "o3FZgUY4Wx+A5dZ2y24PoFNjhIU2cVKU9X2G3a2xUTAFmmen20E3r3R+KNFFA33t\n" +
            "EqvZY4FNAgMBAAECggEAZ6ETWXarNPjJbjYNBOf+cG3iEEIDCz5QMkM0GFTq4d8E\n" +
            "Rb4d1LgL5jqI94g2gpCQWSnG46zW+kLaYMLUWYK/1yWPT8NJrViWwn8A1Bbpy7Zx\n" +
            "D4kZ7cGP2DffUdkT57Vxmz+AYZFlq77PLNWcckv5c8d4jkaRl32z1Cmfifwg/DiM\n" +
            "2+ZrZRAtLfbSpcPMLdJ2/qjVB/bbTdXTmmNEX97iBBfMrlOFXGnmxezF6SME+Qll\n" +
            "xsKgiVi82VEJxdHMktZSOnsfMPqVsyzd1WsYAd1o0y7Gd+6QBUCCtX/dRJ05SCu9\n" +
            "XdySf+Nb3h/b5uGkblWb7MeQDLNHnhQXh3kUkoGahQKBgQD79CPYukEifEpkgYLb\n" +
            "IfFqtpNP/tNAFV06SMFbybVyiaHm5/VO8NpTEeNUiJIcOstpliaH4wrXQ7jt4PB1\n" +
            "P1XMDpl9PGV8+uP3mv27KvdY3jLKs2j7z8kcwpI5iBBGkQ4e9Bc8UXIeg2IfTjVc\n" +
            "k4b76ZPjEhkevcgoAWWElhizdwKBgQDnYsIRnU2bnk2XX8HWTUOuRlA1d7IRMKTC\n" +
            "WfAZZ5Emdz7aSik5ukfswUvtfAVE2ve2hUB/Eb2C5ZgStG1wG4W1ERpiaFQBO9rU\n" +
            "CHvNDvackDJ2nhQAT9e//cbxGuB7Ikr0thRobvoZLg5uUX1Xguhb+wuAFIlHMx6v\n" +
            "iFH3ItN6WwKBgQDEyC/fK8mHl6+6w/RiFiaqBCMWTZ7M5GLGDOEIQYh4Bsruv6g2\n" +
            "maLLUqNM+MW1Sq5orx807r0e2APttsvXpznpgFWdHLjfXk0CrIkT0EETpafh7TJH\n" +
            "ykf3MHhoer7LfquavWYQdEQ/VLokXT8MKSU6R3p71TzNZ8iWhlC1t01fjQKBgQCn\n" +
            "k7bJ2wsh8XxwbUGNW1fxF6tvjcjA+mHZABoLzB4rpaE3pw5EaM0+gp/i1zdZrtxI\n" +
            "rCkR3l9KNc31fgbkcopboI9jRfvRCPBrL9m5oWSp7yUdE0+ZCjpPKhSy+awqgaOX\n" +
            "p2cQW492PQzmiM4mWwC93QtvL3BcyIEoz6nNOsO+kQKBgHFMKBlK4Dty/5Sf0/aT\n" +
            "ET+s0+7Jyozr41rKXn5yXmXg2LK/cN0RtDdERe6PjM8EXHv74sQMHPuMyedn03Bm\n" +
            "7rkyQtL6tZPGNOtKx24BZsNxh2Hqr/yM4YI4WWZ4m8rfFpMwMXCAxx+qc0yFraLH\n" +
            "Vr6pmatp7BmoeeNXmrn3sRtK";


    public static final int STATUS_EXCEPTION = -0x00;
    public static final int STATUS_NO_NETWORK = -0x01;
    public static final int STATUS_TIME_OUT = -0x02;
    public static final int STATUS_UNKNOWN = -0x03;
    public static final int STATUS_SERVER_ERROR = -0x04;
    public static final int STATUS_ERROR = -0x05;

    public static final int[] SUCCESS_CODE = new int[]{200, 210, 201, 203, 232, 230, 211, 212, 220, 221, 250, 255, 260, 270, 278, 280, 285, 289};

    //preference id
    public static final String SP_UNIQUE_ID = "userId";
    public static final String SP_TOKEN = "token";
    public static final String SP_USER_TYPE = "type";
    public static final String SP_LANGUAGE = "language";
    public static final String APPLIED = "2";
    public static final String APPROVE = "1";

    public static final int TUTOR = 2;
    public static final int STUDENT = 1;
    public static final int REQUEST_LISTING_CODE = 100;
    public static final int REQUEST_USER_CODE = 101;


    //model key
    public static final int REGISTER_MODEL = 0x001;
    public static final int EMAIL_LOGIN__MODEL = 0x002;
    public static final int TUTOR_COURSE_MODEL = 0x003;
    public static final int GET_TUTOR_MODEL = 0x004;
    public static final int EDIT_TUTOR_MODEL = 0x005;
    public static final int GET_STD_MODEL = 0x006;
    public static final int STD_COURSE_MODEL = 0x007;
    public static final int STD_ADD_COURSE_MODEL = 0x008;
    public static final int STD_COURSE_DETAIL_MODEL = 0x009;
    public static final int APPLY_COURSE_MODEL = 0x010;
    public static final int FORGOT_PASSWORD_MODEL = 0x011;
    public static final int LOGOUT_MODEL = 0x012;
    public static final int FB_LOGIN_MODEL = 0x013;
    public static final int APPROVE_COURSE_MODEL = 0x014;
    public static final int DELETE_COURSE_MODEL = 0x015;
    public static final int GET_LISTING_DETAIL = 0x016;
    public static final int FB_ADD_TYPE = 0x017;
    public static final int FB_SUCCESS_ADD_TYPE = 0x018;
    public static final int GET_DROPDOWN_TYPE = 0x019;
    public static final int CHANGE_LANGUAGE = 0x020;
    public static final int GUEST_LOGIN = 0x021;
    public static final int GET_NOTIFICATION_LIST = 0x022;
    public static final int SET_NOTIFICATION_SEEN = 0x023;
    public static final int SEARCH_MODEL = 0x024;
    public static final int ADD_COURSE_LIST_MODEL = 0x025;
    public static final int TUITION_CENTER_MODEL = 0X026;
    public static final int ADD_TUITION_LIST_MODEL = 0x27;
    public static final int TUITION_CENTER_DETAIL_MODEL = 0x28;
    public static final int ADS_MODEL = 0x29;


    public static final String SEPARATOR = ":@:";

    //language key
    public static final int LANGUAGE_DEFAULT = -1;
    public static final int LANGUAGE_EN = 0;
    public static final int LANGUAGE_CN = 1;
    public static final int LANGUAGE_MY = 2;
}
